<!DOCTYPE html>
<html>
	<head>
		<?php include_http_metas() ?>
		<?php include_stylesheets()?>
		<?php include_title() ?>
	</head>
	<body class="cyrek-digital">
		<div class="header-wrapper">
			<?php include_partial("dashboard/header")?>
		</div>
		<div class="container-wrapper">
			<div class="sidebar-wrapper">
				<?php include_partial("dashboard/sidebar")?>
			</div>
			<div class="content-wrapper">
				<div class="content">
					
					<?php echo $sf_content ?>
					
				</div>
			</div>
			<div class="footer-wrapper">
				2010 - <?php echo date("Y")?> &copy; <a href="http://cyrekdigital.pl" target="_blank">Cyrek Internet Technologies</a>.
			</div>
		</div>
	
	
	<?php include_javascripts()?>
	<?php include_partial("widgets/notifications")?>
	<?php if (has_slot('additional_javascript')) echo get_slot('additional_javascript') ?>
	</body>
</html>
