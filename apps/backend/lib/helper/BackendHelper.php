<?php

function getAllowedCultures(){
	$dimensions = sfYaml::load(sfConfig::get('sf_config_dir').'/dimensions.yml');
	return $dimensions['allowed']['culture'];
}

function get_page_type_name(){
	$request = sfContext::getInstance()->getRequest();
	switch($request->getParameter("name")){
		case "strony-informacyjne" :{
			return "Strony informacyjne";
			break;
		}
		case "wpisy" :{
			return "Wpisy";
			break;
		}
		case "kategorie" :{
			return "Kategorie";
			break;
		}
		case "produkty" :{
			return "Produkty";
			break;
		}
	}
}

function get_page_type_name_by_kind($kind){
	switch($kind){
		case 1 :{
			return "Strony informacyjne";
			break;
		}
		case 2 :{
			return "Wpisy";
			break;
		}
		case 3 :{
			return "Kategorie";
			break;
		}
		case 4 :{
			return "Produkty";
			break;
		}
	}
}

function get_page_type_id(){
	$request = sfContext::getInstance()->getRequest();
	switch($request->getParameter("name")){
		case "strony-informacyjne" :{
			return 1;
			break;
		}
		case "wpisy" :{
			return 2;
			break;
		}
		case "kategorie" :{
			return 3;
			break;
		}
		case "produkty" :{
			return 4;
			break;
		}
	}
}


function display_notification($type, $title, $text, $hide = "true"){
	$content = '<script type="text/javascript">';
	$content.= "
	$.pnotify({
    title: '".$title."',
    text: '".$text."',
    type: '".$type."',
    hide: ".$hide.",
    history: false
	});";
    $content.="</script>";
    return $content;				
}

function display_notifications($notifications){
	$result = "";
	if($notifications){
		foreach($notifications as $notification){
			$result.=display_notification($notification[0],$notification[1],$notification[2],$notification[3]);
		}	
	}
	return $result;
}

?>

