<?php

class imageActions extends sfActions 
{
	public function executeIndex($request) 
	{
	
	}
	
	public function executeUpload()
	{
		if($this->getRequest()->getMethod() == sfRequest::POST)
		{
			$fname = $this->getRequestParameter('name');
				
			if($fname && $this->getRequestParameter('pid')){
				
				
				$conn = Propel::getConnection();
				$query = "select max(position) as position from image where page_id=".$this->getRequestParameter('pid');
				$rs = $conn->executeQuery($query);
				$position = 0;
				while($rs->next()){
					$position = $rs->getInt('position');
				}
				
				
				$fname = explode(".",$fname);
				$ext = strtolower(end($fname));
				unset($fname[count($fname)-1]);
				$fname = Utils::normalizeName(implode(".",$fname));
				
				$image = new Image();
				$image->setPageId($this->getRequestParameter('pid'));
				$image->setPosition($position+1);
				$image->save();
				
				$fname = $fname.'_'.$image->getId().'.'.$ext;
				
				$uploader = new Uploader();
				$file = $uploader->upload('image',$fname);
				
				if(file_exists(sfConfig::get("sf_upload_dir").'/images/'.$file))
				{
					$image->setUrl($file);
					$image->setDescription($this->getRequestParameter('desc'));
					$image->save();	
				}
				else
				{
					$image->delete();
				}
			}
		}
		
		return sfView::NONE;
	}
	
	public function executeList()
	{
		$conn = Propel::getConnection();
		$excluded = $this->getRequestParameter('excluded');
		$rs = $conn->executeQuery("SELECT * FROM `image` WHERE `page_id` = ".$this->getRequestParameter('pid')." ".(!empty($excluded) ? 'AND `id` NOT IN('.implode(",",$excluded).')' : ''));
		
		$img = array();
		while($rs->next()) $img[$rs->getInt('id')] = array('id' => $rs->getInt('id'), 'name' => (strlen($rs->getString('description')) ? $rs->getString('description') : $rs->getString('url')), 'url' => '/uploads/images/'.$rs->getString('url'), 'desc' => $rs->getString('description'));
		
		echo json_encode($img);
		return sfView::NONE;
	}
	
	public function executeRemove()
	{
		$conn = Propel::getConnection();
		
		$path = sfConfig::get("sf_upload_dir").'/images/';
		if($this->getRequestParameter('id')){
			
			$image = ImagePeer::retrieveByPK($this->getRequestParameter('id'));
			if($image instanceOf Image){
				if(file_exists($path.$image->getUrl())){
					unlink($path.$image->getUrl());
				}	
				$image->delete();
				
				$conn->executeQuery("SET @i = 0;");
				$conn->executeQuery("UPDATE image SET position = @i:=@i+1 where page_id=".$image->getPageId()." order by position");
				
			}
		}
		
		return sfView::NONE;
	}
	
	public function executePositionUpdate($request){
		$conn = Propel::getConnection();
		$data = ($this->getRequestParameter("sortedList"));
	
		$positions=array();
		$i=0;
		foreach($data as $key=>$value){
			$i++;
			$positions[$value["id"]]=$i;
		}
		
		$tmp=array();
		$query = "insert into image (id,position,page_id) values";
		foreach($positions as $id=>$position){
			$tmp[]=" (".$id.",".$position.",".$this->getRequestParameter("id").")";
		}
		$query.=implode(",", $tmp);
		$query.=" on duplicate key update position=values(position)";
		$conn->executeQuery($query);
		
	
	}
	
	
	public function executeMultiRemove()
	{
		if($this->getRequest()->getMethod() == sfRequest::POST || $this->getRequest()->isXmlHttpRequest())
		{
			$pids = $this->getRequestParameter('pids');
			if(!empty($pids))
			{
				$conn = Propel::getConnection();
				
				// pobieram nazwy plikow w celu usuniecia z dysku
				$rs = $conn->executeQuery("SELECT `url` FROM `image` WHERE `id` IN (".implode(",",$this->getRequestParameter('pids')).")");
				$path = sfConfig::get("sf_upload_dir").'/images/';
				while($rs->next())
				{
					if(file_exists($path.$rs->getString('url')))
					{
						unlink($path.$rs->getString('url'));
					}
				}
				
				$rs = $conn->executeQuery("DELETE FROM `image` WHERE `id` IN (".implode(",",$this->getRequestParameter('pids')).")");
			}
		}
		return sfView::NONE;
	}
	
	public function executeDescriptionUpdate($request)
	{
		if($this->getRequest()->getMethod() == sfRequest::POST || $this->getRequest()->isXmlHttpRequest())
		{
			$iid = $this->getRequestParameter('iid');
			if(!empty($iid))
			{
				$conn = Propel::getConnection();
				$conn->executeQuery("UPDATE `image` SET `description` = '".$this->getRequestParameter('description')."' WHERE `id` = ".$this->getRequestParameter('iid'));
			}
		}
	
		return sfView::NONE;
	}
}
