<div class="row">
	<div class="col-lg-12">
		<h2 class="page-title">
			<?php echo __("Messages")?>
		</h2>
	</div>
</div>
	
<div class="email-actions">
<a href="" id="checkAll"><?php echo __("Check all")?></a> | <a href = "" id = "uncheckAll"><?php echo __("Uncheck all")?></a> | <a href = "" id = "removeChecked"><?php echo __("Remove selected")?></a> 
</div>
	
<div class="widget-container mails">
	<!-- <div class="widget-header">
		<h3>
			<i class="icon-envelope"></i>
			<?php echo __("New messages")?>
		</h3>
	</div> -->
	<div class="widget-content">
		<?php if(!empty($messages)):?>
			<ul id="messages">
				<?php foreach($messages as $message):?>
					<li id="message-wrapper-<?php echo $message['id']; ?>">
						<div class="message">
							<div class="message-action">
								<input type="checkbox" class="email-check" value="<?php echo $message['id'];?>" name="emailRemove[<?php echo $message['id'];?>" />
							</div>
							<div class="message-content-wrapper">
							<?php echo __("From")?>: <b><?php echo $message['email']?></b><br />
							<b><?php echo __("Send at")?>:</b> <?php echo date("d-m-Y H:i:s",strtotime($message['created_at']));?><br />
							<b><?php echo __("Message content")?>:</b><br /><br />
							<div class="message-content">
								"<?php echo $message['content']?>"<br />
							</div>
							</div>
						</div>
					</li>
				<?php endforeach;?>
			</ul>
		<?php else:?>
			<?php echo __("No messages.")?>	
		<?php endif;?>
	</div>
</div>

<?php append_to_slot('additional_javascript') ?>
	<script type = "text/javascript">
	$(function()
	{
		$("input.email-check").click(function()
		{
			if($(this).is(":checked")) { markEmailBox($(this)); }
			else { unmarkEmailBox($(this)); }
		});

		$("a#checkAll").click(function()
		{
			$("#messages").find("input.email-check").each(function()
			{
				$(this).prop('checked',true);
				markEmailBox($(this));
			});
			
			return false;
		});

		$("a#uncheckAll").click(function()
		{
			$("#messages").find("input.email-check").each(function()
			{
				$(this).attr('checked',false);
				unmarkEmailBox($(this));
			});
				
			return false;
		});

		$("a#removeChecked").click(function()
		{
			toDelete = [];
			$("#messages").find("li.email-active input.email-check").each(function()
			{
				if($(this).is(":checked")) $("ul#messages li#message-wrapper-"+$(this).val()).fadeOut();
				toDelete.push($(this).val());
			});

			if(toDelete.length > 0)
			{
				$.ajax(
				{
					type: "POST",
					url: "<?php echo url_for('@mail_multi_remove');?>",
					data: { del: toDelete }, 
					success: function(data)
					{
						$("#messages").find("li.email-active").each(function() { $(this).fadeOut(function() { $(this).remove(); }); });
					},
				});
			}
			
			return false;
		});
	});

	function markEmailBox(el)
	{
		$("ul#messages li#message-wrapper-"+el.val()).addClass('email-active');
	}
	
	function unmarkEmailBox(el)
	{
		$("ul#messages li#message-wrapper-"+el.val()).removeClass('email-active');
	}
</script>
<?php end_slot(); ?>