<?php
class mailActions extends sfActions {
	public function executeIndex($request) 
	{
		$emails = array();
		
		$conn = Propel::getConnection();
		$rs = $conn->executeQuery("SELECT * FROM `mail` ORDER BY `created_at` DESC");
		
		while($rs->next()) $emails[$rs->getInt('id')] = $rs->getRow();
		
		$this->messages = $emails;
		
	}
	public function executeRemove($request) {
		
	}
	
	public function executeMultiRemove($request)
	{
		$del = $this->getRequestParameter('del'); 
		if(!empty($del))
		{
			$conn = Propel::getConnection();
			$rs = $conn->executeQuery("DELETE FROM `mail` WHERE `id` IN (".implode(",",$del).")");
		}
	
		return sfView::NONE;
	}
}
