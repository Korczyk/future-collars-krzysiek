<?php if($sf_user->getFlash("notifications",array())):?>
	<?php append_to_slot('additional_javascript') ?>
		<?php echo display_notifications($sf_user->getFlash("notifications",array()))?>
	<?php end_slot() ?>
<?php endif?>