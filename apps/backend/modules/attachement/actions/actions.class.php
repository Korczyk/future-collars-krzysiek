<?php
class attachementActions extends sfActions 
{
	public function executeIndex($request) {
	}
	
	public function executeUpload()
	{
		if($this->getRequest()->getMethod() == sfRequest::POST)
		{
			$fname = $this->getRequestParameter('name');

			if($fname && $this->getRequestParameter('pid'))
			{
				$fname = explode(".",$fname);
				$ext = end($fname);
				unset($fname[count($fname)-1]);
				$fname = Utils::normalizeName(implode(".",$fname));
					
				$att = new Attachement();
				$att->setPageId($this->getRequestParameter('pid'));
				$att->save();
					
				$fname = $fname.'_'.$att->getId().'.'.$ext;
					
				$u = new Uploader();
				$file = $u->upload('attach',$fname);
				
				if(file_exists(sfConfig::get("sf_upload_dir").'/attachements/'.$file))
				{
					$att->setUrl($file);
					$att->setDescription($this->getRequestParameter('desc'));
					$att->save();
				}
				else
				{
					$att->delete();
				}
			}
		}
		
		return sfView::NONE;
	}
	
	public function executeList()
	{
		$conn = Propel::getConnection();
		$excluded = $this->getRequestParameter('excluded');
		$rs = $conn->executeQuery("SELECT * FROM `attachement` WHERE `page_id` = ".$this->getRequestParameter('pid')." ".(!empty($excluded) ? 'AND `id` NOT IN('.implode(",",$this->getRequestParameter('excluded')).')' : ''));
	
		$files = array();
		while($rs->next()) $files[$rs->getInt('id')] = array('id' => $rs->getInt('id'), 'name' => (strlen($rs->getString('description')) ? $rs->getString('description') : $rs->getString('url')), 'url' => '/uploads/attachements/'.$rs->getString('url'), 'desc' => $rs->getString('description'));
	
		echo json_encode($files);
		return sfView::NONE;
	}
	
	public function executeRemove()
	{
		$iid = $this->getRequestParameter('id');
		$path = sfConfig::get("sf_upload_dir").'/attachements/';
		if($iid)
		{
			$conn = Propel::getConnection();
				
			// pobieram nazwy plikow w celu usuniecia z dysku
			$rs = $conn->executeQuery("SELECT `url` FROM `attachement` WHERE `id` = ".$iid);
			while($rs->next())
			{
				if(file_exists($path.$rs->getString('url')))
				{
					unlink($path.$rs->getString('url'));
				}
			}
			$rs = $conn->executeQuery("DELETE FROM `attachement` WHERE `id` = ".$iid);
		}
	
		return sfView::NONE;
	}
	
	public function executeDescriptionUpdate($request)
	{
		if($this->getRequest()->getMethod() == sfRequest::POST || $this->getRequest()->isXmlHttpRequest())
		{
			$iid = $this->getRequestParameter('iid');
			if(!empty($iid))
			{
				$conn = Propel::getConnection();
				$conn->executeQuery("UPDATE `attachement` SET `description` = '".$this->getRequestParameter('description')."' WHERE `id` = ".$this->getRequestParameter('iid'));
			}
		}
	
		return sfView::NONE;
	}
	
}