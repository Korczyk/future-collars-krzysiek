<div class="header ">
	<?php if($sf_user->hasCredential('administrator') and $sf_user->isAuthenticated()):?>
	
		<div class="head-info-panel clearfix">
			<span class="company-name">Cyrek Digital</span>
			<ul class="nav navbar-nav navbar-right navbar-user">

				<li class="dropdown user"><a data-toggle="dropdown"
					class="dropdown-toggle" href="#"><span class="username"><?php echo $sf_user->getAttribute('name')?></span> <i class="icon-icon-strzalka"></i>
				</a>
					<ul class="dropdown-menu" style="">
						<?php if(0):?>
						<li><a href="#"><i class="icon-calendar"></i> <?php echo __("My calendar")?></a></li>
						<li><a href="#"><i class="icon-tasks"></i> <?php echo __("My tasks")?></a></li>
						<li class="divider"></li>
						<?php endif?>
						<li><a href="<?php echo url_for('@user_logout')?>"><i class="icon-key"></i> <?php echo __("Logout")?></a></li>
					</ul>
				</li>
			</ul>
		</div>

	 	
	
	
</div>
	<div class="menu-panel clearfix">
		<ul class="nav navbar-nav navbar-content">  
		 	<li class="current">
				<a href="<?php echo url_for("@homepage")?>"><i class="icon-icon-zamowienia"></i> <span class="hidden-xs"><?php echo __("Dashboard")?></span></a>
			</li>
			<li>
				<a href="<?php echo url_for("@mail_index")?>"><i class="icon-icon-wiadomosci"></i> <span class="hidden-xs"><?php echo __("Messages")?></span></a>
			</li>
			<li><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i  class="icon-icon-zawartosc"></i> <span class="hidden-xs"><?php echo __("Content")?><i class="icon-icon-strzalka"></i></span></a>
				<ul class="dropdown-menu">
					<li>
						<a href="<?php echo url_for("@page_index?name=strony-informacyjne")?>"><i class="icon-angle-right"></i> <?php echo __("Pages")?></a>
						<a href="<?php echo url_for("@stats")?>"><i class="icon-angle-right"></i><?php echo __("Stats")?></a>
						<a href="<?php echo url_for("@page_index?name=wpisy")?>"><i class="icon-angle-right"></i> <?php echo __("Posts")?></a>
						<a href="<?php echo url_for("@page_index?name=kategorie")?>"><i class="icon-angle-right"></i> <?php echo __("Categories")?></a>
						<a href="<?php echo url_for("@image_index")?>"><i class="icon-angle-right"></i> <?php echo __("Images")?></a>
						<a href="<?php echo url_for("@attachement_index")?>"><i class="icon-angle-right"></i> <?php echo __("Attachements")?></a>
					</li>
					
				</ul>
			</li>
			<li>
				<a href="<?php echo url_for("@user_index")?>"><i class="icon-icon-uzytkownik"></i> <span class="hidden-xs"><?php echo __("Users")?></span></a>
			</li>
			<li>
				<a href="<?php echo url_for("@page_index?name=produkty")?>"><i class="icon-icon-produkty"></i> <span class="hidden-xs"><?php echo __("Products")?></span></a>
			</li>
			<li>
				<a href="<?php echo url_for("@config")?>"><i class="icon-icon-ustawienia"></i> <span class="hidden-xs"><?php echo __("Settings")?></span></a>
			</li>
		</ul>
	</div>
<?php endif?>
</div>