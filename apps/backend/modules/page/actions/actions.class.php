<?php 
class pageActions extends sfActions {
	public function executeIndex($request){
	
	}
	public function executeAdd($request)
	{
		sfLoader::loadHelpers("Backend");
		
		$page = new Page();
		$page->save();
		
		$page->setLanguage($this->getRequestParameter('language'),'pl');
		$page->setParent(0);
		$page->setDescendants($page->getId());
		$page->setPath($page->getId());
		$page->setKind(get_page_type_id());
		$page->setPosition(1);
		$page->save();
		
		$this->page = $page;		
	}
	
	public function executeEdit($request){
		
		$page = PagePeer::retrieveByPK($this->getRequestParameter('id'));
		
		if($page instanceof Page){
			$this->page = $page;
					
		}
		else{
			$this->redirect404();
		}
	
	}
	
	public function validateUpdate($request){
		if($this->getRequest()->getMethod() == sfRequest::POST){
			$page = $this->getRequestParameter('page');
	
			$this->page = PagePeer::retrieveByPK($page['id']);
			if($this->page instanceof Page){
				return true;
			}
			else{
				$this->page = new Page;
				return true;
			}
		}
		else{
			return false;
		}
	}
	
	
	public function executeUpdate($request) 
	{
		
		$this->page->fromArray($this->getRequestParameter('page'),BasePeer::TYPE_FIELDNAME);
		$this->page->setCreatedBy("");
		$this->page->setModifiedBy("");
		$this->page->buildIndex();
		$this->page->save();
		
		PagePeer::rebuildPagesTree($this->page->getKind());

		
		$notifications = $this->getUser()->setFlash("notifications", array() );
		$notifications[]=array("success",$this->page->isNew()?"Strona dodana":"Strona zmodyfikowana",$this->page->isNew()?"Strona dodana pomyślnie":"Strona zmodyfikowana pomyślnie",true);
		$this->getUser()->setFlash('notifications', $notifications);
		
		return $this->redirect("@page_index_language?name=".$this->getRequestParameter('name')."&language=".$this->page->getLanguage());
		
	}
	
public function executeRemove($request) 
	{
		sfLoader::loadHelpers("Backend");
		$page = PagePeer::retrieveByPK($this->getRequestParameter("id"));
		if($page instanceOf Page){
			$keys = explode(" ",$page->getDescendants());
			if($keys){
				PagePeer::doDelete($keys);
			}
			
			$page->delete();
			
			PagePeer::rebuildPagesTree($page->getKind());
			
			return $this->redirect("@page_index_language?name=".Utils::normalizeName(get_page_type_name_by_kind($page->getKind()))."&language=".$page->getLanguage());
		}
		else return $this->redirect("@homepage");
		
	}
		
	public function executePositionUpdate($request){
		$conn = Propel::getConnection();
		$data = ($this->getRequestParameter("sortedList"));
		
		$parents=array();
		foreach (new RecursiveIteratorIterator(new RecursiveArrayIterator($data), RecursiveIteratorIterator::SELF_FIRST) as $key => $value) {
			if(isset($value['children'])){
				foreach($value['children'] as $k=>$v){
					$parents[$v['id']]=$value['id'];
				}
			}
		}
		$i=0;
		$positions=array();
		foreach (new RecursiveIteratorIterator(new RecursiveArrayIterator($data), RecursiveIteratorIterator::SELF_FIRST) as $key => $value) {
			if($key==="id"){
				$i++;
				$positions[$value]=$i;
			}
		}
		
		foreach($positions as $id=>$position){
			if(!isset($parents[$id])){
				$parents[$id]=0;
			}
		}

		$tmp=array();
		$query = "insert into page (id,parent,position) values";
		foreach($parents as $id=>$parent){
			$tmp[]=" (".$id.",".$parent.",".$positions[$id].")";
		}
		$query.=implode(",", $tmp);
		$query.=" on duplicate key update parent=values(parent), position=values(position)";
		
		$conn->executeQuery($query);
		
		PagePeer::rebuildPagesTree($this->getRequestParameter("kind"));
		
	}	
}
