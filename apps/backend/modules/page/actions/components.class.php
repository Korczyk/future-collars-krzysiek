<?php

class pageComponents extends sfComponents {
	
	public function executeForm($request) {
		$this->templates = PagePeer::getTemplates();
		
		$c = new Criteria();
		
		$c->add(ImagePeer::PAGE_ID,$this->page->getId());
		$c->addAscendingOrderByColumn(ImagePeer::POSITION);
		$images = ImagePeer::doSelect($c);
			
		$c = new Criteria();
		$c->add(AttachementPeer::PAGE_ID,$this->page->getId());
		$attachements = AttachementPeer::doSelect($c);
			
		$this->images = $images;
		$this->attachements = $attachements;
		
	}
	
public function executePageList(){
		sfLoader::loadHelpers("Backend");
		$conn = Propel::getConnection();
		
		$query = "select id, name, path from page where kind=1 and language='".$this->getRequestParameter("language","pl")."'";
		$rs = $conn->executeQuery($query);
		$parents=array();
		while($rs->next()){
			$parents[$rs->getInt("id")]["name"]=$rs->getString("name");
			$parents[$rs->getInt("id")]["path"]=$rs->getString("path");
		}
		
		$c = new Criteria;
		$c->addAscendingOrderByColumn(PagePeer::POSITION);
		$c->add(PagePeer::KIND,get_page_type_id());
		$c->add(PagePeer::LANGUAGE,$this->getRequestParameter("language","pl"));
		$c->addDescendingOrderByColumn(PagePeer::CREATED_AT);
		
		$pages = PagePeer::doSelect($c);
		
		$this->pages=$pages;
		$this->parents=$parents;
		
	}
	
	public function executePageTree(){
		sfLoader::loadHelpers("Backend");
		$lang=isset($this->pageCurrent)?$this->pageCurrent->getLanguage():$this->getRequestParameter("language",'pl');

		$tree = array();
		$info = array();
		
		$c = new Criteria;
		$c->addAscendingOrderByColumn(PagePeer::POSITION);
		$kind = get_page_type_id();
		
		
		$c->add(PagePeer::KIND,$kind);
		$c->add(PagePeer::LANGUAGE,$lang);
		
		$pages = PagePeer::doSelect($c);
		
		foreach($pages as $page){
			$info[$page->getId()]=$page;
			$temp=null;
			$path = array_reverse(explode(' ',$page->getPath()),true);
			
			$code='if(!isset($node';
			foreach($path as $key=>$value){
				$code .= '[' . $value . ']';
			}
			$code .= ')) $node';
			foreach($path as $key=>$value){
				$code .= '[' . $value . ']';
			}
			$code .= '=array();';
			
			eval($code);
			$tree[0]=$node;
		}
		
		if($this->pageCurrent){
			$pageCurrent = $this->pageCurrent;
		}
		else{
			$pageCurrent = new Page;
		}
		
		
		
		if($this->kind=="select"){
			
			echo (get_partial('pageTreeSelect', array('flattenTree' => PagePeer::buildtree($tree),'pages'=>$info,'pageCurrent'=>$pageCurrent)));
		}
		elseif($this->kind=="nested"){
			echo (get_partial('pageTreeNested', array('flattenTree' => PagePeer::buildtree($tree),'pages'=>$info,'pageCurrent'=>$pageCurrent)));
		}
		return sfView::NONE;
			
				
		
	}
	
	
	
}
