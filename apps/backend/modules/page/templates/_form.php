<?php use_helper("Form")?>
<?php sfContext::getInstance()->getResponse()->addJavascript('ckeditor/ckeditor.js');?>
<?php sfContext::getInstance()->getResponse()->addJavascript('ckfinder/ckfinder.js');?>
<?php sfContext::getInstance()->getResponse()->addJavascript('plugins/moment-with-langs.min.js');?>
<?php sfContext::getInstance()->getResponse()->addJavascript('plugins/plupload/js/plupload.full.min.js');?>

<?php sfContext::getInstance()->getResponse()->addJavascript('plugins/jquery.nestable.js');?>
<?php sfContext::getInstance()->getResponse()->addStylesheet('plugins/jquery.nestable.css');?>

<?php sfContext::getInstance()->getResponse()->addJavascript('plugins/bootstrap-datetimepicker.min.js');?>
<?php sfContext::getInstance()->getResponse()->addStylesheet('plugins/bootstrap-datetimepicker.min.css');?>



<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo url_for('@page_update?name='.$sf_request->getParameter('name')); ?>">
<div class="row">
	<div class="col-lg-8">
		<div class="widget-container">
			<div class="widget-header">
				<h3><i class="icon-list-alt"></i> <?php echo __("GENERAL INFORMATION")?></h3>
				<i class="icon-chevron-down hide-widget"></i>
			</div>
			
			<div class="widget-content">
				<div>
					<fieldset>
				   		<div class="control-group">
				        	<label class="col-sm-3 control-label">Tytuł</label>
					        <div class="col-sm-9">
					        	<?php echo input_tag("page[name]",(isset($page)?$page->getName() : null),array("class"=>"form-control"))?>
					        </div>
					    </div>
					    
					    
				    	
				    	<div class="control-group">
				        	<label class="col-sm-3 control-label">Tytuł w menu</label>
					        <div class="col-sm-9">
					        	<?php echo input_tag("page[name_menu]",(isset($page)?$page->getNameMenu() : null),array("class"=>"form-control"))?>
					        </div>
				    	</div>
				    	
				    	
				    	<div class="control-group">
				        	<label class="col-sm-3 control-label">Adres zewnętrzny</label>
					        <div class="col-sm-9">
					        	<?php echo input_tag("page[external_url]",(isset($page)?$page->getExternalUrl() : null),array("class"=>"form-control"))?>
					        </div>
				    	</div>
				    	
				    	
				    	<div class="control-group">
				        	<label class="control-label col-sm-3">Treść</label>
					        <div class="controls form-group col-sm-9">
					        	<?php echo textarea_tag("page[content]",(isset($page)?$page->getContent() : null),array("class"=>"form-control"))?>
					        </div>
				    	</div>
				   </fieldset>
				</div>
			</div>
		</div>
		
		<div class="widget-container">
			<div class="widget-header">
				
				<h3><i class="icon-list-alt"></i> SEO</h3>
				<i class="icon-chevron-down hide-widget"></i>
			</div>
			
			<div class="widget-content">
				<div>
					<fieldset>
				   		<div class="control-group">
				        	<label class="col-sm-3 control-label">Meta Title</label>
					        <div class="col-sm-9">
					        	<?php echo input_tag("page[option][meta_title]",isset($page)?$page->getOption("meta_title"):0,array("class"=>"form-control"))?>
					        </div>
				    	</div>
				    	
				    	<div class="control-group">
				        	<label class="col-sm-3 control-label">Meta Description</label>
					        <div class="col-sm-9">
					        	<?php echo input_tag("page[option][meta_description]",isset($page)?$page->getOption("meta_description"):0,array("class"=>"form-control"))?>
					        </div>
				    	</div>
				    	
				    	<div class="control-group">
				        	<label class="col-sm-3 control-label">Meta Keywords</label>
					        <div class="col-sm-9">
					        	<?php echo input_tag("page[option][meta_keywords]",isset($page)?$page->getOption("meta_keywords"):0,array("class"=>"form-control"))?>
					        </div>
				    	</div>
				   </fieldset>
				</div>
			</div>
		</div>
		
		
		
		<div class="widget-container">
			<div class="widget-header">
				
				<h3><i class="icon-list-alt"></i> GALERIA ZDJĘĆ</h3>
				<i class="icon-chevron-down hide-widget"></i>
			</div>
			
			<div class="widget-content">
				<div style="padding: 10px">
					<?php include_partial("page/gallery",array("images"=>$images,'page'=>$page))?>
				</div>
			</div>
		</div>
		
		<div class="widget-container">
			<div class="widget-header">
				<h3><i class="icon-list-alt"></i> ZAŁĄCZNIKI</h3>
				<i class="icon-chevron-down hide-widget"></i>
			</div>
				
			<div class="widget-content">
				<?php include_partial("page/attachements",array("attachements"=>$attachements,'page'=>$page))?>
				
			</div>
		</div>
	</div>	
	
	<div class="col-lg-4">
		<div class="widget-container">
			<div class="widget-header">
				<h3><i class="icon-list-alt"></i> <?php echo __("ACTION")?></h3>
				<i class="icon-chevron-down hide-widget"></i>
			</div>
			<div class="widget-content">
				<div>
					<fieldset>
				   		<div class="control-group">
				        	<label class="col-sm-6 control-label"><?php echo __("Created at")?></label>
					       	<div class="col-sm-6 form-group">
					        	 <div class="form-group">
					                <div class='input-group date' id='datetimepicker1'>
					                	<?php echo input_tag("page[created_at]",(isset($page)?$page->getCreatedAt() : date("Y-m-d H:i:s",time())),array("class"=>"form-control","data-format"=>"YYYY-MM-DD HH:mm"))?>
					                    <span class="input-group-addon">
					                    	<span class="glyphicon glyphicon-calendar"></span>
					                	</span>
					                </div>
					            </div>
					        </div>
				    	</div>
				   </fieldset>
				</div>
				<div class="row text-center">
						<input type="submit" name="submit" value="<?php echo __("Save")?>" class="btn btn-success" style="margin-bottom: 10px" />
				</div>
				
			</div>
		</div>
	
		
		<div class="widget-container">
			<div class="widget-header">
				
				<h3><?php echo __("ADDITIONAL INFO")?></h3>
				<i class="icon-chevron-down hide-widget"></i>
			</div>
			<div class="widget-content">
				<div class="">
					<fieldset>
				   		<div class="control-group">
				        	<label class="col-sm-5 control-label"><?php echo __("Parent page")?></label>
					        <div class="col-sm-7">
					        	<?php include_component("page", "pageTree",array("kind"=>"select",'pageCurrent'=>(isset($page)?$page:null)))?>
					        </div>
				    	</div>
				    	
				    	<div class="control-group">
				        	<label class="col-sm-5 control-label"><?php echo __("Page is published")?></label>
					        <div class="col-sm-7 checkbox">
					        	<?php echo checkbox_tag("page[is_published]",1,isset($page)?$page->getIsPublished():0,array("class"=>"form-checkbox", "id" => "checkbox_1"))?>
					        	<label for="checkbox_1"></label>
					        </div>
				    	</div>
				    	
				    	<div class="control-group">
				        	<label class="col-sm-5 control-label"><?php echo __("Page visible in main menu")?></label>
					        <div class="col-sm-7 checkbox">
					        	<?php echo checkbox_tag("page[is_featured]",1,isset($page)?$page->getIsFeatured():0,array("class"=>"form-checkbox", "id" => "checkbox_2"))?>
					        	<label for="checkbox_2"></label>
					        </div>
				    	</div>
				    	
				    	<div class="control-group">
				        	<label class="col-sm-5 control-label"><?php echo __("Hide left menu")?></label>
					        <div class="col-sm-7 checkbox">
					        	<?php echo checkbox_tag("page[option][hide_left_menu]",1,isset($page)?$page->getOption("hide_left_menu"):0,array("class"=>"form-checkbox", "id" => "checkbox_3"))?>
					        	<label for="checkbox_3"></label>
					        </div>
				    	</div>
				    	<div class="control-group">
				        	<label class="col-sm-5 control-label"><?php echo __("Template")?></label>
					        <div class="col-sm-7">
					        	<?php echo select_tag("page[option][template]","<option value=0>".__("Default")."</option>".options_for_select($templates,isset($page)?$page->getOption("template"):0),array("class"=>"form-control"))?>
					        </div>
				    	</div>
				   </fieldset>
				</div>
			</div>
		</div>
		
		
	</div>
</div>		
<?php echo input_hidden_tag("page[id]",isset($page)?$page->getId():"")?>
<?php echo input_hidden_tag("page[kind]",get_page_type_id())?>
<?php echo input_hidden_tag("page[language]",isset($page)?$page->getLanguage():$sf_request->getParameter("language"))?>
</form>



<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">
	 editor=CKEDITOR.replace( 'page_content', {});
	 CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;
	 
	 
	 <?php if(get_page_type_id()==4):?>

	 editor=CKEDITOR.replace( 'page_option_parametry', {});
	 CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	 editor=CKEDITOR.replace( 'page_option_wyposazenie', {});
	 CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	 editor=CKEDITOR.replace( 'page_option_ksztalty', {});
	 CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	 editor=CKEDITOR.replace( 'page_option_gwarancja', {});
	 CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	 editor=CKEDITOR.replace( 'page_option_akcesoria', {});
	 CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	 <?php endif?>
</script>
<?php end_slot() ?>

<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">
$(function() {
	   jQuery('#datetimepicker1').datetimepicker({
		   language: 'pl'
		});
		
});

</script>
<?php end_slot() ?>
