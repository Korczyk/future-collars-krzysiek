<?php sfContext::getInstance()->getResponse()->addStylesheet('plugins/jquery.nestable.css');?>
<?php sfContext::getInstance()->getResponse()->addJavascript('plugins/jquery.nestable.js');?>



<div class="clear">
<div class="cf nestable-lists">

        <div class="dd" id="nestable">
            <ol class="dd-list">
            	<?php foreach($flattenTree as $key=>$id): ?>
				<?php if($id!=0): ?>
					<?php if (isset($pages[$id])): ?>
						
						<?php $currentDeep = (count(explode(' ',$pages[$id]->getPath()))-1);?>
						
						<?php if(isset($flattenTree[$key+1])):?>
							<?php $nextDeep = (count(explode(' ',$pages[$flattenTree[$key+1]]->getPath()))-1);?>
						<?php else:?>
							<?php $nextDeep=0?>
						<?php endif?>
						
						<?php if(isset($flattenTree[$key-1])):?>
							<?php if($flattenTree[$key-1]):?>
								<?php $previousDeep = (count(explode(' ',$pages[$flattenTree[$key-1]]->getPath()))-1);?>
							<?php endif?>
						<?php else:?>
							<?php $previousDeep=0?>
						<?php endif?>
						
						
						<?php echo "<li class='dd-item' data-id='".$id."'>"?>
						<div class="dd-handle"><?php echo ($pages[$id]->getName() ? $pages[$id]->getName() : '<i>Brak tytułu strony</i>');?> 
							<div class="options-page">
								<span class="action dd-nodrag">
								<?php echo link_to(__("edit"),'@page_edit?name='.($sf_request->getParameter('name')).'&id='.$pages[$id]->getId())?>
								
								<?php echo link_to(__("remove"),'@page_remove?name='.($sf_request->getParameter('name')).'&id='.$pages[$id]->getId(),array("confirm"=>__("Do you confirm removal?")))?>
								
								</span>
							</div>
						</div>
				
						<?php if($currentDeep==$nextDeep):?>
							<?php echo "</li>"?>
						<?php elseif($currentDeep<$nextDeep):?>
							<?php echo "<ol class='dd-list'>"?>
						<?php elseif($currentDeep>$nextDeep):?>
							<?php echo str_repeat("</ol></li>", $currentDeep-$nextDeep)?>
						<?php endif?>
					<?php endif ?>
				<?php endif ?>
			<?php endforeach ?>
            </ol>
        </div>
   </div>
</div>


<?php append_to_slot('additional_javascript') ?>

<script type="text/javascript">
	$(window).load(function() {

		 var updateOutput = function(e)
		    {
			    
		        var list   = e.length ? e : $(e.target),
		            output = list.data('output');
		        if (window.JSON) {
		            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
		        } else {
		            output.val('JSON browser support required for this demo.');
		        }
		        

		    	$.post( "<?php echo url_for("@page_position_update")?>", { sortedList: list.nestable('serialize'),kind: <?php echo get_page_type_id()?> } ).done(function( data ) {
            	    
             	}); 
		    };

		    // activate Nestable for list 1
		    $('#nestable').nestable({
		        group: 1
		    })
		    .on('change', updateOutput);
		    
		 	// output initial serialised data
		    $('#nestable').data('output', $('#nestable-output'));
		    

		    $('#nestable-menu').on('click', function(e)
		    {
		        var target = $(e.target),
		            action = target.data('action');
		        if (action === 'expand-all') {
		            $('.dd').nestable('expandAll');
		        }
		        if (action === 'collapse-all') {
		            $('.dd').nestable('collapseAll');
		        }
		    });
	})
</script>
<?php end_slot() ?>
