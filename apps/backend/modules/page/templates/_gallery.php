<?php 
	$maxUp=ini_get( 'upload_max_filesize' );
	if(substr($maxUp,strlen($maxUp)-1,strlen($maxUp)) == 'M') $maxUp=str_replace('M','mb',$maxUp);
	if(substr($maxUp,strlen($maxUp)-1,strlen($maxUp)) == 'K') $maxUp=str_replace('M','kb',$maxUp);
	if(substr($maxUp,strlen($maxUp)-1,strlen($maxUp)) == 'G') $maxUp=str_replace('M','gb',$maxUp);
?>
<?php use_helper("sfThumbnail")?>

<div id="upload-container">
<div id="upload-files-buttons">
	<a href="" id="browse" class="btn btn-primary">Dodaj obrazki</a>
	<a id="uploadfiles" class="btn btn-primary" href="">Rozpocznij upload</a>
</div>

<span style="font-size: 10px;">Maksymalny rozmiar pliku: <?php echo strtoupper($maxUp);?></span>

<div class="upload-success-msg alert alert-success fade in">Wszystkie pliki zostały załadowane!</div>
<div class="remove-success-msg alert alert-success fade in">Wybrane zdjęcia zostały usunięte!</div>

<div class="errors-container"></div>

<div id="upload-content" style="display: none">
		<div id="upload-header">
			<div id="upload-header-filename">Nazwa pliku</div>
			<div id="upload-header-desc">Opis</div>
			<div id="upload-header-size">Rozmiar</div>
			<div id="upload-header-delete">Usuń</div>
		</div>
		<div id="files-list">
		</div>
	</div>
	
</div>
<div id="checked-all-actions" style="<?php if(empty($images)):?>display: none;<?php endif; ?>">
	<a href="" id="check-all" class="checker-state-button"><?php echo __("Check all"); ?></a>
	<a href="" id="delete-checked"><?php echo __("Remove selected"); ?></a>
</div>
<div class="images-list">
	<div class="cf nestable-lists">
        <div class="dd" id="nestable">
            <ol class="dd-list">
			<?php if(!empty($images)):?>
			<?php foreach($images as $key => $image):?>
				<li class='dd-item' data-id='<?php echo $image->getId()?>'>
					<div id="gall-img-<?php echo $image->getId();?>" class="dd-handle image-item image-item-<?php echo ($key%2 == 0 ? 'odd' : 'even');?> clear" style="">
						<div class="image-check">
							<input type="checkbox" name="image[]" class="image-checker" data-id="<?php echo $image->getId();?>" value="<?php echo $image->getId();?>" />
						</div>
						<div class="image-img">
							<?php echo thumbnail_tag(sfConfig::get("sf_upload_dir")."/images/".$image->getUrl(), 100, 100)?>
						</div>
						<div class="image-name dd-nodrag">
							<span class = "change-description-wrapper"><?php echo ($image->getDescription() ? $image->getDescription() : $image->getUrl());?></span>
							<a class = "change-description-trigger" data-type = "image" data-imageid = "<?php echo $image->getId();?>" href = ""><?php echo __("Change description");?></a>
						</div>
						<div class="image-actions dd-nodrag">
							<?php echo link_to(__("Remove"),"@image_remove?id=".$image->getId(),array("data-id"=>$image->getId(),"class" => "removeFromGallery"));?>
						</div>
					</div>
				</li>
			<?php endforeach; ?>
		<?php endif; ?>
			</ol>
		</div>
	</div>
</div>

<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">
	imageUploader('<?php echo $maxUp; ?>');
	imageDescriptionChanger();

	$(window).load(function() {

		 var updateOutput = function(e)
		    {
			    
		        var list   = e.length ? e : $(e.target),
		            output = list.data('output');
		        if (window.JSON) {
		            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
		        } else {
		            output.val('JSON browser support required for this demo.');
		        }
		        
				$.post( "<?php echo url_for("@image_position_update")?>", { sortedList: list.nestable('serialize'),id: <?php echo isset($page)?$page->getId():""?> } ).done(function( data ) {
            	    
             	});
		    	
            	    
             	
		    };

		    // activate Nestable for list 1
		    $('#nestable').nestable({
		        group: 1,
		        maxDepth: 1
		        
		    })
		    .on('change', updateOutput);
		    
		 	// output initial serialised data
		    $('#nestable').data('output', $('#nestable-output'));
		    

		    $('#nestable-menu').on('click', function(e)
		    {
		        var target = $(e.target),
		            action = target.data('action');
		        if (action === 'expand-all') {
		            $('.dd').nestable('expandAll');
		        }
		        if (action === 'collapse-all') {
		            $('.dd').nestable('collapseAll');
		        }
		    });
	})
</script>

<?php end_slot()?>