<?php 
	$maxUp=ini_get( 'upload_max_filesize' );
	if(substr($maxUp,strlen($maxUp)-1,strlen($maxUp)) == 'M') $maxUp=str_replace('M','mb',$maxUp);
	if(substr($maxUp,strlen($maxUp)-1,strlen($maxUp)) == 'K') $maxUp=str_replace('M','kb',$maxUp);
	if(substr($maxUp,strlen($maxUp)-1,strlen($maxUp)) == 'G') $maxUp=str_replace('M','gb',$maxUp);
?>

<div style="padding: 10px">
	<div id="attachements-container">
		<div id="attachements-upload-buttons">
			<a href="" id="browse-attachement" class="btn btn-primary">Dodaj pliki</a>
			<a id="upload-attachement" class="btn btn-primary" href="">Rozpocznij upload</a>
		</div>
		<span style="font-size: 10px;">Maksymalny rozmiar pliku: <?php echo strtoupper($maxUp);?></span>
		
		<div class="attachement-add-success-msg alert alert-success fade in">Wszystkie pliki zostały załadowane!</div>
		<div class="attachement-remove-success-msg alert alert-success fade in">Wybrane zdjęcia zostały usunięte!</div>
		
		<div class="attachements-errors-container"></div>
		
		<div id="attachements-content" style="display: none;">
			<div id="attachement-header">
				<div id="attachement-header-filename">Nazwa pliku</div>
				<div id="attachement-header-desc">Opis</div>
				<div id="attachement-header-size">Rozmiar</div>
				<div id="attachement-header-delete">Usuń</div>
			</div>
			<div id="attachements-list"></div>
		</div>
		
		<div id="attached-files-list">
			<?php if(!empty($attachements)):?>
				<?php foreach($attachements as $key => $attachement):?>
					<div id="attach-file-<?php echo $attachement->getId();?>" class="attach-item attach-item-<?php echo ($key%2 == 0 ? 'odd' : 'even');?> clear" style="">
						<div class="attach-check">
							<input type="checkbox" name="image[]" class="attach-checker" data-id="<?php echo $attachement->getId();?>" value="<?php echo $attachement->getId();?>" />
						</div>
						<div class="attach-name dd-nodrag">
							<span class="change-description-wrapper"><?php echo ($attachement->getDescription() ? $attachement->getDescription() : $attachement->getUrl());?></span>
							<a class="change-description-trigger" data-type="attachement" data-imageid="<?php echo $attachement->getId();?>" href=""><?php echo __("Change description");?></a>
						</div>
						<div class="attach-actions">
							<?php echo link_to(__("Remove"),"@attachement_remove?id=".$attachement->getId(),array("data-id"=>$attachement->getId(),"class" => "removeFromAttach"));?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</div>


<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">
	filesUploader('<?php echo $maxUp; ?>');
</script>
<?php end_slot()?>