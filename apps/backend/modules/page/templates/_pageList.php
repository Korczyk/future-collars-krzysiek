<?php sfContext::getInstance()->getResponse()->addStylesheet('plugins/jquery.nestable.css');?>

<ol class="dd-list">
<?php foreach($pages as $page):?>
	<li class="dd-item" data-id="<?php echo $page->getId()?>">
		<div class="dd-handle"><?php echo $page->getName()?> <span class="breadcrump"><?php echo $page->buildBreadcrump($parents)?></span>
		
		
		
		<div class="options-page">
			
			<span class="action">
				<a href = "<?php echo url_for('@page_edit?name='.($sf_request->getParameter('name')).'&id='.$page->getId());?>">edytuj</a>
				
				<a href = "<?php echo url_for('@page_remove?name='.($sf_request->getParameter('name')).'&id='.$page->getId());?>">usuń</a>
			</span>
		</div>
		</div>
		
	</li>
<?php endforeach?>
</ol>