<?php sfContext::getInstance()->getResponse()->addJavascript('/js/plugins/jquery-sortable.js');?>

<div class="row">
	<div class="col-lg-12 relative">
		<h2 class="page-title">
			<?php echo get_page_type_name()?> <small> <a class="add-item" href="<?php echo url_for("@page_add?name=".$sf_request->getParameter("name")."&language=".$sf_request->getParameter('language','pl'))?>"><i class="icon-icon-plus"></i></a></small>
		</h2>
		<div class="language-bar">
		<ul>
			<?php foreach(getAllowedCultures() as $language):?>
			<li><a href="<?php echo url_for('@page_index_language?language='.$language."&name=".$sf_request->getParameter("name"))?>"><img <?php if($sf_request->getParameter('language','pl')==$language):?>height="19" width="30" <?php else:?>height="12" width="19" <?php endif?> src="/images/backend/flags/<?php echo $language?>.png"/></a></li>
			<?php endforeach?>
		</ul>
	</div>
	</div>
</div>


<?php if(get_page_type_id()==1 or get_page_type_id()==3):?>
	<?php include_component("page", "pageTree",array("kind"=>"nested"))?>
<?php else:?>
	<?php include_component("page", "pageList")?>
<?php endif?>