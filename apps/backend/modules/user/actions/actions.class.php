<?php
class userActions extends sfActions {
	
	public function executeIndex($request) 
	{
		$c = new Criteria();
		if(!$this->getUser()->hasCredential("root")){
			$c->add(UserPeer::ROLE,1,Criteria::GREATER_THAN);
		}
		$users = UserPeer::doSelect($c);
		
		$roles = array(1 => 'Root', '2' => 'Administrator', '3' => 'Twórca treści');
		
		$this->roles = $roles;
		$this->users = $users;
	}
	
	public function executeLogin($request){
	
		if($this->getRequest()->getMethod()==sfRequest::POST){
			
			if($this->getRequestParameter("login") and $this->getRequestParameter("passwd")){
				
				$c = new Criteria;
				$c->add(UserPeer::EMAIL,$this->getRequestParameter('login'));
				$c->add(UserPeer::IS_DELETED,0);
				$user = UserPeer::doSelectOne($c);
				if($user instanceOf User){
					
					if(md5($user->getSalt().$this->getRequestParameter('passwd'))==$user->getPasswd()){
				
						$this->getUser()->setAuthenticated(true);
						$this->getUser()->addCredential('administrator');
						if($user->getRole()==1){
							$this->getUser()->addCredential('root');
							$this->getUser()->setAttribute("role", "Super Administrator");
						}
						elseif($user->getRole()==2){
							$this->getUser()->addCredential('admin');
							$this->getUser()->setAttribute("role", "Administrator");
						}
						elseif($user->getRole()==3){
							$this->getUser()->addCredential('redactor');
							$this->getUser()->setAttribute("role", "Redaktor");
						}
							
						$this->getUser()->setAttribute('name',$user->getName());
						$this->getUser()->setAttribute('id',$user->getId());
						$this->getUser()->setAttribute('email',$user->getEmail());
						return $this->redirect('@homepage');
					}
					else{
						return $this->redirect('@user_login');
					}
				}
				else{
					return $this->redirect('@user_login');
				}
			}
		}
	}
		

	
	public function executeLogout(){
		$this->getUser()->setAuthenticated(false);
		$this->getUser()->getAttributeHolder()->clear();
		$this->getUser()->clearCredentials();
		$this->redirect('@homepage');
	}
	
	public function executeAdd($request) 
	{
		/*$post = $this->getRequestParameter('createUser');
		
		if(!empty($post) && $post['name'] == 'ok')
		{
			$this->getUser()->setFlash('info', array('type' => 'ok', value => 'Nowy użytkownik został dodany!'));
			$this->redirect('@user_index');
		}
		else
		{
			$this->errors = true;
		}
		*/
	}
	
	
	public function validateEdit($request){
		$user = UserPeer::retrieveByPK($this->getRequestParameter("id"));
		
		if($this->getUser()->hasCredential("root")){
			return true;		
		}
		elseif(!$this->getUser()->hasCredential("root") and !($user->getRole()==1)){
			return true;
		}
		else{
			return $this->redirect("@user_index");
		}
	}

	public function executeEdit($request) 
	{
		$c = new Criteria();
		$c->add(UserPeer::ID,$this->getRequestParameter('id'));
		$user = UserPeer::doSelectOne($c);
		
		if(!($user instanceof User)){
			return $this->redirect404();
		}
		else{
			$this->user = $user;
		}
	
	}
	public function executeRemove($request) {
		
		$user = UserPeer::retrieveByPK($this->getRequestParameter("id"));
		
		if(!$this->getUser()->hasCredential("root") and ($user->getRole()==1)){
			
		}
		else{
			$user->delete();
		}
		return $this->redirect("@user_index");
	}
	

	public function executeUpdate($request) 
	{
		print_r($this->getUser()->getFlash("validation_erros"));
		
		//mamy zvalidowane i sprawdzone dane
		$this->user->fromArray($this->getRequestParameter('user'),BasePeer::TYPE_FIELDNAME);
		$this->user->save();
		
		$notifications = $this->getUser()->setFlash("notifications", array() );
		$notifications[]=array("success",$this->user->isNew()?"Użytkownik dodany":"Użytkownik zmodyfikowany",$this->user->isNew()?"Użytkownik został dodany pomyślnie":"Użytkownik został zmieniony pomyślnie",true);
		$this->getUser()->setFlash('notifications', $notifications);
		
		
		return $this->redirect("@user_index");
	}
	
	
	public function validateUpdate($request) 
	{
		if($this->getRequest()->getMethod() == sfRequest::POST)
		{
			$user = $this->getRequestParameter('user');
			
			if($user['id']){
				$this->user = UserPeer::retrieveByPK($user['id']);
				
				// dalej walidacja pól w tym powtarzalności email etc
				// jeśli błąd to formularz do flash i redirect do formularza $this->redirect("@user_edit") z uzupełnieniem danych else

				$validator = new Validation($user);
				$validator->addRule("email","email",array($user['email']))
				->addRule("passwd_confirm","equals",array($user['passwd'],$user['passwd_confirm']))
				->addRule("passwd","conditional_min_length",array($user['passwd'],6))
				->check();
				
				
				if($validator->getErrors()){
					
					return $this->redirect("@user_edit?id=".$this->user->getId());
				}
				else{
					return true;
				}
				
				
			}
			else{
				$this->user = new User();
				
				// dalej walidacja pól w tym powtarzalności email etc
				// jeśli błąd to formularz do flash i redirect return $this->redirect("@user_add"); do formularza z uzupełnieniem danych else
				//np. $this->setFlash("info_error","Wystąpił błąd");
				return true;
			}
			
		}
		else{
			return $this->redirect404();
		}	
	}
}
