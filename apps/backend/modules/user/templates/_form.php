<?php use_helper("Form")?>

<form class="form-horizontal" action = "<?php echo url_for('@user_update');?>" method = "post">
	<div class="row">
		<div class="col-lg-8">
			<div class="widget-container">
				<div class="widget-header">
					<h3><i class="icon-user"></i> <?php echo __("General information")?></h3>
					<i class = "icon-chevron-down hide-widget"></i>
				</div>
				
				<div class="widget-content">
					<div>
						<fieldset>
				   			<div class="control-group">
								<label class="col-sm-3 control-label"><?php echo __("First name and last name")?>:*</label>	
								<div class="col-sm-9">
									<?php echo input_tag('user[name]',(isset($user)?$user->getName():''),array("class"=>"form-control", "required"=>"required")); ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="col-sm-3 control-label">Adres email:*</label>	
								<div class="col-sm-9">
									<?php echo input_tag('user[email]',(isset($user)?$user->getEmail():''),array("class"=>"form-control", "required"=>"required", "type"=>"email")); ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="col-sm-3 control-label">Rola:*</label>	
								<div class="col-sm-9">
									<?php if(!$sf_user->hasCredential("root")):?>
										<?php echo select_tag('user[role]',options_for_select(array('2'=>'Administrator', '3'=>'Twórca treści'), isset($user)?$user->getRole():2), array("class"=>"form-control", "required"=>"required")); ?>
									<?php else:?>
										<?php echo select_tag('user[role]',options_for_select(array(1=>'Root', '2'=>'Administrator', '3'=>'Twórca treści'), isset($user)?$user->getRole():2), array("class"=>"form-control", "required"=>"required")); ?>
									<?php endif?>
								<!--  (isset($user)?$user->getRole():'') -->
									
								</div>
							</div>
							
							<div class="control-group">
								<label class="col-sm-3 control-label">Hasło:*</label>	
								<div class="col-sm-9">
									<?php echo input_password_tag('user[passwd]',null,array("class"=>"form-control","autocomplete"=>"off")); ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="col-sm-3 control-label">Potwierdź hasło:*</label>	
								<div class="col-sm-9">
									<?php echo input_password_tag('user[passwd_confirm]',null,array("class"=>"form-control","autocomplete"=>"off")); ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="col-sm-3 control-label">Dodatkowy komentarz:</label>	
								<div class="col-sm-9">
									<?php echo textarea_tag('user[description]',(isset($user)?$user->getDescription():''),array("class"=>"form-control", 'style'=>'height: 150px;')); ?>
								</div>
							</div>
							
							<div class="control-group" style = "margin-top: 3%; margin-bottom: 3%; text-align: center; ">
									<?php echo submit_tag(isset($user)?"Zapisz zmiany":"Zapisz",array('class'=>'btn btn-success'));?>
							</div>
							<?php echo input_hidden_tag('user[id]',(isset($user)?$user->getId():'')); ?>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>