<?php use_helper('Backend')?>
<div class="row">
	<div class="col-lg-12">
		<h2 class="page-title">
			Użytkownicy <small><a  class="add-item" href="<?php echo url_for("@user_add")?>"><i class="icon-icon-plus"></i></a></small>
		</h2>
	</div>
</div>

<?php $i=0; ?>
<?php if(!empty($users)):?>
	<table class="items-list" id="users-list">
		<tr class="header">
			<td>L.P.</td>
			<td>Imię i nazwisko</td>
			<td class="td-res-hide td-res-email">Email</td>
			<td class="td-res-hide td-res-role">Rola</td>
			<td class="td-res-hide">Dodatkowy komentarz</td>
			<td colspan="2">Akcje</td>
		</tr>
		<?php foreach($users as $user):?>
		<?php $i++; ?>
			<tr>
				<td style="width: 2%;"><?php echo $i; ?></td>
				<td><?php echo $user->getName()?></td>
				<td class="td-res-hide td-res-email"><?php echo $user->getEmail()?></td>
				<td class="td-res-hide td-res-role"><?php echo (array_key_exists($user->getRole(),$roles) ? $roles[$user->getRole()] : "");?></td>
				<td class="td-res-hide"><?php echo (strlen($user->getDescription()) ? $user->getDescription() : '');?></td>
				<td style="text-align: center;"><?php echo link_to('edytuj','@user_edit?id='.$user->getId(),array())?></td>
				<td style="text-align: center;"><?php echo link_to('usuń','@user_remove?id='.$user->getId(),array())?></td>
			</tr>
		<?php endforeach; ?>
	</table>
<?php else: ?>
	<p style="padding: 1%;">Aktualnie brak utworzonych użytkowników</p>
<?php endif; ?>
	