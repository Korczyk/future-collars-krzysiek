

<form class="form-horizontal" action = "<?php echo url_for('@user_login');?>" method = "post">
	<div class="row">
		<div style="margin: 50px auto; width: 300px">
			<div class="widget-container">
				<div class="widget-header" style="padding: 1px 30px 10px 30px; border-bottom: 0px;">
					<h2><i class="icon-icon-uzytkownik"></i> Logowanie</h2>
					
				</div>
				
				<div class="widget-content">
					<div>
						<fieldset>
				   			<div class="control-group">
								<label class="col-sm-4 control-label" style="width: 80px">Login</label>	
								<div class="col-sm-8">
									<?php echo input_tag('login',null,array("class"=>"form-control", "required" => "required")); ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="col-sm-4 control-label" style="width: 80px">Hasło</label>	
								<div class="col-sm-8">
									<?php echo input_password_tag('passwd',null,array("class"=>"form-control", "required" => "required", "type" => "email")); ?>
								</div>
							</div>
							<div class="control-group">
									
								<div class="col-sm-12 text-center" style="text-align: right;padding-right: 36px;">
									<?php echo submit_tag("Zaloguj",array("class"=>"btn btn-success"))?>		
								</div>
							</div>
						</fieldset>
						
						
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
</form>


						