<?php
class configActions extends sfActions {
	
	public function executeIndex($request){
		
		
		$frontend_yaml = (sfYaml::load(file_get_contents(sfConfig::get("sf_apps_dir")."/frontend/config/settings.yml")));
		$backend_yaml = (sfYaml::load(file_get_contents(sfConfig::get("sf_apps_dir")."/backend/config/settings.yml")));
		
		
		if($this->getRequest()->getMethod()==sfRequest::POST){
			
			//frontend yml
			
			//modules
			if(is_array($frontend_yaml['all']['.settings']['enabled_modules'])){
				foreach($frontend_yaml['all']['.settings']['enabled_modules'] as $key=>$value){
					if(substr($value, 0,3)=="csf"){
						unset($frontend_yaml['all']['.settings']['enabled_modules'][$key]);
					}
				}
			}
			
			foreach($this->getRequestParameter("enabled_modules",array()) as $key=>$value){
				$frontend_yaml['all']['.settings']['enabled_modules'][]=substr($key,0,-6);
			}
			
			$frontend_yaml['all']['.settings']['enabled_modules']=array_values($frontend_yaml['all']['.settings']['enabled_modules']);
			//helpers
			
			if(is_array($frontend_yaml['all']['.settings']['standard_helpers'])){
				foreach($frontend_yaml['all']['.settings']['standard_helpers'] as $key=>$value){
					if(substr($value, 0,3)=="csf"){
						unset($frontend_yaml['all']['.settings']['standard_helpers'][$key]);
					}
				}
			}
			
			foreach($this->getRequestParameter("enabled_modules",array()) as $key=>$value){
				if(file_exists(sfConfig::get("sf_plugins_dir")."/".$key."/lib/helper/".substr($key,0,-6)."Helper.php")){
					$frontend_yaml['all']['.settings']['standard_helpers'][]=substr($key,0,-6);
				}	
			}
			
			
			file_put_contents(sfConfig::get("sf_apps_dir")."/frontend/config/settings.yml", str_replace("'", "", sfYaml::dump($frontend_yaml,3)));
			
			//frontend yml
			
			//backend yml
			if(is_array($backend_yaml['all']['.settings']['enabled_modules'])){
				foreach($backend_yaml['all']['.settings']['enabled_modules'] as $key=>$value){
					if(substr($value, 0,3)=="csf"){
						unset($backend_yaml['all']['.settings']['enabled_modules'][$key]);
					}
				}
			}
				
			foreach($this->getRequestParameter("enabled_modules",array()) as $key=>$value){
				$backend_yaml['all']['.settings']['enabled_modules'][]=substr($key,0,-6);
			}
				
			$backend_yaml['all']['.settings']['enabled_modules']=array_values($backend_yaml['all']['.settings']['enabled_modules']);
				
			//helpers
				
			if(is_array($backend_yaml['all']['.settings']['standard_helpers'])){
				foreach($backend_yaml['all']['.settings']['standard_helpers'] as $key=>$value){
					if(substr($value, 0,3)=="csf"){
						unset($backend_yaml['all']['.settings']['standard_helpers'][$key]);
					}
				}
			}

			foreach($this->getRequestParameter("enabled_modules",array()) as $key=>$value){
				if(file_exists(sfConfig::get("sf_plugins_dir")."/".$key."/lib/helper/".substr($key,0,-6)."Helper.php")){
					$backend_yaml['all']['.settings']['standard_helpers'][]=substr($key,0,-6);
				}	
			}
			
			file_put_contents(sfConfig::get("sf_apps_dir")."/backend/config/settings.yml", str_replace("'", "", sfYaml::dump($backend_yaml,3)));
			
			foreach($this->getRequestParameter("enabled_modules",array()) as $key=>$value){
				Utils::installPlugin(substr($key,0,-6));
			}
			
			
			//backend yml
			
		}
	    
		
		
		//$this->enabled_modules=(is_array($frontend_yaml)?$frontend_yaml['all']['.settings']['enabled_modules']:array());
		//$this->enabled_modules_backend=(is_array($backend_yaml)?$backend_yaml['all']['.settings']['enabled_modules']:array());
		
		$enabled_modules=array();
		foreach($frontend_yaml['all']['.settings']['enabled_modules'] as $key=>$value){
			if(substr($value, 0,3)=="csf"){
				$enabled_modules[$value]=$value;
			}
		}
		foreach($backend_yaml['all']['.settings']['enabled_modules'] as $key=>$value){
			if(substr($value, 0,3)=="csf"){
				$enabled_modules[$value]=$value;
			}
		}
		
		
		$this->enabled_modules=$enabled_modules;
		
		
		if ($handle = opendir(sfConfig::get("sf_plugins_dir"))) {
			while (false !== ($entry = readdir($handle))) {
				if(substr($entry, 0,3)=="csf"){
					$plugins[]=$entry;
				}
			}
			closedir($handle);
			$this->plugins = $plugins;
			
		}
		
		
		if($this->getRequest()->getMethod()==sfRequest::POST){
			if($this->getRequestParameter("config")){
				foreach($this->getRequestParameter("config") as $key=>$value){
					
					$c = new Criteria;
					$c->add(ConfigPeer::NAME,$key."_".$this->getRequestParameter("language"));
					$config = ConfigPeer::doSelectOne($c);
					if($config instanceof Config){
						
					}
					else{
						$config = new Config;
					}
					
					$config->setName($key."_".$this->getRequestParameter("language"));
					$config->setValue($value);
					$config->save();
				
				}
			}
		}
		
		$config = new Config;
		$this->config = $config;
		
	}
}
