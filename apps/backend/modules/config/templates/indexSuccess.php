<div class="row">
	<div class="col-lg-12">
		<h2 class="page-title">
			Ustawienia
		</h2>
		<div class="language-bar">
			<ul>
				<?php foreach(getAllowedCultures() as $language):?>
				<li><a href="<?php echo url_for('@config_language?language='.$language)?>"><img <?php if($sf_request->getParameter('language','pl')==$language):?>height="19" width="30" <?php else:?>height="12" width="19" <?php endif?> src="/images/backend/flags/<?php echo $language?>.png"/></a></li>
				<?php endforeach?>
			</ul>
		</div>
	</div>
</div>
	
<ul class="nav nav-tabs">
	<li class="active">
		<a href="#general-info" data-toggle="tab"><?php echo __("General information")?></a>
	</li>
	
	
	<?php foreach($enabled_modules as $module):?>
		<?php if(substr($module, 0,3)=="csf"):?>
			<?php if(file_exists(sfConfig::get("sf_plugins_dir")."/".$module."Plugin/modules/".($module)."/templates/_form.php")):?>
			<li><a href="#<?php echo $module?>" data-toggle="tab">
		    	<?php echo __($module)?></a>
		    </li>
		   <?php endif?>
		<?php endif?>
	<?php endforeach?>
	
	
	
	
   
</ul>

<div class="tab-content" id="nav-tab">
	<div class="tab-pane active" id="general-info">
		<form method="post">
			<div class="widget-content">
				<div>
					<fieldset>
				   		<div class="control-group">
				        	<label class="col-sm-2 control-label"><?php echo __("Main email")?></label>
					        <div class="col-sm-4">
					        	<?php echo input_tag("config[email]",(isset($config)?$config->getOption("email",$sf_request->getParameter('language','pl')) : null),array("class"=>"form-control"))?>
					        </div>
					    </div>
					 </fieldset>
					 <?php if($sf_user->hasCredential("root")):?>
					 <fieldset>
				   		<div class="control-group">
				        	<label class="col-sm-2 control-label"><?php echo __("Enabled modules")?></label>
					        <div class="col-sm-4">
					        	<table>
					        		<tr>
					        			<td>
					        				
				        					<?php foreach($plugins as $plugin):?>
								        		<div class="checkbox">
								        		<?php echo checkbox_tag("enabled_modules[".$plugin."]",1,(in_array(substr($plugin,0,-6), $enabled_modules)))?> <label for="<?php echo 'enabled_modules_'.$plugin?>"><?php echo substr($plugin,0,-6)?></label></div><br/>
								        	<?php endforeach?>
					        			</td>
					        			
					        		</tr>
					        	
					        	</table>
					        	
					        	
					        </div>
					    </div>
					 </fieldset>
					 <?php endif?>
					 <input type="submit" name="submit" value="Zapisz" class="btn btn-success" style="margin-bottom: 10px">
					 
				</div>
			</div>
		
		<?php echo input_hidden_tag("language",$sf_request->getParameter("language","pl"))?>
		</form>
	</div>
	
	
	<?php foreach($enabled_modules as $module):?>
		<?php if(substr($module, 0,3)=="csf"):?>
			<?php if(file_exists(sfConfig::get("sf_plugins_dir")."/".$module."Plugin/modules/".$module."/templates/_form.php")):?>
			<div class="tab-pane " id="<?php echo $module?>">
				<?php $partial = $module."/form"?>
				<?php include_partial($partial)?>
		   </div>
		   <?php endif?>
		<?php endif?>
	<?php endforeach?>
	
		
</div>
