<?php


class csfCultureFilter extends sfFilter
{
	public function execute($filterChain)
	{
		$request = $this->getContext()->getRequest();
		$user    = $this->getContext()->getUser();
		$controller = $this->getContext()->getController();
		$action = sfContext::getInstance()->getActionStack()->getLastEntry();

		$path_info = (isset($_SERVER["PATH_INFO"])?$_SERVER["PATH_INFO"]:"");
		switch(substr($path_info,1,2)){
			
			case "en" : {
				$locale = "en_US";
				break;
			}
			case "pl" : {
				$locale = "pl_PL";
				break;
			}
			case "de" : {
				$locale = "de_DE";
				break;
			}
			default : {
				$locale = "pl_PL";
				break;
			}
		}
		
		$user->setCulture($locale);
		
		$filterChain->execute();
		

	}
}




?>