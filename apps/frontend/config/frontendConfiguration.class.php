<?php
require_once (dirname ( __FILE__ ) . '/../../../plugins/ysfDimensionsPlugin/lib/config/ysfApplicationConfiguration.class.php');
class frontendConfiguration extends ysfApplicationConfiguration {
	public function configure() {
		
		
		switch(substr($_SERVER["REQUEST_URI"],1,2)){
			case "en" : {
				$culture = "en";
				break;
			}
			case "pl" : {
				$culture = "pl";
				break;
			}
			case "de" : {
				$culture = "de";
				break;
			}
			default : {
				$culture = "pl";
				break;
			}	
		}
		
		
		
		
		$this->setDimension (array(
			"culture" => $culture 
		));
		
		parent::setup ();
	}
}
