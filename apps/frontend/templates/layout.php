<!DOCTYPE html>
<html>
	<head>
		<?php include_http_metas() ?>
		<?php include_metas() ?>
		<?php include_stylesheets()?>
		<?php include_title() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


		<!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
		<meta property="og:site_name" content="" /> <!-- website name -->
		<meta property="og:site" content="" /> <!-- website link -->
		<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
		<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
		<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
		<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
		<meta property="og:type" content="article" />

	    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
	    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
	    <link href="css/front/bootstrap.css" rel="stylesheet">
	    <link href="css/front/fontawesome-all.css" rel="stylesheet">
	    <link href="css/front/swiper.css" rel="stylesheet">
		<link href="css/front/magnific-popup.css" rel="stylesheet">
		<link href="css/front/styles.css" rel="stylesheet">
		<link href="icomoon/style.css" rel="stylesheet">
		<link href="css/front/own-css/lib/selectric.min.css" rel="stylesheet">
		<link href="css/front/own-css/lib/modal.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="css/front/own-css/main.css">
		
	</head>
	<body class="<?php include_slot('body_classes') ?>"  data-spy="scroll" data-target=".fixed-top">
		<?php echo $sf_content ?>
	<?php include_javascripts()?>
	<script src="js/front/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/front/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/front/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/front/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/front/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/front/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/front/morphext.min.js"></script> <!-- Morphtext rotating text in the header -->
    <script src="js/front/own-js/lib/jquery.selectric.min.js"></script> <!-- Custom scripts -->
    <script src="js/front/own-js/lib/modal.min.js"></script> <!-- Custom scripts -->
    <script src="js/front/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/front/scripts.js"></script> <!-- Custom scripts -->
    <script src="js/front/own-js/main.js"></script> <!-- Custom scripts -->
	<?php if (has_slot('additional_javascript')) echo get_slot('additional_javascript') ?>
	</body>
</html>
