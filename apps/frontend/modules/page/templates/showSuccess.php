<?php sfContext::getInstance()->getResponse()->setTitle($page->getOption("meta_title")?($page->getOption("meta_title")." | ".$sf_request->getHost()):($page->getName()." | ".$sf_request->getHost()))?>
<?php sfContext::getInstance()->getResponse()->addMeta('description', $page->getOption("meta_description"))?>
<?php sfContext::getInstance()->getResponse()->addMeta('keywords', $page->getOption("meta_keywords"))?>



<?php include_partial("localizer",array("page"=>$page))?>

<div class="clear">
	<?php if(!$page->getOption("hide_left_menu")):?>
	<div class="sidebar-wrapper">
		<div class="sidebar">
			<?php include_component("page", "menuSidebar",array("page"=>$page))?>
		</div>
	</div>
	<?php endif?>
	<div class="main-wrapper <?php if($page->getOption("hide_left_menu")):?>wide<?php endif?>">
		
		<div class="main">
			<h1><?php echo $page->getName()?></h1>
			<?php echo $page->getContent()?>	
			<?php include_component("page", "gallery",array("page"=>$page))?>
			<?php include_component("page", "attachements",array("page"=>$page))?>
		</div>
	</div>
</div>
<?php slot("body_classes")?>
<?php foreach($page->getBreadcrumps() as $breadcrump):?><?php echo $breadcrump['slug']?> <?php endforeach?>
<?php end_slot()?>
