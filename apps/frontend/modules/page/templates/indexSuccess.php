<?php sfContext::getInstance()->getResponse()->setTitle($page->getOption("meta_title")?$page->getOption("meta_title"):($sf_request->getHost()))?>
<?php sfContext::getInstance()->getResponse()->addMeta('description', $page->getOption("meta_description"))?>
<?php sfContext::getInstance()->getResponse()->addMeta('keywords', $page->getOption("meta_keywords"))?>


    
    
    
<?php slot("body_classes")?>
homepage
<?php end_slot()?>




<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
        <div class="container">
            <a class="navbar-brand logo-image" href="index.html"><img src="images/SVG/Group%20598.svg" alt="alternative"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-awesome fas fa-bars"></span>
                <span class="navbar-toggler-awesome fas fa-times"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#home-section">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#about-section">O akcji</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#consultation-section">Konsulatacje</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#story-transformation-section">Przemiany</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#motivation-section">Motywacja</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#knowledge-base-section">Video</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#partners-section">Partnerzy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#guide-section">Poradnik</a>
                    </li>
                </ul>
                <span class="nav-item social-icons">
                <span class="fa-stack">
                    <a href="#your-link">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fab fa-facebook-f fa-stack-1x"></i>
                    </a>
                </span>
                <span class="fa-stack">
                    <a href="#your-link">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fab fa-twitter fa-stack-1x"></i>
                    </a>
                </span>
            </span>
            </div>
        </div>

    </nav>

    <header id="home-section" class="header section">
        <div class="header-content">
            <div class="container">
                <div class="swiper-container swiper-home" id="js-swiper-home">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide row row-reset">
                            <div class="col-lg-5">
                                <div class="text-container">
                                    <p class="powered-by">Powered by <img src="images/PNG/Image%2044.png" srcset="images/PNG/Image%2044@2x.png" alt="image"></p>
                                    <h1>Woman Update</h1>
                                    <p class="medium-paragraph">Na zmianę obrazu nigdy nie jest za późno. Wyjdź poza ramy i zdobądź nowe kwalifikacje! Tak dłużej być nie może! Mona Lisa, Dama z Łasiczką, Dziewczyna z Perłą i Wenus z Milo już to zrobiły.</p>
                                    <a href="#consultation-section" class="btn btn-bg nav-link page-scroll"><div class="btn__internal"><span class="btn__label">Aktualizuj obraz</span><i class="btn__icon icon-right"></i></div></a>
                                </div>
                            </div>
                            <figure class="col-lg-7">
                                <img src="images/PNG/Mask%20Group%2023.png" srcset="images/PNG/Mask%20Group%2023@2x.png" alt="image">
                            </figure>
                        </div>
                        <div class="swiper-slide row row-reset">
                            <div class="col-lg-5">
                                <div class="text-container">
                                    <p class="powered-by">Powered by <img src="images/PNG/Image%2044.png" srcset="images/PNG/Image%2044@2x.png" alt="image"></p>
                                    <h1>Darmowe konsultacje</h1>
                                    <p class="medium-paragraph">Nasz doradca ds. zmiany i zawodów przyszłości pomoże Ci w przejściu przez aktualizację.</p>
                                    <a href="#consultation-section" class="btn btn-bg nav-link page-scroll"><div class="btn__internal"><span class="btn__label">Aktualizuj obraz</span><i class="btn__icon icon-right"></i></div></a>
                                </div>
                            </div>
                            <figure class="col-lg-7">
                                <img src="images/PNG/Mask%20Group%2024.png" srcset="images/PNG/Mask%20Group%2024@2x.png" alt="image">
                            </figure>
                        </div>
                        <div class="swiper-slide row row-reset">
                            <div class="col-lg-5">
                                <div class="text-container">
                                    <p class="powered-by">Powered by <img src="images/PNG/Image%2044.png" srcset="images/PNG/Image%2044@2x.png" alt="image"></p>
                                    <h1>Chcesz się przebranżowić?</h1>
                                    <p class="medium-paragraph">Pobierz poradnik “Zmień swój obraz z Mona Lisą” i dowiedz się, od czego zacząć</p>
                                    <a href="#consultation-section" class="btn btn-bg nav-link page-scroll"><div class="btn__internal"><span class="btn__label">Pobierz poradnik</span><i class="btn__icon icon-right"></i></div></a>
                                </div>
                            </div>
                            <figure class="col-lg-7">
                                <img src="images/PNG/Mask%20Group%2025.png" srcset="images/PNG/Mask%20Group%2025@2x.png" alt="image">
                            </figure>
                        </div>
                        <div class="swiper-slide row row-reset">
                            <div class="col-lg-5">
                                <div class="text-container">
                                    <p class="powered-by">Powered by <img src="images/PNG/Image%2044.png" srcset="images/PNG/Image%2044@2x.png" alt="image"></p>
                                    <h1>Cytaty znanych kobiet</h1>
                                    <p class="medium-paragraph">Pobierz poradnik “Zmień swój obraz z Mona Lisą” i dowiedz się, od czego zacząć</p>
                                    <a href="#consultation-section" class="btn btn-bg nav-link page-scroll"><div class="btn__internal"><span class="btn__label">Pobierz poradnik</span><i class="btn__icon icon-right"></i></div></a>
                                </div>
                            </div>
                            <figure class="col-lg-7">
                                <img src="images/PNG/Mask%20Group%2027.png" srcset="images/PNG/Mask%20Group%2027@2x.png" alt="image">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="group-swiper">
                    <div class="swiper-button-prev">
                        <div class="icon-left-slide">
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-next">
                        <div class="icon-right-slide">
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="social-media">
            <div class="container">
                <div class="row">
                    <div class="social-media__internal">
<!--                        <a href="https://facebook.com" class="text-center facebook"><img src="images/SVG/Facebook_Logo_(2019).svg" alt="facebook"></a>-->
                        <a href="https://facebook.com" class="text-center facebook"></a>
<!--                        <a href="https://instagram.com" class="text-center instagram"><img src="images/SVG/Instagram_logo.svg" alt="instagram"></a>-->
                        <a href="https://instagram.com" class="text-center instagram"></a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="about-section" class="section">
        <div class="container">
            <div class="row">
                <div class="about-section__nav-tabs col-lg-12">
                    <h2 class="">O akcji</h2>
                    <ul class="nav nav-tabs col-lg-10 col-md-12" id="about-tabs">
                        <li class="nav-item col-lg-4 col-md-6"><a href="#about-tab-1" role="tab" data-toggle="tab" class="nav-link text-center active"><span class="label">Co to jest Woman Update?</span></a></li>
                        <li class="nav-item col-lg-4 col-md-6"><a href="#about-tab-2" role="tab" data-toggle="tab" class="nav-link text-center"><span class="label">Dla kogo jest Woman Update?</span></a></li>
                        <li class="nav-item col-lg-4 col-md-6"><a href="#about-tab-3" role="tab" data-toggle="tab" class="nav-link text-center"><span class="label">Korzyści</span></a></li>
                    </ul>
                </div>
                <div class="about-section__nav-content  tab-content col-lg-12" id="about-tabs-content">
                    <div class="tab-pane fade show active" id="about-tab-1" role="tabpanel" aria-labelledby="about-tab-1">
                        <div class="row">
                            <div class="col-lg-5">
                                <h3>Co to jest Woman Update?</h3>
                                <p class="medium-paragraph">Woman Update to akcja, która ma na celu zachęcenie kobiet do przebranżowienia się – zaktualizowania swojego obrazu.</p>
                                <p class="regular-paragraph">Technologia jest tu i teraz. KAŻDA kobieta ma szansę na zdobycie kompetencji cyfrowych - nie ma znaczenia doświadczenie czy wiek a nawet… epoka. Kampania ma pokazać, że kluczem do bezpiecznej przyszłości, stabilnej pracy czy dobrych zarobków jest ciągły rozwój i zdobywanie nowych umiejętności.</p>
                            </div>
                            <div class="col-lg-7">
                                <div class="video-container video-wrapper">
                                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=fLCjQJCekTs" data-effect="fadeIn">
                                        <img src="images/PNG/ban-1.png" srcset="images/PNG/ban-1@2x.png" alt="image">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="about-tab-2" role="tabpanel" aria-labelledby="about-tab-2">
                        <div class="row">
                            <figure class="person col-lg-4">
                                <img src="images/PNG/woman-pic-1.png" srcset="images/PNG/woman-pic-1@2x.png"  class="person__favicon" alt="image">
                                <figcaption class="text-center figcaption">
                                    <p class="main__label">Wiek nie ma znaczenia!</p>
                                    <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                    <p class="description">Czujesz, że w Twoim wieku nowa kariera to głupi pomysł? Wiek to tylko rama, z której możesz wyjść w każdej chwili. Woman Update pomoże Ci w tym.</p>
                                </figcaption>
                            </figure>
                            <figure class="person col-lg-4">
                                <img src="images/PNG/woman-pic-2.png" srcset="images/PNG/woman-pic-2@2x.png"  class="person__favicon" alt="image">
                                <figcaption class="text-center figcaption">
                                    <p class="main__label">Mamie nie wypada?</p>
                                    <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                    <p class="description">Urlop macierzyński to doskonały czas na zdobycie nowych kwalifikacji. Przeprowadź aktualizację tak, jak chcesz: na swoich warunkach, dla siebie.</p>
                                </figcaption>
                            </figure>
                            <figure class="person col-lg-4">
                                <img src="images/PNG/woman-pic-3.png" srcset="images/PNG/woman-pic-3@2x.png"  class="person__favicon" alt="image">
                                <figcaption class="text-center figcaption">
                                    <p class="main__label">Na nową pracę nigdy nie jest za późno!</p>
                                    <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                    <p class="description">Czujesz, że Twoje miejsce jest gdzie indziej? Masz wrażenie, że stoisz w miejscu? Zalecamy aktualizację kariery. Z Woman Update to możliwe.</p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="about-tab-3" role="tabpanel" aria-labelledby="about-tab-3">
                        <div class="row">
                            <div class="col-lg-4">
                                <ul class="list-of-benefits to--left">
                                    <li class="medium-paragraph">Dowiesz się, jak zmienić ścieżkę kariery.</li>
                                    <li class="medium-paragraph">Uwierzysz, że możesz się przebranżowić.</li>
                                    <li class="medium-paragraph">Wiek, wykształcenie czy sytuacja rodzinna przestaną być dla Ciebie przeszkodami</li>
                                    <li class="medium-paragraph">Odkryjesz nowe ścieżki kariery i miejsca pracy.</li>
                                </ul>
                            </div>
                            <div class="col-lg-4 image-container">
                                <img src="images/PNG/Mask%20Group%2035.png" srcset="images/PNG/Mask%20Group%2035@2x.png" alt="image">
                            </div>
                            <div class="col-lg-4">
                                <ul class="list-of-benefits to--right">
                                    <li class="medium-paragraph">Poznasz kobiety, które wyszły poza ramy i osiągnęły sukces.</li>
                                    <li class="medium-paragraph">Nabierzesz odwagi i wiary w swoje możliwości.</li>
                                    <li class="medium-paragraph">Staniesz się inspiracją dla innych – zupełnie jak Mona Lisa zy Wenus z Milo.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="transformation-section" class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h2>Przejdź przemianę z Mona Lisą</h2>
                    <p class="medium-paragraph">Przed jakimi wyzwaniami stanęła Mona Lisa? Jak zmieniła swój obraz? Pobierz nasz poradnik i dowiedz się, jak wyjść poza ramy przeszłości.</p>
                    <ul class="list-of-benefits to--left">
                        <li class="medium-paragraph">Dowiesz się, jak zmienić ścieżkę kariery.</li>
                        <li class="medium-paragraph">Uwierzysz, że możesz się przebranżowić.</li>
                        <li class="medium-paragraph">Wiek, wykształcenie czy sytuacja rodzinna przestaną być dla Ciebie przeszkodami.</li>
                        <li class="medium-paragraph">Odkryjesz nowe ścieżki kariery i miejsca pracy.</li>
                    </ul>
                    <form id="transformation-form" data-toggle="validator" data-focus="false" class="form">
                        <div class="form-message">
                            <div id="cmsgSubmit__top" class="h3 hidden"></div>
                        </div>
                        <div class="form-group with-star">
                            <div class="form-control-input__parent">
                                <input type="email" class="form-control-input" id="cguide_adress_email" name="form_top[guide_adress_email]" required placeholder="Adres e-mail">
                            </div>
                            <div class="help-block with-errors"></div>
                            <p class="help-block help-block__custom form-text-error">Na podany adres e-mail zostanie wysłany plik z poradnikiem.</p>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-bg"><div class="btn__internal"><span class="btn__label">Pobierz poradnik</span><i class="btn__icon icon-right"></i></div></button>
                        </div>
<!--                        <div class="form-message">-->
<!--                            <div id="cmsgSubmit" class="h3 text-center hidden"></div>-->
<!--                        </div>-->

                    </form>
                </div>
                <figure class="col-lg-6">
                    <img src="images/PNG/book.png" srcset="images/PNG/book@2x.png" alt="alt" class="img-fluid">
                </figure>
            </div>
        </div>
    </div>

    <div id="story-transformation-section" class="section">
        <div class="container">
            <div class="row">
                <h2 class="col-lg-12">Historie przemian</h2>
                <div class="swiper-container swiper-story-transformation" id="js-swiper-story-transformation">
                    <div class="swiper-wrapper">
                        <?php for ($i=0;$i<3;$i++) { ?>
                        <div class="swiper-slide row row-reset">
                            <figure class="col-lg-5 col-md-5 image-container">
                                <img src="images/PNG/history-1.png" srcset="images/PNG/history-1@2x.png" alt="image">
                            </figure>
                            <div class="col-lg-7 col-md-12 description-container">
                                <p class="semibold-paragraph">Joanna Kowalska</p>
                                <p class="regular-paragraph">Początek traktatu czasu być przez moralne i darować. Albowiem samowiedza pokazuje się wszelkiej mądrości i niezawisłe od Dobra, ale ta realność, która karami grozi, nie inna jaka istota. Więc zło dopiero po usunięciu wszelkich moralnych ustaw światem był w reszcie niemamy żadnego występku albo pijanemu mogą komu zdawało, jak wysługę, jak pewna nagroda niema być osobą, a jednak znajdującemi się przygody na. świat był ideał pomyśleć, gdzie nam granice naszego ku dobremu nadane; ale rzeczy tę szczęśliwość czyli wysługę. Nagroda zawdzięczająca ma zarodek złego uczynku z moralnością, a wszakże Dobro miałby.</p>
                                <div class="video-container video-wrapper">
                                    <a href="https://www.youtube.com/watch?v=fLCjQJCekTs" class="btn btn-outline popup-youtube" data-effect="fadeIn"><div class="btn__internal"><span class="btn__label">Obejrzyj film</span><i class="btn__icon icon-play"></i></div></a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="group-swiper">
                <div class="swiper-button-prev">
                    <div class="icon-left-slide">
                        <span class="path2"></span>
                        <span class="path3"></span>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next">
                    <div class="icon-right-slide">
                        <span class="path2"></span>
                        <span class="path3"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="motivation-section" class="slider-1 section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-center">Dasz radę kobieto!</h2>
                    <div class="slider-container">
                        <div class="swiper-container card-slider swiper-card swiper-motivation" id="js-swiper-motivation">
                            <div class="swiper-wrapper">
                                <?php for ($i=0;$i<2;$i++) { ?>
                                    <div class="swiper-slide">
                                        <figure class="person">
                                            <img src="images/PNG/shutterstock_566714665.png" srcset="images/PNG/shutterstock_566714665@2x.png"  class="person__favicon" alt="image">
                                            <figcaption class="text-center figcaption">
                                                <p class="main__label">Wiek nie ma znaczenie!</p>
                                                <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                                <p class="description">Czujesz, że w Twoim wieku nowa kariera to głupi pomysł? Wiek to tylko rama, z której możesz wyjść w każdej chwili. Woman Update pomoże Ci w tym.</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="person">
                                            <img src="images/PNG/shutterstock_1673103022.png" srcset="images/PNG/shutterstock_1673103022@2x.png"  class="person__favicon" alt="image">
                                            <figcaption class="text-center figcaption">
                                                <p class="main__label">Wiek nie ma znaczenie!</p>
                                                <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                                <p class="description">Czujesz, że w Twoim wieku nowa kariera to głupi pomysł? Wiek to tylko rama, z której możesz wyjść w każdej chwili. Woman Update pomoże Ci w tym.</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="person">
                                            <img src="images/PNG/shutterstock_1689404020.png" srcset="images/PNG/shutterstock_1689404020@2x.png" class="person__favicon" alt="image">
                                            <figcaption class="text-center figcaption">
                                                <p class="main__label">Wiek nie ma znaczenie!</p>
                                                <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                                <p class="description">Czujesz, że w Twoim wieku nowa kariera to głupi pomysł? Wiek to tylko rama, z której możesz wyjść w każdej chwili. Woman Update pomoże Ci w tym.</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-button-next icon-right-slide-2"></div>
                            <div class="swiper-button-prev icon-left-slide-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="online-lessons-section" class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Bezpłatne lekcje online dla kobiet, które chcą coś zmienić</h2>
                    <p class="medium-paragraph">Zrób pierwszy krok w zmianie swojego obrazu i skorzystaj z bezpłatnych lekcji online.</p>
                    <a href="#consultation-section" class="btn btn-bg nav-link page-scroll"><div class="btn__internal"><span class="btn__label">Sprawdź</span><i class="btn__icon icon-right"></i></div></a>
                </div>
                <figure class="col-lg-6">
                    <img src="images/PNG/Group%20585.png" srcset="images/PNG/Group%20585@2x.png" alt="alt" class="img-fluid">
                </figure>
            </div>
        </div>
    </div>

    <div id="knowledge-base-section" class="section">
        <div class="container">
            <div class="row">
                <h2>Baza wiedzy</h2>
                <div class="list-of-person row row-reset">
                    <?php for ($j=0;$j<3;$j++) { ?>
                        <?php for ($i=0; $i<3;$i++) { ?>
                            <figure class="person col-lg-4 col-md-6">
                                <div class="video-container video-wrapper">
                                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=fLCjQJCekTs" data-effect="fadeIn">
                                        <img src="images/PNG/Component<?php echo $i; ?>.png" srcset="images/PNG/Component<?php echo $i; ?>@2x.png" class="person__favicon" alt="image">
                                        <!--                                <span class="video-play-button icon-play-ban"></span>-->
                                    </a>
                                </div>
                                <figcaption class="text-center figcaption">
                                    <p class="main__label">Zaktualizuj obraz</p>
                                    <p class="description">Chcesz zaktualizować swój obraz? Nasz doradca ds.</p>
                                </figcaption>
                            </figure>
                        <?php } ?>
                    <?php } ?>
                </div>

                <div class="show-more">
                    <div class="show-more__internal" id="js-knowledge-base-show-more">
                        <span class="label semibold-paragraph">Pokaż więcej materiałów</span><span class="icon icon-down"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="partners-section" class="section">
        <div class="container">
            <div class="row">
                <h2 class="col-lg-12">Wspieramy kobiety w ich przemianie</h2>
                <p class="medium-paragraph col-lg-12">Chcesz zaktualizować swój obraz? Nasz doradca ds. zmiany i zawodów przyszłości wskaże Ci drogę.</p>
                <div class="swiper-container swiper-partners" id="js-swiper-partners">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img src="images/PNG/Image%2039.png" srcset="images/PNG/Image%2039@2x.png" alt="partner">
                        </div>
                        <div class="swiper-slide">
                            <img src="images/PNG/Image%2040.png" srcset="images/PNG/Image%2040@2x.png" alt="partner">
                        </div>
                        <div class="swiper-slide">
                            <img src="images/PNG/Image%2041.png" srcset="images/PNG/Image%2041@2x.png" alt="partner">
                        </div>
                        <div class="swiper-slide">
                            <img src="images/PNG/Image%2042.png" srcset="images/PNG/Image%2042@2x.png" alt="partner">
                        </div>
                        <div class="swiper-slide">
                            <img src="images/PNG/Image%2043.png" srcset="images/PNG/Image%2043@2x.png" alt="partner">
                        </div>
                    </div>
                </div>
                <a href="#consultation-section" class="btn btn-outline nav-link page-scroll"><div class="btn__internal"><span class="btn__label">Sprawdź</span><i class="btn__icon icon-right"></i></div></a>
            </div>
        </div>
    </div>

    <div id="guide-section" class="section">
        <div class="container">
            <div class="row">
                <h2 class="col-lg-12">Najczęściej zadawane pytania</h2>
                <ul class="list-of-questions col-lg-12">
                    <?php for ($i=0;$i<5;$i++) { ?>
                    <li class="question <?php if($i==1):?>active<?php endif; ?>">
                        <h4>Mam 30, 40, 50 lat i więcej. Czy nie jestem za stara na przebranżowienie się?<div class="icon-toggle icon-down"></div></h4>
                        <div class="row">
                            <div class="col-lg-9 col-md-10 col-sm-12">
                                <p class="regular-paragraph">Początek traktatu czasu być przez moralne i darować. Albowiem samowiedza pokazuje się wszelkiej mądrości i niezawisłe od Dobra, ale ta realność, która karami grozi, nie inna jaka istota. Więc zło dopiero po usunięciu wszelkich moralnych ustaw światem był w reszcie niemamy żadnego występku albo pijanemu mogą komu zdawało, jak wysługę, jak pewna nagroda niema być osobą, a jednak znajdującemi się przygody na. świat był ideał pomyśleć, gdzie nam granice naszego ku dobremu nadane; ale rzeczy tę szczęśliwość czyli wysługę. Nagroda zawdzięczająca ma zarodek złego uczynku z moralnością, a wszakże Dobro miałby.Początek traktatu czasu być przez moralne i darować. Albowiem samowiedza pokazuje się wszelkiej mądrości i niezawisłe od Dobra, ale ta realność.</p>
                            </div>
<!--                            <div class="col-lg-5 col-md-6 col-sm-12">-->
<!--                                <div class="video-container video-wrapper">-->
<!--                                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=fLCjQJCekTs" data-effect="fadeIn">-->
<!--                                        <img src="images/PNG/fotomonto_8.png" srcset="images/PNG/fotomonto_8@2x.png" alt="image">-->
<!--                                        <span class="video-play-button icon-play-ban"></span>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <a class="more-ask nav-link page-scroll" href="#home-section">
                    <p class="more-ask__internal medium-paragraph">Więcej pytań znajdziesz w poradniku!</p>
                </a>
            </div>
        </div>
    </div>

    <!--
    <div id="consultation-section" class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Darmowe konsultacje z doradcą</h2>
                    <p class="medium-paragraph">Chcesz zaktualizować swój obraz? Nasz doradca ds. zmiany i zawodów przyszłości wskaże Ci drogę.</p>
                    <p class="regular-paragraph">Zostaw nam swoje dane, a nasz doradca skontaktuje się z Tobą!</p>
                    <form id="consultation-form" data-toggle="validator" data-focus="false" class="form">
                        <div class="form-message">
                            <div id="cmsgSubmit__bottom" class="h3 hidden"></div>
                        </div>
                        <div class="form-group with-star">
                            <div class="form-control-input__parent">
                                <input type="text" class="form-control-input" name="form_bottom[full_name]" id="cfull_name" required>
                                <label class="label-control" for="cname">Imię i nazwisko</label>
                            </div>
                            <div class="help-block with-errors form-text-error"></div>
                        </div>
                        <div class="form-group with-star">
                            <div class="form-control-input__parent">
                                <input type="email" class="form-control-input" name="form_bottom[email]" id="cemail" required>
                                <label class="label-control" for="cemail">Email</label>
                            </div>
                            <div class="help-block with-errors form-text-error"></div>
                        </div>
                        <div class="form-group">
                            <div class="form-control-input__parent">
                                <input type="tel" class="form-control-input" name="form_bottom[tel]" id="ctel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}">
                                <label class="label-control" for="ctel">Tel</label>
                            </div>
                            <div class="help-block with-errors form-text-error"></div>
                        </div>
                        <div class="form-group with-star">
                            <select name="form_bottom[reason]" id="creason" class="required">
                                <option value="">Dlaczego się do nas zgłaszasz? (wybierz temat)</option>
                                <option value="1">temat 1</option>
                                <option value="2">temat 2</option>
                                <option value="3">temat 3</option>
                            </select>
                            <div class="help-block with-errors form-text-error"></div>
                        </div>
                        <div class="form-group custom-checkbox checkbox">
                            <label>
                                <input type="checkbox" id="cterms" name="form_bottom[agreed]" value="Agreed-to-Terms" required>
                                <span class="checkmark"></span>
                                <span class="regular-paragraph regulations">Zapoznałem się z informacją o administratorze i przetwarzaniu danych</span>
                                <div class="tooltip-wrapper">
                                    <div class="tooltip">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In dapibus consectetur orci, ut mattis purus auctor sit amet. Ut eu iaculis nulla. Curabitur facilisis nulla ante, sed aliquet odio dignissim vitae. Nulla facilisi. Morbi nisl velit, luctus in blandit et, interdum nec leo. Suspendisse vel varius felis. Quisque ullamcorper, odio rutrum accumsan tincidunt, leo velit iaculis eros, in luctus leo erat nec massa.</div>
                                </div>
                            </label>
                            <div class="help-block with-errors form-text-error"></div>
                        </div>
                        <div class="help-block with-errors"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-bg"><div class="btn__internal"><span class="btn__label">Wyślij wiadomość</span><i class="btn__icon icon-send"></i></div></button>
                        </div>

                    </form>
                </div>
                <figure class="col-lg-6">
                    <img src="images/PNG/Group%20236.png" srcset="images/PNG/Group%20236.png" alt="alt" class="img-fluid">
                </figure>
            </div>
        </div>
    </div>
    -->

    <div id="consultation-section" class="section">
        <div class="container">
            <div class="row">
                <figure class="col-lg-12">
                    <img src="images/PNG/ban_big.png" srcset="images/PNG/ban_big@2x.png" alt="alt" class="img-fluid">
                </figure>
                <div class="col-lg-12">
                    <h2>Aktualizuj swoją karierę</h2>
                    <p class="medium-paragraph">Wygraj z nami test kompetencji i sesję z doradcą kariery.</p>
                    <p class="regular-paragraph">Weź udział w naszym konkursie, w którym możesz wygrać jedną z 30 nagród!</p>
<!--                    <a href="/files/konkurs.pdf" class="btn btn-bg" download="konkurs.pdf" a href="#modal-update-your-career" rel="modal:open"><div class="btn__internal"><span class="btn__label">Sprawdź szczegóły</span><i class="btn__icon icon-right"></i></div></a>-->
                    <a class="btn btn-bg" href="#modal-update-your-career" rel="modal:open"><div class="btn__internal"><span class="btn__label">Sprawdź szczegóły</span><i class="btn__icon icon-right"></i></div></a>
                </div>
            </div>
        </div>
    </div>

    <div id="our-advisers-section" class="slider-1 section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-center">Nasi doradcy</h2>
                    <div class="slider-container">
                        <div class="swiper-container card-slider swiper-card swiper-motivation" id="js-swiper-motivation">
                            <div class="swiper-wrapper">
                                <?php for ($i=0;$i<2;$i++) { ?>
                                    <div class="swiper-slide">
                                        <figure class="person">
                                            <img src="images/PNG/shutterstock_566714665.png" srcset="images/PNG/shutterstock_566714665@2x.png"  class="person__favicon" alt="image">
                                            <figcaption class="text-center figcaption">
                                                <p class="main__label">Doradca 1</p>
                                                <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                                <p class="description">Czujesz, że w Twoim wieku nowa kariera to głupi pomysł? Wiek to tylko rama, z której możesz wyjść w każdej chwili. Woman Update pomoże Ci w tym.</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="person">
                                            <img src="images/PNG/shutterstock_1673103022.png" srcset="images/PNG/shutterstock_1673103022@2x.png"  class="person__favicon" alt="image">
                                            <figcaption class="text-center figcaption">
                                                <p class="main__label">Doradca 2</p>
                                                <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                                <p class="description">Czujesz, że w Twoim wieku nowa kariera to głupi pomysł? Wiek to tylko rama, z której możesz wyjść w każdej chwili. Woman Update pomoże Ci w tym.</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="person">
                                            <img src="images/PNG/shutterstock_1689404020.png" srcset="images/PNG/shutterstock_1689404020@2x.png" class="person__favicon" alt="image">
                                            <figcaption class="text-center figcaption">
                                                <p class="main__label">Doradca 3</p>
                                                <img src="images/SVG/Path%20147.svg" class="icon" alt="icon">
                                                <p class="description">Czujesz, że w Twoim wieku nowa kariera to głupi pomysł? Wiek to tylko rama, z której możesz wyjść w każdej chwili. Woman Update pomoże Ci w tym.</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-button-next icon-right-slide-2"></div>
                            <div class="swiper-button-prev icon-left-slide-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="change-yourself-image-section" class="section">
        <div class="container">
            <div class="row">
                <figure class="col-lg-3 col-md-3 left-image">
                    <img src="images/PNG/Group%20237.png" srcset="images/PNG/Group%20237@2x.png" alt="image">
                </figure>
                <div class="col-lg-6 col-md-9 middle-text">
                    <h2>Zmień swój obraz</h2>
                    <p class="medium-paragraph">Chcesz zaktualizować swój obraz? Nasz doradca ds. zmiany i zawodów przyszłości wskaże Ci drogę.</p>
                    <a href="#consultation-section" class="btn btn-bg nav-link page-scroll"><div class="btn__internal"><span class="btn__label">Skonsultuj się</span><i class="btn__icon icon-send"></i></div></a>
                </div>
                <figure class="col-lg-3 col-md-3 right-image">
                    <img src="images/PNG/foot_picture.png" srcset="images/PNG/foot_picture@2x.png" alt="image">
                </figure>
            </div>
        </div>
    </div>

    <div id="organizers-section" class="section">
        <div class="container">
            <div class="row">
                <h2>Organizatorzy akcji</h2>
                <div class="list-of-organizers">
                    <img src="images/PNG/Image%2044.png" srcset="images/PNG/Image%2044@2x.png" alt="image">
                    <img src="images/PNG/Group%20239.png" srcset="images/PNG/Group%20239@2x.png" alt="image">
                </div>
            </div>
        </div>
    </div>

    <footer id="footer-section" class="section">
        <div class="container">
            <div class="row">
                <p class="medium-paragraph">Kampania</p>
                <p class="bold-paragraph">#womanupdate</p>
                <p class="medium-paragraph">jest finansowana w ramach programu NESsT Empowers</p>
                <p class="regular-paragraph">Copyright © 2020 Future Collars</p>
            </div>
        </div>
    </footer>

    <?php include_once 'modal-update-your-career.php'; ?>