<div class="logo-wrapper">
	<div class="logo">
		//logo
	</div>
</div>
<div class="menu-top-wrapper">
	<div class="menu-top">
		<?php include_component("page","menuTop")?>
	</div>
</div>

<div class="language-bar">
	<ul>
		<?php foreach(get_allowed_cultures() as $culture):?>
		<li <?php if(sfConfig::get("sf_dimension")==$culture):?>class="active"<?php endif?>>
			<a href="/<?php echo $culture?>"><?php echo $culture?></a>
		</li>
		<?php endforeach?>
		
	</ul>

</div>
