<?php sfContext::getInstance()->getResponse()->setTitle(__('Search results').' - '.sfConfig::get('app_meta_title_sufix'))?>


<div id="localizer">
	<?php echo __("You are here")?>: <?php echo link_to(__('Homepage'),'@home')?>
		&nbsp;›&nbsp; <?php echo __('Search results')?>
</div>

<div class="clear">
	
	<div class="sidebar-wrapper">
		<div class="sidebar">
			<?php include_component("page", "menuSidebar")?>
		</div>
	</div>
	
	<div class="main-wrapper">
		
		<div class="main">
			<h1><?php echo __('Search results')?></h1>
			<?php if($pages):?>
				<ul id="search-results">
				<?php foreach($pages as $page):?>
					<li style="font-size:16px"><?php echo str_replace('%2F','/',link_to($page->getName(),'@page?slug='.$page->getMegaSlug().'&id='.$page->getId()))?></li>
					<li style="font-size:12px; margin-bottom: 10px"><?php echo trim(preg_replace("/&nbsp;/", "", (strip_tags(substr_wrap($page->getContent(),300,1)))))?> <?php echo str_replace('%2F','/',link_to('czytaj więcej','@page?slug='.$page->getMegaSlug()))?></li>
				<?php endforeach?>
				</ul>
			<?php else:?>
				<?php echo __('No results found.')?>
			<?php endif?>
		</div>
	</div>
</div>




	


