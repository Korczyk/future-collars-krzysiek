
<ul class="parents-wrapper" style="overflow: visible">
	<?php $i=0?>
	<?php if(isset($tree[0])):?>
		<?php foreach($tree[0] as $key=>$value):?>
			<li class="parent-wrapper">
				<?php if($pages[$key]->getExternalUrl()):?>
					<a class="parent" href="<?php echo $pages[$key]->getExternalUrl()?>"><?php echo $pages[$key]->getNameMenu()?></a>
				<?php else:?>
					<a class="parent" href="<?php echo url_for('@page?slug='.$pages[$key]->getMegaSlug())?>"><?php echo $pages[$key]->getNameMenu()?></a>
				<?php endif?>
				
				<?php if($value):?>
					<ol class="children-wrapper" >
					<?php foreach($value as  $k=>$v):?>
						<li class="child-wrapper">
							<?php if($pages[$k]->getExternalUrl()):?>
								<a class="child" href="<?php echo $pages[$k]->getExternalUrl()?>"><?php echo $pages[$k]->getNameMenu()?></a>
							<?php else:?>
								<a class="child"href="<?php echo url_for('@page?slug='.$pages[$k]->getMegaSlug())?>"><?php echo $pages[$k]->getNameMenu()?></a>
							<?php endif?>						
						</li>
					<?php endforeach?>
					</ol>
				<?php endif?>
			</li>
		<?php endforeach?>	
	<?php endif?>
</ul>


