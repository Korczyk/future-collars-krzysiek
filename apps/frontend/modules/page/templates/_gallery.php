<?php use_helper("sfThumbnail")?>
<?php sfContext::getInstance()->getResponse()->addJavascript('/js/plugins/fancybox/jquery.fancybox-1.3.1.js');?>
<?php sfContext::getInstance()->getResponse()->addStylesheet('/js/plugins/fancybox/jquery.fancybox-1.3.1.css');?>



<?php $i=0;?>
<?php foreach($images as $image):?>
	<?php if($i%4==0):?>
		<div class="clear">
	<?php endif?>
		<div class="thumb-wrapper">
			<a class="thumb" rel="thumb-group" href="<?php echo $image?>" target="_blank">
				<?php echo thumbnail_tag($image,150,150,array("alt"=>""))?>
			</a>
		</div>
	<?php if(($i+1)%4==0 or ($i+1)==count($images)):?>
		</div>
	<?php endif?>
	<?php $i++?>		
<?php endforeach;?>

<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">
	$(document).ready(function() {
		$("a[rel=thumb-group]").fancybox({
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'titlePosition' 	: 'over',
			'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
			    return '';
			}
		});
	});
</script>
<?php end_slot() ?>



