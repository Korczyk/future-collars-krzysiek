<?php /* Strona Kontaktowa */?>

<div class = "content clear" id = "contact-page">
	<div id = "contact-wrapper" class = "clear">
		<div class = "contact-left-wrapper">
			<form action = "" method = "post" id = "contact-form">
				<ul>
					<li>
						<label>Imię i nazwisko</label>
						<input type = "text" name = "contact[name]" />
					</li>
					<li>
						<label>Numer telefonu</label>
						<input type = "text" name = "contact[phone]" />
					</li>
					<li>
						<label>E-mail</label>
						<input type = "text" name = "contact[email]" />
					</li>
					<li>
						<label>Treść wiadomości</label>
						<textarea name = "contact[message]"></textarea>
					</li>
					<li class = "clear">
						<span id = "submit-button-wrapper">
							<input type = "submit" value = "Wyślij" />
						</span>
					</li>
				</ul>
			</form>
			<div id = "contact-form-alert"></div>
		</div>
		<div class = "contact-right-wrapper">
			<h3>Dane kontaktowe</h3>
			Cyrek Internet Technologies<br />
			ul. Wschodnia 29<br />
			95 - 100 Zgierz<br />
		</div>
	</div>
	<div id = "google-maps-wrapper">
		<iframe height="357" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.pl/maps?f=q&source=s_q&hl=en&geocode=&q=zgierz&aq=&sll=51.618545,19.367121&sspn=2.257979,5.817261&ie=UTF8&hq=&hnear=Zgierz,+Zgierz+County,+%C5%81%C3%B3d%C5%BA+Voivodeship&t=m&ll=51.864514,19.408035&spn=0.037844,0.096302&z=13&iwloc=A&output=embed"></iframe>
	</div>
</div>

<?php append_to_slot('additional_javascript'); ?>
<script type = "text/javascript">
$(function()
{
	$("#contact-form").submit(function()
	{
		$.ajax(
		{
			type: "POST",
			url: "/wyslij-wiadomosc",
			data: $("#contact-form").serialize(), 
			success: function(data)
			{
				$('#contact-form-alert').html(data).show().delay(5000).fadeOut(); 
				$("#contact-form").find("input[type=text], textarea").val('');
			},
			complete: function(data)
			{
			}
		});

		return false;
	});
});
</script>
<?php end_slot();?>