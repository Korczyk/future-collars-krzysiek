
<?php foreach($posts as $post):?>
<div class="post">
	<a class="post-header" href="<?php echo url_for("@page?slug=".$post->getMegaSlug())?>"><?php echo $post->getName()?></a>
	<p class="post-content"><?php echo substr_wrap($post->getContent(),300)?></p>
	<a class="post-read-more" href="<?php echo url_for("@page?slug=".$post->getMegaSlug())?>"><?php echo __("read more")?></a>
</div>
<?php endforeach?>