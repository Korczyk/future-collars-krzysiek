<div class="localizer">
	<?php echo __("You are here")?>: <?php echo link_to(__('Homepage'),'@home')?>
	<?php foreach($page->getBreadcrumps() as $breadcrump):?>
		&nbsp;›&nbsp;<?php echo link_to($breadcrump['name_menu'],'@page?slug='.$breadcrump['mega_slug'])?>
	<?php endforeach?> 
</div>