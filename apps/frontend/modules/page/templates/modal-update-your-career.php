<div id="modal-update-your-career" class="modal">
    <div class="modal__internal">
        <div class="modal__internal-scroll">
            <h3 class="bold-paragraph">KONKURS - Aktualizuj swoją karierę</h3>
            <h4>Wygraj z nami test kompetencji i sesję z doradcą kariery</h4>
            <p class="regular-paragraph">Jeśli czujesz, że nie wiesz jakie są Twoje kompetencje albo potrzebujesz z kimś porozmawiać o swoim rozwoju zawodowym to mamy dla Ciebie rozwiązanie. Weź udział w naszym konkursie, w którym możesz wygrać jedną z ​<b>30 nagród</b>​ tj. :</p>
            <p class="regular-paragraph regular-paragraph--with-image-1"><span><b>Test osobowości</b> od ​firmy <b>PERSO.IN®</b>​ ​ ​-​ ​to narzędzie diagnostyczne, dzięki któremu możesz odkryć psychologiczne różnice między ludźmi. Po uzupełnieniu ankiety internetowej otrzymasz spersonalizowany raport, który pokazuje Twoje naturalne predyspozycje i zawiera instrukcje jak wykorzystać osobowość w życiu prywatnym i zawodowym.</span></p>
            <p class="regular-paragraph regular-paragraph--with-image-2"><span><b>Godzinną sesje</b>̨ z doradcą kariery​, który pomoże Ci określić Twoje talenty, podpowie w jakim kierunku zawodowym możesz iść, jakie umiejętności rozwijać.</span></p>
            <p class="semibold-paragraph">Co zrobić, aby wziąć udział w konkursie?</p>
            <ul class="list-of-benefits to--left list-of-tips">
                <li class="regular-paragraph">jak nowe technologie pomagają Ci w życiu codziennym, osobistym i pozytywnie zmieniają Twoją rzeczywistość.</li>
                <li class="regular-paragraph">lub</li>
                <li class="regular-paragraph">jak nowe technologie pomogły Ci wyjść z dotychczasowych “ram”, w których dotąd byłaś osadzona i żyłaś</li>
                <li class="regular-paragraph">lub</li>
                <li class="regular-paragraph">jak nowe technologie pomogły Ci w rozwoju</li>
            </ul>
            <p class="regular-paragraph">Osoby, które opublikują najbardziej kreatywne posty otrzymają ww. nagrody. Konkurs rusza od 9 czerwca. Na Wasze posty czekamy przez tydzień tj. do 16 czerwca. Wyniki ogłosimy do 7 dni od daty zakończenia konkursu.</p>
            <a href="/files/konkurs.pdf" class="regular-paragraph link link-to-paragraph" download="konkurs.pdf">Pobierz regulamin konkursu</a>
            <p class="semibold-paragraph">Dziękujemy firmie PERSO IN® za wsparcie projektu.</p>
        </div>
    </div>
</div>