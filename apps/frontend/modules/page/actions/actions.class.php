<?php
class pageActions extends sfActions {
	
	
	
	public function executeSearch($request){
		$searchString=$this->getRequest()->getParameter('search_string');
		$pages=array();
		if($this->getRequest()->getParameter('search_string')){
			$searchString=$this->getRequest()->getParameter('search_string');
			$searchString=strip_tags($searchString);
			$searchString=str_replace(array("\x22","\x27"),'',$searchString);
			$searchString=trim($searchString);
			$this->getRequest()->setParameter('search_string',$searchString);
	
			$keys = PagePeer::search($this->getRequestParameter('search_string'));
			
			if($keys){
				$c=new Criteria;
				$c->add(PagePeer::ID,$keys,	Criteria::IN);
				$c->add(PagePeer::LANGUAGE,sfConfig::get('sf_dimension'));
				$pages = PagePeer::doSelect($c);
			}
		}
		$this->pages=$pages;
	}
	
	public function executeIndex($request) {
		
		
		
		sfLoader::loadHelpers("I18N");
		
		$c = new Criteria;
		$c->add(PagePeer::SLUG,Utils::normalizeName(__("Homepage")));
		$c->add(PagePeer::KIND,1);
		$c->add(PagePeer::LANGUAGE,sfConfig::get('sf_dimension'));
		$page = PagePeer::doSelectOne($c);
		
		if(!($page instanceOf Page)){
			return $this->forward404();
		}
		
		$this->page = $page;
		
	}
	
	public function validateShow($request) {
		
		$c = new Criteria;
		$c->add(PagePeer::MEGA_SLUG,$this->getRequestParameter('slug'));
		$c->add(PagePeer::LANGUAGE,sfConfig::get('sf_dimension'));
		$page = PagePeer::doSelectOne($c);
		if($page instanceof Page){
			$this->page = $page;
		
		}
		else{
			return $this->forward404();
		}
		
		return true;
	}
	public function executeShow($request) {
		sfLoader::loadHelpers(array('I18N','Partial','Form'));
		
		if($this->page->getOption("template")){
			return $this->setTemplate("template".$this->page->getOption("template"));
		}
		
		
		
	}
	
	// public function executeSendMessage()
	// {
	// 	sfLoader::loadHelpers("I18N");
	// 	$c = new Criteria();
	// 	$c->add(ConfigPeer::NAME,'email_'.sfConfig::get('sf_dimension'));
	// 	$email = ConfigPeer::doSelectOne($c);
	
	// 	if($this->getRequest()->getMethod() == sfRequest::POST || $this->getRequest()->isXmlHttpRequest())
	// 	{
	// 		$post = $this->getRequestParameter('contact');
	// 		if(!empty($post))
	// 		{
	// 			$header = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=UTF-8' . "\r\n";
	// 			$header .= 'From: '.sfConfig::get('app_email')."\r\n" .
	// 					'Reply-To: '.$post['email'] . "\r\n" .
	// 					'X-Mailer: PHP/' . phpversion();
					
	// 			$msg = "Witaj, <br />";
	// 			$msg .= "otrzymałeś nową wiadomość email ze strony. Poniżej znajdują się szczegóły: <br /><br />";
					
	// 			$msg .= "Imię i nazwisko: ".$post['name']."<br />";
	// 			$msg .= "Telefon: ".$post['phone']."<br />";
	// 			$msg .= "Email: ".$post['email']."<br />";
	// 			$msg .= "Treść wiadomości: ".$post['message'];
					
	// 			if($email->getValue())
	// 			{
	// 				mail($email->getValue(), 'Wiadomość ze strony', $msg, $header);
	// 			}
	
	// 			$mail = new Mail();
	// 			$mail->setContent($msg);
	// 			$mail->setEmail($post['email']);
	// 			$mail->setCreatedAt(date("Y-m-d H:i:s"));
	// 			$mail->save();
	
	// 			echo __('Send Message Success');
	// 		}
	// 	}
	// 	return sfView::NONE;
	// }

	public function executeSendMessage()
	    {
		sfLoader::loadHelpers("I18N");
		$c = new Criteria();
		$c->add(ConfigPeer::NAME,'email_'.sfConfig::get('sf_dimension'));
		$email = ConfigPeer::doSelectOne($c);
	
		if($this->getRequest()->getMethod() == sfRequest::POST)
		{
		    
		    $response= array();
            $errors = array();
            
            

			if($this->getRequestParameter('form_top')){
                $post = $this->getRequestParameter('form_top');
                
                if(empty($post['form_top']['guide_address_email']) || !$post['form_top']['guide_address_email']) {
                    $errors['guide_address_email'] = "Musisz wypełnić to pole";
                }
                
                $regex = '/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/';
                if (!preg_match($pattern, $post['form_top']['guide_address_email']) === 1) {
                    $errors['guide_address_email'] = "Adres email jest nie prawidłowy";
                }
                
            }elseif($this->getRequestParameter('form_bottom')){
                $post = $this->getRequestParameter('form_bottom');
                
                if(empty($post['form_bottom']['full_name']) || !$post['full_name']['email']) {
                    $errors['full_name'] = "Musisz wypełnić to pole";
                }
                
                if(empty($post['form_bottom']['email']) || !$post['form_bottom']['email']) {
                    $errors['email'] = "Musisz wypełnić to pole";
                }
                
                $regex = '/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/';
                if (!preg_match($pattern, $post['form_bottom']['email']) === 1) {
                    $errors['email'] = "Adres email jest nie prawidłowy";
                }
                
                
                $regex = '/^[0-9]{3}-[0-9]{3}-[0-9]{3}+$/';
                if (!preg_match($pattern, $post['form_bottom']['tel']) === 1) {
                    $errors['tel'] = "Telefon jest nie prawidłowy";
                }
                
                if(empty($post['form_bottom']['reason']) || !$post['form_bottom']['reason']) {
                    $errors['reason'] = "Wybierz powód";
                }
                
                if(empty($post['form_bottom']['agreed']) || !$post['form_bottom']['agreed']) {
                    $errors['agreed'] = "Wybierz powód";
                }
                
                
                
                if(!empty($post) && empty($errors))
    			{
    				$header = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=UTF-8' . "\r\n";
    				$header .= 'From: '.sfConfig::get('app_email')."\r\n" .
    						'Reply-To: '.$post['email'] . "\r\n" .
    						'X-Mailer: PHP/' . phpversion();
    					
    				$msg = "Witaj, <br />";
    				$msg .= "otrzymałeś nową wiadomość email ze strony. Poniżej znajdują się szczegóły: <br /><br />";
    					
    				// $msg .= "Imię i nazwisko: ".$post['form_bottom']['full_name']."<br />";
    				// if(isset($post['form_bottom']['tel']) {
    				//     $msg .= "Telefon: ".$post['form_bottom']['tel']."<br />";    
    				// }
    				// $msg .= "Email: ".$post['form_bottom']['email']."<br />";
    				// $msg .= "Powód: ".$post['form_bottom']['reason'];
    				$msg .= "Witaj ".$$post['form_top']['guide_address_email'].", aby pobrać poradnik kliknij w link: ";
    					
    				if($email->getValue())
    				{
    					mail($email->getValue(), 'Wiadomość ze strony', $msg, $header);
    				}
    	
    				$mail = new Mail();
    				$mail->setContent($msg);
    				$mail->setEmail($post['email']);
    				$mail->setCreatedAt(date("Y-m-d H:i:s"));
    				$mail->save();
    	
    				echo __('Send Message Success');
    			}
            }
            
            
            
            
            
            
			
		}
		return sfView::NONE;
	}
}
