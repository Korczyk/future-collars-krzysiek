<?php
class pageComponents extends sfComponents {
	public function executeHeader() {
		
	}
	
	public function executeFooter() {
		
	}
	
	public function executePosts() {
		$c = new Criteria;
		$c->addDescendingOrderByColumn(PagePeer::CREATED_AT);
		$c->add(PagePeer::KIND,2);
		$c->add(PagePeer::PARENT,$this->page->getId());
		$c->add(PagePeer::LANGUAGE, sfConfig::get('sf_dimension'));
		$posts = PagePeer::doSelect($c);
		$this->posts = $posts;
	}
	
	public function executeGallery() {
		$c = new Criteria;
		$c->addAscendingOrderByColumn(ImagePeer::POSITION);
		$c->add(ImagePeer::PAGE_ID,$this->page->getId());
		$images = ImagePeer::doSelect($c);
		$this->images = $images;
	}
	
	public function executeAttachements() {
		$c = new Criteria;
		$c->addAscendingOrderByColumn(AttachementPeer::POSITION);
		$c->add(AttachementPeer::PAGE_ID,$this->page->getId());
		$attachements = AttachementPeer::doSelect($c);
		$this->attachements = $attachements;
	}
	
	
	
	public function executeMenuTop(){
		$tree = array();
		$info = array();
		$childrenIds=array();
	
	
		$c = new Criteria;
		$c->addAscendingOrderByColumn(PagePeer::POSITION);
		$c->add(PagePeer::IS_FEATURED,1);
		$c->add(PagePeer::KIND,1);
		$c->add(PagePeer::LANGUAGE, sfConfig::get('sf_dimension'));
		$pages = PagePeer::doSelect($c);
		foreach($pages as $page){
			$info[$page->getId()]=$page;
			$children = explode(' ',$page->getChildren());
			
			if(!isset($tree[0][$page->getId()])){
				$tree[0][$page->getId()]=array();
			}
			foreach($children as $child){
				if($child){
					$childrenIds[]=$child;
					$tree[0][$page->getId()][$child]=array();
				}
			}
		}
	
	
	
		$c=new Criteria;
		$c->add(PagePeer::ID,$childrenIds,Criteria::IN);
		$pages = PagePeer::doSelect($c);
	
		foreach($pages as $page){
			$info[$page->getId()]=$page;
		}

		$this->pages=$info;
		$this->tree=$tree;
	}
	
	public function executeMenuSidebar(){
			
		sfLoader::loadHelpers("I18N");
		
		$tree = array();
		$info = array();
		$childrenIds=array();
		$this->page;
		
		if(isset($this->page)){
			
			if($this->page->getChildren()){
				$parent=$this->page->getId();
			}
			else{
				$parent=$this->page->getParent();
			}
		}
		else{
			$parent=0;
		}
		
		$c = new Criteria;
		$c->addAscendingOrderByColumn(PagePeer::POSITION);
		$c->add(PagePeer::KIND,1);
		$c->add(PagePeer::PARENT,$parent);
		$c->add(PagePeer::SLUG,Utils::normalizeName(__("Homepage")),Criteria::NOT_EQUAL);
		$c->add(PagePeer::LANGUAGE, sfConfig::get('sf_dimension'));
		$pages = PagePeer::doSelect($c);
		foreach($pages as $page){
			$info[$page->getId()]=$page;
			$children = explode(' ',$page->getChildren());
				
			if(!isset($tree[0][$page->getId()])){
				$tree[0][$page->getId()]=array();
			}
			foreach($children as $child){
				if($child){
					$childrenIds[]=$child;
					$tree[0][$page->getId()][$child]=array();
				}
			}
		}
		
		
		
		$c=new Criteria;
		$c->add(PagePeer::ID,$childrenIds,Criteria::IN);
		$pages = PagePeer::doSelect($c);
		
		foreach($pages as $page){
			$info[$page->getId()]=$page;
		}
		
		$parent=PagePeer::retrieveByPK($parent);
		if($parent instanceOf Page){
			$this->parent=$parent;
		}
		else $this->parent=null;
		
		$this->pages=$info;
		$this->tree=$tree;
			
	
	
	
	
	}
}
