<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

// create a new test browser
$browser = new sfTestBrowser();

$browser->
  get('/image/index')->
  isStatusCode(200)->
  isRequestParameter('module', 'image')->
  isRequestParameter('action', 'index')->
  checkResponseElement('body', '!/This is a temporary page/');
