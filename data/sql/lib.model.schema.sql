
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- attachement
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `attachement`;


CREATE TABLE `attachement`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`page_id` INTEGER  NOT NULL,
	`url` VARCHAR(1024),
	`description` VARCHAR(1024),
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `attachement_FKIndex1`(`page_id`),
	CONSTRAINT `attachement_FK_1`
		FOREIGN KEY (`page_id`)
		REFERENCES `page` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
)Engine=InnoDB;

#-----------------------------------------------------------------------------
#-- config
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `config`;


CREATE TABLE `config`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255),
	`value` TEXT,
	PRIMARY KEY (`id`)
)Engine=InnoDB;

#-----------------------------------------------------------------------------
#-- image
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `image`;


CREATE TABLE `image`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`page_id` INTEGER  NOT NULL,
	`url` VARCHAR(255),
	`description` VARCHAR(255),
	PRIMARY KEY (`id`),
	KEY `image_FKIndex1`(`page_id`),
	CONSTRAINT `image_FK_1`
		FOREIGN KEY (`page_id`)
		REFERENCES `page` (`id`)
		ON UPDATE RESTRICT
		ON DELETE CASCADE
)Engine=InnoDB;

#-----------------------------------------------------------------------------
#-- mail
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `mail`;


CREATE TABLE `mail`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`content` TEXT  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`)
)Engine=InnoDB;

#-----------------------------------------------------------------------------
#-- page
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `page`;


CREATE TABLE `page`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`language` VARCHAR(32) default '' NOT NULL,
	`name` VARCHAR(255),
	`name_menu` VARCHAR(255) default '' NOT NULL,
	`slug` VARCHAR(255) default '' NOT NULL,
	`mega_slug` VARCHAR(1024) default '' NOT NULL,
	`content` TEXT,
	`content_2` TEXT,
	`parent` INTEGER,
	`descendants` TEXT,
	`children` TEXT,
	`is_published` TINYINT,
	`path` VARCHAR(255),
	`stats` TEXT,
	`kind` TINYINT,
	`is_deleted` TINYINT,
	`position` INTEGER,
	`is_featured` TINYINT default 0 NOT NULL,
	`is_expanded` TINYINT default 1 NOT NULL,
	`external_url` VARCHAR(1024) default '' NOT NULL,
	`passwd` VARCHAR(32) default '' NOT NULL,
	`options` TEXT,
	`created_at` DATETIME,
	`created_by` INTEGER,
	`modified_at` DATETIME,
	`modified_by` INTEGER,
	PRIMARY KEY (`id`)
)Engine=InnoDB;

#-----------------------------------------------------------------------------
#-- page_index
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `page_index`;


CREATE TABLE `page_index`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`page_id` INTEGER  NOT NULL,
	`word` VARCHAR(255),
	`weight` INTEGER,
	PRIMARY KEY (`id`),
	KEY `page_index_FKIndex1`(`page_id`),
	CONSTRAINT `page_index_FK_1`
		FOREIGN KEY (`page_id`)
		REFERENCES `page` (`id`)
		ON UPDATE RESTRICT
		ON DELETE CASCADE
)Engine=InnoDB;

#-----------------------------------------------------------------------------
#-- user
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;


CREATE TABLE `user`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128),
	`email` VARCHAR(128),
	`role` TINYINT,
	`description` VARCHAR(255),
	`is_deleted` TINYINT default 0,
	`passwd` VARCHAR(32),
	`salt` VARCHAR(32),
	PRIMARY KEY (`id`)
)Engine=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
