<?php sfContext::getInstance()->getResponse()->addJavascript('plugins/csfSlideshow/csfSlideshow.js');?>
<?php sfContext::getInstance()->getResponse()->addStylesheet('plugins/csfSlideshow/csfSlideshow.css');?>

<div id="pager">
	<ul>
	</ul>
</div>
<ul id="slideshow" style="margin-top: 0px">
	<?php $i=0;?>
	<?php if(!empty($slides)):?>
		<?php foreach($slides as $slide):?>
			<?php if($slide):?>
			<?php $i++;?>
			<?php if($i==1):?>
				<li class="active">
			<?php else:?>
				<li>
			<?php endif?>
				<div class="slide-wrapper">
					<div class="slide">
						<?php echo $slide['content']?>
					</div>
				</div>
			</li>
			<?php endif?>
		
		<?php endforeach?>
	<?php endif;?>
</ul>

