<?php sfContext::getInstance()->getResponse()->addJavascript('/js/ckeditor/ckeditor.js');?>
<?php sfContext::getInstance()->getResponse()->addJavascript('/js/ckfinder/ckfinder.js');?>

<?php $config = new Config;?>
<?php $slideshow = unserialize($config->getOption("csfSlideshow",$sf_request->getParameter("language","pl")))?>
<div class="csfSlideshowPlugin">
	<form method="post">
		<?php echo textarea_tag("slideshow_code[1]",isset($slideshow[1])?$slideshow[1]:"",array("class"=>"form-control"))?><br/>
		<?php echo textarea_tag("slideshow_code[2]",isset($slideshow[2])?$slideshow[2]:"",array("class"=>"form-control"))?><br/>
		<?php echo textarea_tag("slideshow_code[3]",isset($slideshow[3])?$slideshow[3]:"",array("class"=>"form-control"))?><br/>
		<?php echo textarea_tag("slideshow_code[4]",isset($slideshow[4])?$slideshow[4]:"",array("class"=>"form-control"))?><br/>
		<?php echo textarea_tag("slideshow_code[5]",isset($slideshow[5])?$slideshow[5]:"",array("class"=>"form-control"))?><br/>
		<a href="javascript:void(0)" id="save" class="btn btn-success"><?php echo __('Save')?></a>
		<?php echo input_hidden_tag("language",$sf_request->getParameter("language","pl"))?>
	</form>
	<div class="output">

	</div>
</div>

<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">

$(document).on('click', '.csfSlideshowPlugin a#save', function () {
	for ( instance in CKEDITOR.instances ) CKEDITOR.instances[instance].updateElement();
	
	 $.ajax({
        type: "POST",
        url: '<?php echo url_for("@csfSlideshow?action=update")?>',
        data: $(".csfSlideshowPlugin form").serialize(), // serializes the form's elements.
        success: function(data)
        {
            $(".csfSlideshowPlugin .output").html(data); // show response from the php script.
        }
      });

 	return false; // avoid to execute the actual submit of the form.	
})


</script>
<?php echo end_slot()?>

<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">

	editor=CKEDITOR.replace( 'slideshow_code_1', {});
	CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	editor=CKEDITOR.replace( 'slideshow_code_2', {});
	CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	editor=CKEDITOR.replace( 'slideshow_code_3', {});
	CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	editor=CKEDITOR.replace( 'slideshow_code_4', {});
	CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

	editor=CKEDITOR.replace( 'slideshow_code_5', {});
	CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;
</script>
<?php end_slot() ?>