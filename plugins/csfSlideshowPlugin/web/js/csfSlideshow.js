function slideSwitch() {
	
	if(typeof($play) == "undefined") $play=1;

	if($play==1){
	
		var $active = $('#slideshow li.active');
	    if ( $active.length == 0 ) $active = $('#slideshow li:last');
	   
	    var $next =  $active.next().length ? $active.next()
	        : $('#slideshow li:first');
	    $active.css({opacity: 1.0})
	        .addClass('last-active')
	        .animate({opacity: 0.0}, 1000, function() {
	           
	        });
	    $next.css({opacity: 0.0})
	        .addClass('active')
	        .animate({opacity: 1.0}, 1000, function() {
	            $active.removeClass('active last-active');
	        });
	    $('#pager li').removeClass('on');
	    $('#pager li').eq($next.index()).addClass('on');
	}
}

$(function() {
	
    setInterval( "slideSwitch()", 5000 );
   
});

$(window).load(function() {
	
	$("#slideshow li").each(function(index){		
		if(index == 0){
			$("#pager ul").append('<li class="on"><a href="javascript:void(0)"><span>'+ (index+1) +'</span></a></li>');
		}
		else{
			$("#pager ul").append('<li><a href="javascript:void(0)"><span>'+ (index+1) +'</span></a></li>');
		}
	})
	
	
	$('#pager a').click(function(){
		$play=0;
		
		
		
		$play = 0;
	
		var $active = $('#slideshow LI.active');
	    if ( $active.length == 0 ) $active = $('#slideshow LI:last');
	   
	    var $next =  $('#slideshow li').eq($(this).parent().index());
	    $active.css({opacity: 1.0})
	        .addClass('last-active')
	        .animate({opacity: 0.0}, 1000, function() {
	           
	        });
	    $next.css({opacity: 0.0})
	        .addClass('active')
	        .animate({opacity: 1.0}, 1000, function() {
	            $active.removeClass('active last-active');
	        });
	
	    $('#pager li').removeClass('on');
	    $('#pager li').eq($(this).parent().index()).addClass('on');
	    

		
	})

});

