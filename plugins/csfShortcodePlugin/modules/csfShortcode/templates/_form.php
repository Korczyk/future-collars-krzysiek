<?php sfContext::getInstance()->getResponse()->addJavascript('/js/ckeditor/ckeditor.js');?>
<?php sfContext::getInstance()->getResponse()->addJavascript('/js/ckfinder/ckfinder.js');?>

<?php $config = new Config;?>
<?php $shortcode = unserialize($config->getOption("csfShortcode",$sf_request->getParameter("language","pl")))?>
<?php $shortcode = is_array($shortcode)?$shortcode:array()?>

<div class="csfShortcodePlugin-wrapper">
<div class="csfShortcodePlugin">
	
	
	
	
	<form method="post">
		<h4>Dodaj</h4>
		<div class="row">
			<div class="col-lg-3">
				<?php echo select_tag("shortcode[".(count($shortcode)+1)."][kind]",options_for_select(array("text"=>"Tekst","html"=>"HTML")),array("id"=>"shortcode_kind_".(count($shortcode)+1),"class"=>"form-control shortcode-kind"))?>
			</div>
			<div class="col-lg-3">
				<input type="text" name="shortcode[<?php echo (count($shortcode)+1)?>][name]" id="shortcode_name_<?php echo (count($shortcode)+1)?>" class="form-control" value="">
			</div>
			<div class="col-lg-6">
				<textarea name="shortcode[<?php echo (count($shortcode)+1)?>][content]" id="shortcode_content_<?php echo (count($shortcode)+1)?>" rows="" cols="" class="form-control"></textarea>
			</div>
		</div>
	
			
		<?php if($shortcode):?>
			<hr/>
			<h4>Edytuj</h4>
			<?php $i=0;?>
			<?php foreach($shortcode as $id=>$value):?>
			<?php $i++?>
			<div class="row" style="margin-bottom: 5px">
				<div class="col-lg-3">
					<?php echo select_tag("shortcode[".$id."][kind]",options_for_select(array("text"=>"Tekst","html"=>"HTML"),$value['kind']),array("id"=>"shortcode_kind_".$id,"class"=>"form-control shortcode-kind"))?>
				</div>
				<div class="col-lg-3">
					<input type="text" name="shortcode[<?php echo $id?>][name]" id="shortcode_name_<?php echo $id?>" class="form-control" value="<?php echo $value['name']?>">
				</div>
			
				<div class="col-lg-6">
					<textarea name="shortcode[<?php echo $id?>][content]" id="shortcode_content_<?php echo $id?>" rows="" cols="" class="form-control"><?php echo $value['content']?></textarea>
				</div>
			</div>	
			<?php endforeach?>
		<?php endif?>
		
		
		<a href="javascript:void(0)" id="save" class="btn btn-success"><?php echo __('Save')?></a>
		<?php echo input_hidden_tag("language",$sf_request->getParameter("language","pl"))?>
	</form>
	<div class="output">

	</div>
</div>
</div>

<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">





$(document).on('click', '.csfShortcodePlugin a#save', function () {

	for ( instance in CKEDITOR.instances ) CKEDITOR.instances[instance].updateElement();
	
	 $.ajax({
         type: "POST",
         url: '<?php echo url_for("@csfShortcode?action=update")?>',
         data: $(".csfShortcodePlugin form").serialize(), // serializes the form's elements.
         success: function(data)
         {
             $(".csfShortcodePlugin").html(data); // show response from the php script.

             $(".shortcode-kind").each(function(){
            	if($(this).val()=="html"){
            		id = $(this).attr("id").substr(15);
            		
            		editor=CKEDITOR.replace( 'shortcode_content_' + id, {});
            		CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;

            		
            	}
             });

             
         }
       });
	 
  return false; // avoid to execute the actual submit of the form.
	
})

$(document).on('change', '.shortcode-kind', function () {
	
	id = $(this).attr("id").substr(15);
	if($(this).val()=="text"){
		CKEDITOR.instances['shortcode_content_' + id].destroy()
	}
	else if($(this).val()=="html"){
		editor = CKEDITOR.replace( 'shortcode_content_' + id, {});
		CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;
	}
})

<?php foreach($shortcode as $id=>$value):?>
	<?php if($value['kind']=="html"):?>
	editor = CKEDITOR.replace( 'shortcode_content_<?php echo $id?>', {});
	CKFinder.setupCKEditor( editor, '/js/ckfinder' ) ;
	<?php endif?>

<?php endforeach;?>

	
</script>
<?php echo end_slot()?>

