<?php

class csfShortcodeActions extends sfActions{
	public function executeUpdate(){
		$c = new Criteria;
		$c->add(ConfigPeer::NAME,"csfShortcode_".$this->getRequestParameter("language"));
		$config = ConfigPeer::doSelectOne($c);
		if($config instanceof Config){
			
		}
		else{
			$config = new Config;
		}
		
		$shortcode = $this->getRequestParameter("shortcode");
		if($shortcode){
			foreach($shortcode as $key=>$value){
				$shortcode[$key]['name'] = trim($value["name"]);
				
				if(trim($value["name"])){
					if($value['kind']=="text"){
						
						$shortcode[$key]['content']= strip_tags($value['content']);
					}
				}
				else{
					unset($shortcode[$key]);
				}
			}
		}
		
		$config->setName("csfShortcode_".$this->getRequestParameter("language"));
		$config->setValue(serialize($shortcode));
		$config->save();
	}
	
	
}