<?php

function shortcode($name){
	$config = new Config;
	$shortcodes = unserialize($config->getOption("csfShortcode"));
	
	$shortcode=array();
	if(is_array($shortcodes)){
		foreach($shortcodes as $key=>$value){
			$shortcode[$value['name']]=$value['content'];
		}
	}
	
	if(isset($shortcode[$name])){
		return $shortcode[$name];
	}
	else{
		return null;
	}
	
	
}




