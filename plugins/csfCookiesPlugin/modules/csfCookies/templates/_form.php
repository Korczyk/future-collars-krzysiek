<?php $config = new Config;?>

<div class="csfCookiesPlugin">
	<form method="post">
		<?php echo textarea_tag("cookie_content",$config->getOption("csfCookies",$sf_request->getParameter("language","pl")),array("class"=>"form-control"))?><br/>
		<a href="javascript:void(0)" id="save" class="btn btn-success"><?php echo __('Save')?></a>
		<?php echo input_hidden_tag("language",$sf_request->getParameter("language","pl"))?>
	</form>
	<div class="output">

	</div>
</div>

<?php append_to_slot('additional_javascript') ?>

<script type="text/javascript">
CKEDITOR.replace( 'cookie_content', {});

$(document).on('click', '.csfCookiesPlugin a#save', function () {
	for ( instance in CKEDITOR.instances ) CKEDITOR.instances[instance].updateElement();
	
	 $.ajax({
         type: "POST",
         url: '<?php echo url_for("@csfCookies?action=update")?>',
         data: $(".csfCookiesPlugin form").serialize(), // serializes the form's elements.
         success: function(data)
         {
             $(".csfCookiesPlugin .output").html(data); // show response from the php script.
         }
       });

  return false; // avoid to execute the actual submit of the form.
	
})
</script>
<?php echo end_slot()?>