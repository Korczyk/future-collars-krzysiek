<?php if(!$sf_request->getCookie('hide_cookie')):?>
<?php use_helper("I18N")?>
<?php use_javascript('plugins/csfCookies/csfCookies.js','last');?>
<?php use_stylesheet('plugins/csfCookies/csfCookies.css');?>
<?php if($content):?>
<div class="cookie-wrapper"> 
	<div class="cookie">
		<?php echo $content?>
		<a href="javacript:void(0)" id="cookie-close">
			<img src="/images/plugins/csfCookies/cookies-close.png" alt="<?php echo __("Close")?>">
		</a> 
	 </div>
</div>
<?php endif?>
<?php endif?>
