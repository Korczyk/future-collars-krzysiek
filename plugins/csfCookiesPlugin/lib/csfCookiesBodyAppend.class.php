<?php


class csfCookiesPluginHeadAppendFilter extends sfFilter
{
	public function execute($filterChain)
	{
		
		$response = $this->getContext()->getResponse();
		$content = $response->getContent();
	
		$config = new Config();
		
		$request = $this->getContext()->getRequest();
		$response = $this->getContext()->getResponse();
		$user    = $this->getContext()->getUser();
		$controller = $this->getContext()->getController();
		$action = $this->getContext()->getController()->getActionStack()->getLastEntry()->getActionInstance();
		
		
		
		$content = $action->getPartial("csfCookies/cookie",array("content"=>$config->getOption("csfCookies")));

		$filterChain->execute();
		
		$response->setContent(str_ireplace('</body>', $content.'</body>',$response->getContent()));

	}
}




?>