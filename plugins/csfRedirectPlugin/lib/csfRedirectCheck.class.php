<?php


class csfRedirectCheckFilter extends sfFilter
{
	public function execute($filterChain)
	{
		$config = new Config();
		
		$request = $this->getContext()->getRequest();
		$response = $this->getContext()->getResponse();
		$user    = $this->getContext()->getUser();
		$controller = $this->getContext()->getController();
			
		
		$array = unserialize($config->getOption("csfRedirect"));
		if(!empty($array))
		{
			foreach($array as $key=>$value){
				$redirects[$value['source']]=$value['target'];
			}
		}
		
		$url = $request->getUri();
		
		while(isset($redirects[$url])){
			$url = $redirects[$url];
		}
		
		if($request->getUri()!=$url){
			return $controller->redirect($url,301);
		}
		
		
		$filterChain->execute();
		
		
		

	}
}




?>