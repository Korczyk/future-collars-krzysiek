<?php

class csfRedirectActions extends sfActions{
	
	
	public function executeUpdate(){
		$c = new Criteria;
		$c->add(ConfigPeer::NAME,"csfRedirect_".$this->getRequestParameter("language"));
		$config = ConfigPeer::doSelectOne($c);
		if($config instanceof Config){
			
		}
		else{
			$config = new Config;
		}
		
		$redirect = $this->getRequestParameter("redirect");
		if($redirect){
			foreach($redirect as $key=>$value){
				$redirect[$key]['source'] = trim($value["source"]);
				$redirect[$key]['target'] = trim($value["target"]);
				
				if(trim($value["source"]) and trim($value["target"])){
					
				}
				else{
					unset($redirect[$key]);
				}
			}
		}
		
		$config->setName("csfRedirect_".$this->getRequestParameter("language"));
		$config->setValue(serialize($redirect));
		$config->save();
		
		$this->config = $config; 
	}
	
	
	
	
}