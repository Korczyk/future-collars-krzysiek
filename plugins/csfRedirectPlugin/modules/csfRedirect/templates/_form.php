<?php $config = new Config;?>
<?php $redirect = unserialize($config->getOption("csfRedirect",$sf_request->getParameter("language","pl")))?>
<?php $redirect = is_array($redirect)?$redirect:array()?>

<div class="csfRedirectPlugin-wrapper">
	<div class="csfRedirectPlugin">
		
		
		
		
		<form method="post">
			<h4>Dodaj</h4>
			<div class="row">
				
				<div class="col-lg-3">
					<input placeholder="<?php echo __("source url")?>" type="text" name="redirect[<?php echo (count($redirect)+1)?>][source]" class="form-control" value="">
				</div>
				<div class="col-lg-6">
					<input placeholder="<?php echo __("target url")?>" type="text" name="redirect[<?php echo (count($redirect)+1)?>][target]" class="form-control" value="">
				</div>
			</div>
		
				
			<?php if($redirect):?>
				<hr/>
				<h4>Edytuj</h4>
				<?php $i=0;?>
				<?php foreach($redirect as $id=>$value):?>
				<?php $i++?>
				<div class="row" style="margin-bottom: 5px">
					
					<div class="col-lg-3">
						<input placeholder="<?php echo __("source url")?>" type="text" name="redirect[<?php echo $i?>][source]" class="form-control" value="<?php echo $value['source']?>">
					</div>
					<div class="col-lg-6">
						<input placeholder="<?php echo __("target url")?>" type="text" name="redirect[<?php echo $i?>][target]" class="form-control" value="<?php echo $value['target']?>">
					</div>
				</div>	
				<?php endforeach?>
			<?php endif?>
			
			<a href="javascript:void(0)" id="save" class="btn btn-success"><?php echo __('Save')?></a>
			<?php echo input_hidden_tag("language",$sf_request->getParameter("language","pl"))?>
		</form>
		<div class="output">
	
		</div>
	</div>
</div>

<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">

$(document).on('click', '.csfRedirectPlugin a#save', function () {
	
	for ( instance in CKEDITOR.instances ) CKEDITOR.instances[instance].updateElement();
	
	 $.ajax({
         type: "POST",
         url: '<?php echo url_for("@csfRedirect?action=update")?>',
         data: $(".csfRedirectPlugin form").serialize(), // serializes the form's elements.
         success: function(data)
         {
             $(".csfRedirectPlugin-wrapper").html(data); // show response from the php script.
         }
       });

	
  return false; // avoid to execute the actual submit of the form.	
})

</script>
<?php echo end_slot()?>

