<?php $config = new Config;?>

<div class="csfGoogleAdwords">
	<form method="post">
		<?php echo textarea_tag("google_adwords_tracking_code",$config->getOption("csfGoogleAdwords",$sf_request->getParameter("language","pl")),array("class"=>"form-control"))?><br/>
		<a href="javascript:void(0)" id="save" class="btn btn-success"><?php echo __('Save')?></a>
		<?php echo input_hidden_tag("language",$sf_request->getParameter("language","pl"))?>
	</form>
	<div class="output">

	</div>
</div>

<?php append_to_slot('additional_javascript') ?>
<script type="text/javascript">
$(document).on('click', '.csfGoogleAdwords a#save', function () {
	 $.ajax({
         type: "POST",
         url: '<?php echo url_for("@csfGoogleAdwords?action=update")?>',
         data: $(".csfGoogleAdwords form").serialize(), // serializes the form's elements.
         success: function(data)
         {
             $(".csfGoogleAdwords .output").html(data); // show response from the php script.
         }
       });

  return false; // avoid to execute the actual submit of the form.
	
})
</script>
<?php echo end_slot()?>