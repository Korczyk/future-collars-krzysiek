<?php


class csfAnalyticsHeadAppendFilter extends sfFilter
{
	public function execute($filterChain)
	{
		$config = new Config();
		
		$request = $this->getContext()->getRequest();
		$response = $this->getContext()->getResponse();
		$user    = $this->getContext()->getUser();
		$controller = $this->getContext()->getController();
			
		$response->setContent(str_replace("</head>", $config->getOption("csfGoogleAnalytics")."</head>", $response->getContent()));
		$filterChain->execute();
		
		

	}
}




?>