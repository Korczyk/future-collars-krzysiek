<?php
class Filtration{

	var $filters = array();


	public function __construct(array $array){
		$this->data = $array;

	}

	public static function factory(array $array){
		return new Filtration($array);
	}

	public function filter($field = "", $callback = "", $parameters = array()){

		if(is_null($field)){
			foreach($this->data as $key=>$value){
				$this->filters[$key][]=array("callback"=>$callback,"parameters"=>$parameters);
			}
		}
		if((!$field or !isset($this->data[$field])) and !is_null($field)){
			throw new Exception("No field name or field name not in data");
		}
		elseif(!$callback or (!method_exists('Filtration', $callback) and !function_exists($callback))){
			throw new Exception("No callback name or callback doesn't exists");
		}
		else{
			$this->filters[$field][]=array("callback"=>$callback,"parameters"=>$parameters);
		}
		return $this;
	}

	public function apply(){
		$data=array();

		foreach($this->filters as $field=>$filters){
			foreach($filters as $filter){

				if(isset($this->data[$field])){
					if(method_exists('Filter', $filter["callback"])){
						$data[$field] = call_user_func_array(array(new Filter,$filter["callback"]),array($this->data[$field],$filter["parameters"]));

					}
					elseif(function_exists($filter["callback"])){
						array_unshift($filter["parameters"],$this->data[$field]);
						$data[$field] = call_user_func_array($filter["callback"],$filter["parameters"]);


					}
					else{

					}
				}
			}
		}
		return $this->data;
	}
}