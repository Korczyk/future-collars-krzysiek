<?php
class Validation{

	var $rules = array();

	
	public function __construct(array $array){
		$this->data = $array;
		
	}

	public static function factory(array $array){
		return new Validation($array);
	}

	public function addRule($field = "", $callback = "", $parameters = array()){

		
		if(!$field or !isset($this->data[$field])){
			echo $field;
			exit();
			//throw new Exception("No field name or field name not in data");
		}
		
		
		if($callback and (method_exists('Valid', $callback))){

		}
		else{
			if(function_exists($callback)){

			}
			else{
				throw new Exception("No callback name or callback doesn't exists");
			}	
		}
		
		
		
		$this->rules[$field][]=array("callback"=>$callback,"parameters"=>$parameters);
		
		return $this;
	}

	public function check(){
		
		
		
		$errors = array();
		foreach($this->rules as $field=>$rules){
			
			foreach($rules as $rule){
				if(isset($this->data[$field])){
				
					if(method_exists('Valid', $rule["callback"])){
						$passed = call_user_func_array(array(new Valid,$rule["callback"]),$rule["parameters"]);
						if(!$passed){
							$errors[$field][$rule["callback"]]="error";
						}
					}
					elseif(function_exists($rule["callback"])){
						$passed = call_user_func_array('in_array',array($this->data[$field],$rule["parameters"]));
						if(!$passed){
							$errors[$field][$rule["callback"]]="error";
						}
					}
				}
			}
		}
		
		sfContext::getInstance()->getUser()->setFlash("validation_erros", $errors);
		sfContext::getInstance()->getUser()->setFlash("validation_data", $this->data);
		
		$this->errors=$errors;
		return $this;
	}

	public function getErrors(){
		return $this->errors;
	}


}