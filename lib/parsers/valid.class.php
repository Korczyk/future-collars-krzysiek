<?php

class Valid{

	public static function alpha($str){
		$str = (string) $str;
		return (bool) preg_match('/^\pL++$/uD', $str);
	}

	public static function alpha_dash($str){
		$regex = '/^[-\pL\pN_]++$/uD';
		return (bool) preg_match($regex, $str);
	}

	public static function alpha_numeric($str){
		return (bool) preg_match('/^[\pL\pN]++$/uD', $str);
	}

	public static function decimal($str, $places = 2, $digits = NULL){
		if($digits>0){
			$digits = '{'.( (int) $digits).'}';
		}
		else{
			$digits = '+';
		}

		list($decimal) = array_values(localeconv());
		return (bool) preg_match('/^[+-]?[0-9]'.$digits.preg_quote($decimal).'[0-9]{'.( (int) $places).'}$/D', $str);
	}

	public static function digit($str){
		return (bool) preg_match('/^\pN++$/uD', $str);
	}

	public static function email($email, $strict = FALSE){
		if (mb_strlen($email,"UTF-8") > 254){
			return FALSE;
		}

		if ($strict === TRUE){
			$qtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
			$dtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
			$atom  = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
			$pair  = '\\x5c[\\x00-\\x7f]';

			$domain_literal = "\\x5b($dtext|$pair)*\\x5d";
			$quoted_string  = "\\x22($qtext|$pair)*\\x22";
			$sub_domain     = "($atom|$domain_literal)";
			$word           = "($atom|$quoted_string)";
			$domain         = "$sub_domain(\\x2e$sub_domain)*";
			$local_part     = "$word(\\x2e$word)*";

			$expression     = "/^$local_part\\x40$domain$/D";
		}
		else
		{
			$expression = '/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})$/iD';
		}

		return (bool) preg_match($expression, (string) $email);
	}


	public static function equals($value, $required){
		return ($value === $required);
	}



	public static function email_domain($email){
		if ( ! Valid::not_empty($email))
			return FALSE; // Empty fields cause issues with checkdnsrr()

		return (bool) checkdnsrr(preg_replace('/^[^@]++@/', '', $email), 'MX');
	}

	public static function exact_length($value, $length){
		if (is_array($length))
		{
			foreach($length as $strlen)
			{
				if (mb_strlen($value,"UTF-8") === $strlen)
					return TRUE;
			}
			return FALSE;
		}
		return mb_strlen($value,"UTF-8") === $length;
	}


	public static function matches($array, $field, $match){
		return ($array[$field] === $array[$match]);
	}


	public static function max_length($value, $length){
		return mb_strlen($value,"UTF-8") <= $length;
	}


	public static function min_length($value, $length){
		return mb_strlen($value,"UTF-8") >= $length;
	}
	
	public static function conditional_min_length($value, $length){
		if(mb_strlen($value,"UTF-8")){
			return mb_strlen($value,"UTF-8") >= $length;
		}
		else{
			return true;
		}
		
	}

	public static function not_empty($str){
		$str = (string) $str;
		return ! in_array($str, array(NULL, FALSE, '', array()), TRUE);
	}


	public static function numeric($str){
		list($decimal) = array_values(localeconv());
		return (bool) preg_match('/^-?+(?=.*[0-9])[0-9]*+'.preg_quote($decimal).'?+[0-9]*+$/D', (string) $str);
	}

	public static function phone($number, $lengths = NULL){
		if ( ! is_array($lengths)){
			$lengths = array(7,10,11);
		}
		$number = preg_replace('/\D+/', '', $number);
		return in_array(strlen($number), $lengths);
	}


	public static function range($number, $min, $max){
		return ($number >= $min AND $number <= $max);
	}


	public static function regex($value, $expression){
		return (bool) preg_match($expression, (string) $value);
	}

	public static function url($url){
		// Based on http://www.apps.ietf.org/rfc/rfc1738.html#sec-5
		if ( ! preg_match('~^

        # scheme
        [-a-z0-9+.]++://

        # username:password (optional)
        (?:
                [-a-z0-9$_.+!*\'(),;?&=%]++   # username
            (?::[-a-z0-9$_.+!*\'(),;?&=%]++)? # password (optional)
            @
        )?

        (?:
            # ip address
            \d{1,3}+(?:\.\d{1,3}+){3}+

            | # or

            # hostname (captured)
            (
                     (?!-)[-a-z0-9]{1,63}+(?<!-)
                (?:\.(?!-)[-a-z0-9]{1,63}+(?<!-)){0,126}+
            )
        )

        # port (optional)
        (?::\d{1,5}+)?

        # path (optional)
        (?:/.*)?

        $~iDx', $url, $matches))
			return FALSE;

		// We matched an IP address
		if ( ! isset($matches[1]))
			return TRUE;

		// Check maximum length of the whole hostname
		// http://en.wikipedia.org/wiki/Domain_name#cite_note-0
		if (strlen($matches[1]) > 253)
			return FALSE;

		// An extra check for the top level domain
		// It must start with a letter
		$tld = ltrim(substr($matches[1], (int) strrpos($matches[1], '.')), '.');
		return ctype_alpha($tld[0]);
	}



}