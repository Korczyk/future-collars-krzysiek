<?php


function utf8_strlower($string =''){

	return mb_strtolower($string,'UTF-8');

}
function utf8_strupper($string =''){

	return mb_strtoupper($string,'UTF-8');

}

function utf8_strtitle($string =''){

	return mb_convert_case($string,MB_CASE_TITLE,'UTF-8');

}

function utf8_substr($str,$from,$len){
	return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$from.'}'.
			'((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len.'}).*#s',
			'$1',$str);
}

function utf8_strlen($string){
	return strlen(utf8_decode($string));
}


function substr_wrap($str, $length, $minword = 3)
{
	$sub = '';
	$len = 0;

	foreach (explode(' ', $str) as $word)
	{
		$part = (($sub != '') ? ' ' : '') . $word;
		$sub .= $part;
		$len += strlen($part);

		if (strlen($word) > $minword && strlen($sub) >= $length)
		{
			break;
		}
	}

	return $sub . (($len < strlen($str)) ? '...' : '');
}
