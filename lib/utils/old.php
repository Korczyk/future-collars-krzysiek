<?php
class Utils{

	public static function normalizeName($string)
	{
		$in = array( '/_/', '/-/', '/ä/', '/ö/', '/ü/', '/Ä/', '/Ö/', '/Ü/', '/ß/',
				'/ą/', '/Ą/', '/ć/', '/Ć/', '/ę/', '/Ę/', '/ł/', '/Ł/' ,'/ń/', '/Ń/', '/ó/', '/Ó/', '/ś/', '/Ś/', '/ź/', '/Ź/', '/ż/', '/Ż/',
				'/Š/','/Ž/','/š/','/ž/','/Ÿ/','/Ŕ/','/Á/','/Â/','/Ă/','/Ä/','/Ĺ/','/Ç/','/Č/','/É/','/Ę/','/Ë/','/Ě/','/Í/','/Î/','/Ď/','/Ń/',
				'/Ň/','/Ó/','/Ô/','/Ő/','/Ö/','/Ř/','/Ů/','/Ú/','/Ű/','/Ü/','/Ý/','/ŕ/','/á/','/â/','/ă/','/ä/','/ĺ/','/ç/','/č/','/é/','/ę/',
				'/ë/','/ě/','/í/','/î/','/ď/','/ń/','/ň/','/ó/','/ô/','/ő/','/ö/','/ř/','/ů/','/ú/','/ű/','/ü/','/ý/','/˙/',
				'/Ţ/','/ţ/','/Đ/','/đ/','/ß/','/Œ/','/œ/','/Ć/','/ć/','/ľ/','/&Oacute;/','/&oacute;/');
	
		$out   = array(' ',' ','ae', 'oe', 'ue', 'Ae', 'Oe', 'Ue', 'ss',
				'a', 'A', 'c', 'C', 'e', 'E', 'l', 'L', 'n', 'N', 'o', 'O', 's', 'S', 'z', 'Z', 'z', 'Z',
				'S','Z','s','z','Y','R','A','A','A','A','L','C','C','E','E','E','E','I','I','D','N',
				'N','O','O','O','O','R','U','U','U','U','Y','r','a','a','a','a','l','c','c','e','e',
				'e','e','i','i','i','n','o','o','o','o','o','r','u','u','u','u','y','y',
				'TH','th','DH','dh','ss','OE','oe','AE','ae','u','O','o');
	
		$permalink = preg_replace($in, $out, $string);
	
		return  mb_strtolower(preg_replace("/[ ]+/", "-" , preg_replace("/[^a-zA-Z0-9 ]/", "", $permalink)),'UTF-8');
	}
	
	public static final function normalizeString($string, $separator = ' ')
	{
	
	
		$in = array('/ä/', '/ö/', '/ü/', '/Ä/', '/Ö/', '/Ü/', '/ß/',
				'/ą/', '/Ą/', '/ć/', '/Ć/', '/ę/', '/Ę/', '/ł/', '/Ł/' ,'/ń/', '/Ń/', '/ó/', '/Ó/', '/ś/', '/Ś/', '/ź/', '/Ź/', '/ż/', '/Ż/',
				'/Š/','/Ž/','/š/','/ž/','/Ÿ/','/Ŕ/','/Á/','/Â/','/Ă/','/Ä/','/Ĺ/','/Ç/','/Č/','/É/','/Ę/','/Ë/','/Ě/','/Í/','/Î/','/Ď/','/Ń/',
				'/Ň/','/Ó/','/Ô/','/Ő/','/Ö/','/Ř/','/Ů/','/Ú/','/Ű/','/Ü/','/Ý/','/ŕ/','/á/','/â/','/ă/','/ä/','/ĺ/','/ç/','/č/','/é/','/ę/',
				'/ë/','/ě/','/í/','/î/','/ď/','/ń/','/ň/','/ó/','/ô/','/ő/','/ö/','/ř/','/ů/','/ú/','/ű/','/ü/','/ý/','/˙/',
				'/Ţ/','/ţ/','/Đ/','/đ/','/ß/','/Œ/','/œ/','/Ć/','/ć/','/ľ/','/&oacute;/','/&Oacute;/');
	
		$out   = array('ae', 'oe', 'ue', 'Ae', 'Oe', 'Ue', 'ss',
				'a', 'A', 'c', 'C', 'e', 'E', 'l', 'L', 'n', 'N', 'o', 'O', 's', 'S', 'z', 'Z', 'z', 'Z',
				'S','Z','s','z','Y','R','A','A','A','A','L','C','C','E','E','E','E','I','I','D','N',
				'N','O','O','O','O','R','U','U','U','U','Y','r','a','a','a','a','l','c','c','e','e',
				'e','e','i','i','i','n','o','o','o','o','o','r','u','u','u','u','y','y',
				'TH','th','DH','dh','ss','OE','oe','AE','ae','u','O','o');
	
	
	
	
	
		$permalink = preg_replace($in, $out, $string);
		return  mb_strtolower(preg_replace("/[ ]+/", ' ' , preg_replace("/[^a-zA-Z0-9\x{0430}-\x{044F}\x{0410}-\x{042F} ]/u", "", $permalink)),'UTF-8');
	}
	
	public static function installPlugin($plugin_name){
		
		if(file_exists(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/css")){
			if(file_exists(sfConfig::get('sf_web_dir')."/css/plugins/".$plugin_name)){
				/*
				if ($handle = @opendir(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/css")) {
					while (false !== ($file = readdir($handle))) {
						if ($file != '.' && $file != '..' && substr($file,0,1) != '.'){
							copy(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/css/".$file,sfConfig::get('sf_web_dir')."/css/plugins/".$plugin_name."/".$file);
						}
					}
					closedir($handle);
				}
				*/
			}
			else{
				
				symlink('/home/username/public_html/directory1', '/home/username/public_html/directory2');
				
				mkdir(sfConfig::get('sf_web_dir')."/css/plugins/".$plugin_name, 0755);
				/*
				if ($handle = @opendir(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/css")) {
					while (false !== ($file = readdir($handle))) {
						if ($file != '.' && $file != '..' && substr($file,0,1) != '.'){
							copy(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/css/".$file,sfConfig::get('sf_web_dir')."/css/plugins/".$plugin_name."/".$file);
						}
					}
					closedir($handle);
				}
				*/
		
			}
		}
		if(file_exists(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/js")){
			if(file_exists(sfConfig::get('sf_web_dir')."/js/plugins/".$plugin_name)){
				/*if ($handle = @opendir(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/js")) {
					while (false !== ($file = readdir($handle))) {
						if ($file != '.' && $file != '..' && substr($file,0,1) != '.'){
							copy(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/js/".$file,sfConfig::get('sf_web_dir')."/js/plugins/".$plugin_name."/".$file);
						}
					}
					closedir($handle);
				}
				*/
			}
			else{
				mkdir(sfConfig::get('sf_web_dir')."/js/plugins/".$plugin_name, 0755);
				/*if ($handle = @opendir(sfConfig::get("sf_plugins_dir")."/".$plugin_name."/web/js")) {
					while (false !== ($file = readdir($handle))) {
						if ($file != '.' && $file != '..' && substr($file,0,1) != '.'){
							copy(sfConfig::get("sf_plugins_dir")."/".$plugin_name."/web/js/".$file,sfConfig::get('sf_web_dir')."/js/plugins/".$plugin_name."/".$file);
						}
					}
					closedir($handle);
				}
				*/
			}
		}
		if(file_exists(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/images")){
			if(file_exists(sfConfig::get('sf_web_dir')."/images/plugins/".$plugin_name)){
				/*if ($handle = @opendir(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/images")) {
					while (false !== ($file = readdir($handle))) {
						if ($file != '.' && $file != '..' && substr($file,0,1) != '.'){
							copy(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/images/".$file,sfConfig::get('sf_web_dir')."/images/plugins/".$plugin_name."/".$file);
						}
					}
					closedir($handle);
				}
				*/
			}
			else{
				mkdir(sfConfig::get('sf_web_dir')."/images/plugins/".$plugin_name, 0755);
				/*if ($handle = @opendir(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/images")) {
					while (false !== ($file = readdir($handle))) {
						if ($file != '.' && $file != '..'){
							copy(sfConfig::get("sf_plugins_dir")."/".$plugin_name."Plugin/web/images/".$file,sfConfig::get('sf_web_dir')."/images/plugins/".$plugin_name."/".$file);
						}
					}
					closedir($handle);
				}
				*/
			}
		}
	}


}
?>