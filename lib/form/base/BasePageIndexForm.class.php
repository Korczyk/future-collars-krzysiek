<?php

/**
 * PageIndex form base class.
 *
 * @package    form
 * @subpackage page_index
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BasePageIndexForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'      => new sfWidgetFormInputHidden(),
      'page_id' => new sfWidgetFormPropelSelect(array('model' => 'Page', 'add_empty' => false)),
      'word'    => new sfWidgetFormInput(),
      'weight'  => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'      => new sfValidatorPropelChoice(array('model' => 'PageIndex', 'column' => 'id', 'required' => false)),
      'page_id' => new sfValidatorPropelChoice(array('model' => 'Page', 'column' => 'id')),
      'word'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'weight'  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('page_index[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PageIndex';
  }


}
