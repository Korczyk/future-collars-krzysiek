<?php

/**
 * User form base class.
 *
 * @package    form
 * @subpackage user
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseUserForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'name'        => new sfWidgetFormInput(),
      'email'       => new sfWidgetFormInput(),
      'role'        => new sfWidgetFormInput(),
      'description' => new sfWidgetFormInput(),
      'is_deleted'  => new sfWidgetFormInput(),
      'passwd'      => new sfWidgetFormInput(),
      'salt'        => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorPropelChoice(array('model' => 'User', 'column' => 'id', 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'email'       => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'role'        => new sfValidatorInteger(array('required' => false)),
      'description' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'is_deleted'  => new sfValidatorInteger(array('required' => false)),
      'passwd'      => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'salt'        => new sfValidatorString(array('max_length' => 32, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('user[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'User';
  }


}
