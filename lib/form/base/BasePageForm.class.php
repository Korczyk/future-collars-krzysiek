<?php

/**
 * Page form base class.
 *
 * @package    form
 * @subpackage page
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BasePageForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'language'     => new sfWidgetFormInput(),
      'name'         => new sfWidgetFormInput(),
      'name_menu'    => new sfWidgetFormInput(),
      'slug'         => new sfWidgetFormInput(),
      'mega_slug'    => new sfWidgetFormInput(),
      'content'      => new sfWidgetFormTextarea(),
      'content_2'    => new sfWidgetFormTextarea(),
      'parent'       => new sfWidgetFormInput(),
      'descendants'  => new sfWidgetFormTextarea(),
      'children'     => new sfWidgetFormTextarea(),
      'is_published' => new sfWidgetFormInput(),
      'path'         => new sfWidgetFormInput(),
      'stats'        => new sfWidgetFormTextarea(),
      'kind'         => new sfWidgetFormInput(),
      'is_deleted'   => new sfWidgetFormInput(),
      'position'     => new sfWidgetFormInput(),
      'is_featured'  => new sfWidgetFormInput(),
      'is_expanded'  => new sfWidgetFormInput(),
      'external_url' => new sfWidgetFormInput(),
      'passwd'       => new sfWidgetFormInput(),
      'options'      => new sfWidgetFormTextarea(),
      'created_at'   => new sfWidgetFormDateTime(),
      'created_by'   => new sfWidgetFormInput(),
      'modified_at'  => new sfWidgetFormDateTime(),
      'modified_by'  => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorPropelChoice(array('model' => 'Page', 'column' => 'id', 'required' => false)),
      'language'     => new sfValidatorString(array('max_length' => 32)),
      'name'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'name_menu'    => new sfValidatorString(array('max_length' => 255)),
      'slug'         => new sfValidatorString(array('max_length' => 255)),
      'mega_slug'    => new sfValidatorString(array('max_length' => 1024)),
      'content'      => new sfValidatorString(array('required' => false)),
      'content_2'    => new sfValidatorString(array('required' => false)),
      'parent'       => new sfValidatorInteger(array('required' => false)),
      'descendants'  => new sfValidatorString(array('required' => false)),
      'children'     => new sfValidatorString(array('required' => false)),
      'is_published' => new sfValidatorInteger(array('required' => false)),
      'path'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'stats'        => new sfValidatorString(array('required' => false)),
      'kind'         => new sfValidatorInteger(array('required' => false)),
      'is_deleted'   => new sfValidatorInteger(array('required' => false)),
      'position'     => new sfValidatorInteger(array('required' => false)),
      'is_featured'  => new sfValidatorInteger(),
      'is_expanded'  => new sfValidatorInteger(),
      'external_url' => new sfValidatorString(array('max_length' => 1024)),
      'passwd'       => new sfValidatorString(array('max_length' => 32)),
      'options'      => new sfValidatorString(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'created_by'   => new sfValidatorInteger(array('required' => false)),
      'modified_at'  => new sfValidatorDateTime(array('required' => false)),
      'modified_by'  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('page[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Page';
  }


}
