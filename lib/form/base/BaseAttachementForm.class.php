<?php

/**
 * Attachement form base class.
 *
 * @package    form
 * @subpackage attachement
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseAttachementForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'page_id'     => new sfWidgetFormPropelSelect(array('model' => 'Page', 'add_empty' => false)),
      'url'         => new sfWidgetFormInput(),
      'description' => new sfWidgetFormInput(),
      'created_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorPropelChoice(array('model' => 'Attachement', 'column' => 'id', 'required' => false)),
      'page_id'     => new sfValidatorPropelChoice(array('model' => 'Page', 'column' => 'id')),
      'url'         => new sfValidatorString(array('max_length' => 1024, 'required' => false)),
      'description' => new sfValidatorString(array('max_length' => 1024, 'required' => false)),
      'created_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('attachement[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Attachement';
  }


}
