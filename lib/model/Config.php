<?php

/**
 * Subclass for representing a row from the 'config' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Config extends BaseConfig
{
	public function buildConfig(){
	
		$conn = Propel::getConnection();
		$query = 'select name, value from config where name like "%_'.sfConfig::get('sf_dimension').'"';
		$rs = $conn->executeQuery($query);
		$options=array();
		while($rs->next()){
			$options[$rs->getString('name')]=$rs->getString('value');
		}		
		$this->options=$options;
	
	}
	
	
	
	public function getOption($name,$language = null){
		
		if(!sfConfig::has("cms_config")){
			$this->buildConfig();
			sfConfig::set("cms_config",base64_encode(serialize($this->options)));
		}
		else{
			if(!isset($this->options)){
				$this->options = unserialize(base64_decode(sfConfig::get('cms_config')));
			}
		}
		if($language){
			if(isset($this->options[$name."_".$language])){
				return $this->options[$name."_".$language];
			}
			else return null;
		}
		else{
			if(isset($this->options[$name."_".sfConfig::get('sf_dimension')])){
				return $this->options[$name."_".sfConfig::get('sf_dimension')];
			}
			else return null;
		}
		
	}
}
