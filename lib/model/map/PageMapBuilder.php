<?php



class PageMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.PageMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('page');
		$tMap->setPhpName('Page');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('LANGUAGE', 'Language', 'string', CreoleTypes::VARCHAR, true, 32);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('NAME_MENU', 'NameMenu', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('SLUG', 'Slug', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('MEGA_SLUG', 'MegaSlug', 'string', CreoleTypes::VARCHAR, true, 1024);

		$tMap->addColumn('CONTENT', 'Content', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CONTENT_2', 'Content2', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('PARENT', 'Parent', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('DESCENDANTS', 'Descendants', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CHILDREN', 'Children', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IS_PUBLISHED', 'IsPublished', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('PATH', 'Path', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('STATS', 'Stats', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KIND', 'Kind', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('IS_DELETED', 'IsDeleted', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('POSITION', 'Position', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('IS_FEATURED', 'IsFeatured', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('IS_EXPANDED', 'IsExpanded', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('EXTERNAL_URL', 'ExternalUrl', 'string', CreoleTypes::VARCHAR, true, 1024);

		$tMap->addColumn('PASSWD', 'Passwd', 'string', CreoleTypes::VARCHAR, true, 32);

		$tMap->addColumn('OPTIONS', 'Options', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('CREATED_BY', 'CreatedBy', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('MODIFIED_AT', 'ModifiedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('MODIFIED_BY', 'ModifiedBy', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 