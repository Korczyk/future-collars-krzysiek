<?php



class PageIndexMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.PageIndexMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('page_index');
		$tMap->setPhpName('PageIndex');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('PAGE_ID', 'PageId', 'int', CreoleTypes::INTEGER, 'page', 'ID', true, null);

		$tMap->addColumn('WORD', 'Word', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('WEIGHT', 'Weight', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 