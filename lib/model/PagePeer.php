<?php

class PagePeer extends BasePagePeer
{
	
	public static function removeStopWordsFromArray($words){
	
		$stop_words = array('na','pod','i','z','jak', 'a', 'jest','są');
		return array_diff($words, $stop_words);
	}
	
	public static function stemPhrase($phrase){
		$phrase=Utils::normalizeString($phrase);
	
		// split into words
		$words=str_word_count($phrase, 1);
		$words=preg_split("/[\s, ]+/", $phrase);
	
		// ignore stop words
		$words = PagePeer::removeStopWordsFromArray($words);
	
	
		// stem words
		$stemmed_words = array();
		foreach ($words as $word)
		{
			// ignore 1 and 2 letter words
			if (strlen(utf8_decode($word)) <= 2)
			{
				continue;
			}
			$stemmed_words[] = $word;
		}
		return $stemmed_words;
	}
	
	public static function search($phrase, $exact = true, $offset = 0, $max = 10){
	
		$conn = Propel::getConnection();
		$words    = array_values(PagePeer::stemPhrase($phrase));
	
		$nb_words = count($words);
		if (!$words){
			return array();
		}
	
		$query = 'select distinct page_id, count(*) as nb, sum(weight) as total_weight from page_index where ';
		$tmp=array();
		foreach($words as $word){
			$tmp[] = "word='".$word."'";
		}
		$query.=implode(' OR ',$tmp);
		$query.=' group by page_id  ';
		if ($exact)
		{
			$query .= ' having nb = '.$nb_words;
		}
		$query.=' order by total_weight desc, nb desc ';
	
	
	
		$rs=$conn->executeQuery($query);
		$keys=array();
		while($rs->next()){
			$keys[]=$rs->getInt('page_id');
		}
		return $keys;
	
	
	
	
	
	}
	
	public static function getTemplates(){
		$files=array();
		$templates=array();
		
		if ($handle = opendir(sfConfig::get("sf_apps_dir")."/frontend/modules/page/templates/")) {
			
		
			while (false !== ($entry = readdir($handle))) {
				
				preg_match("/template[0-9]Success\.php/",$entry,$match);
				if($match){
					$files[]=$match[0];
				}
			}

					
		
			closedir($handle);
		}
		
		foreach($files as $file){
			$content=file(sfConfig::get("sf_apps_dir")."/frontend/modules/page/templates/".$file);
			preg_match("/template([0-9])Success\.php/",basename($file),$match);
			$id = $match[1];
		
			if(isset($content[0])){
				preg_match("/\*(.*)\*/",$content[0],$match);
				if(isset($match[1])){
					$templates[$id]=$match[1];
				}
		
			}
		}
		
		
		return $templates;
	}
	
	public static function buildtree($branch, &$flattenTree=array()){
		if(!empty($branch)){
			foreach($branch as $k=>$v){
				$flattenTree[]=$k;
				PagePeer::buildtree($v,$flattenTree);
			}
		}
		return $flattenTree;
	}
	
	
	
	
	public static function buildMegaslug($id=0, $path=array(), $pages = array()){
		$path = array_reverse($path);
		
		$slug=array();
		foreach($path as $p){
			$slug[] = $pages[$p]->getSlug();
		}
		
		if($slug){
			return implode("/",$slug);
		}
		else{
			return $pages[$id]->getSlug();
		}
		
	}
	public static function buildPath($id=0, $path=array(), $pages = array()){
		if($id){
			$page=$pages[$id];
			$path[]=$page->getId();
			if($page->getParent()!=0){
				return PagePeer::buildPath($page->getParent(), $path,$pages);
			}	
		}
		return $path;
	}
	
	public static function buildChildren($id=0, $pages=array()){
		$children=array();
		foreach($pages as $page){
			if($page->getParent()==$id and ($page->getKind()==1 or $page->getKind()==3)){
				$children[]=$page->getId();
			}
		}
		return $children;
	}
	
	public static function buildMegaPath($id=0){
		
		if($id){
			
			
			
		}
		else{
			return null;
		}
	}
	
	
	
	public static function rebuildPagesTree(){
		$conn = Propel::getConnection();
		$request = sfContext::getInstance()->getRequest();
		
		$c=new Criteria;
		/*
		$pages=PagePeer::doSelect($c);
		foreach($pages as $page){
			$p[$page->getId()]=$page;
		}
		$pages = $p;
	*/
	$p=array();
        $pages=PagePeer::doSelect($c);
        foreach($pages as $page){
            $p[$page->getId()]=$page;
        }
        $pages = $p;
		
		
		
		if($pages){
			$query = "insert into page (id,path,children,mega_slug) values";
			$tmp=array();
			foreach($pages as $page){
				
				$path = PagePeer::buildPath($page->getId(),array(),$pages);
				
				$megaslug = PagePeer::buildMegaSlug($page->getId(),$path,$pages);
				$children = PagePeer::buildChildren($page->getId(),$pages);
				
				$tmp[]=" (".$page->getId().",'".implode(" ",$path)."','".implode(" ",$children)."','".$megaslug."')";
				
				if($page->getMegaSlug()!=$megaslug){
					csfRedirect::add("http://".$request->getHost()."/".$page->getMegaSlug(), "http://".$request->getHost()."/".$megaslug, $page->getLanguage());
				}
			}
			$query.=implode(",", $tmp);
			$query.=" on duplicate key update path=values(path), children=values(children), mega_slug=values(mega_slug)";
			if($tmp){
				$conn->executeQuery($query);
			}
				
		}
	
		foreach($pages as $page){
			if(!isset($descendants[$page->getId()])){
				$descendants[$page->getId()][]=$page->getId();
			}
			if($page->getChildren()){
				$descendants[$page->getId()]=array_merge($descendants[$page->getId()],explode(' ',$page->getChildren()));
			}
			$descendants[$page->getId()]=array_unique($descendants[$page->getId()]);
	
			if($page->getPath()){
				$path = explode(' ',$page->getPath());
				unset($path[$page->getId()]);
	
				foreach($path as $id){
					if(!isset($descendants[$id])){
						$descendants[$id][]=$id;
					}
					if($page->getChildren()){
						$descendants[$id]=array_merge($descendants[$id],explode(' ',$page->getChildren()));
					}
					$descendants[$id]=array_unique($descendants[$id]);
				}
			}
		}
		
		
		$query = "insert into page (id,descendants) values";
		$tmp=array();
		foreach($pages as $page){
			if(isset($descendants[$page->getId()])){
				if($descendants[$page->getId()]){
					$tmp[]=" (".$page->getId().",'".implode(' ',$descendants[$page->getId()])."')";
				}
			}
		}
		$query.=implode(",", $tmp);
		$query.=" on duplicate key update descendants=values(descendants)";
		if($tmp){
			$conn->executeQuery($query);
		}
		
	}
}
