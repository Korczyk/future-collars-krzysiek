<?php


abstract class BasePage extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $language = '';


	
	protected $name;


	
	protected $name_menu = '';


	
	protected $slug = '';


	
	protected $mega_slug = '';


	
	protected $content;


	
	protected $content_2;


	
	protected $parent;


	
	protected $descendants;


	
	protected $children;


	
	protected $is_published;


	
	protected $path;


	
	protected $stats;


	
	protected $kind;


	
	protected $is_deleted;


	
	protected $position;


	
	protected $is_featured = 0;


	
	protected $is_expanded = 1;


	
	protected $external_url = '';


	
	protected $passwd = '';


	
	protected $options;


	
	protected $created_at;


	
	protected $created_by;


	
	protected $modified_at;


	
	protected $modified_by;

	
	protected $collAttachements;

	
	protected $lastAttachementCriteria = null;

	
	protected $collImages;

	
	protected $lastImageCriteria = null;

	
	protected $collPageIndexs;

	
	protected $lastPageIndexCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getLanguage()
	{

		return $this->language;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getNameMenu()
	{

		return $this->name_menu;
	}

	
	public function getSlug()
	{

		return $this->slug;
	}

	
	public function getMegaSlug()
	{

		return $this->mega_slug;
	}

	
	public function getContent()
	{

		return $this->content;
	}

	
	public function getContent2()
	{

		return $this->content_2;
	}

	
	public function getParent()
	{

		return $this->parent;
	}

	
	public function getDescendants()
	{

		return $this->descendants;
	}

	
	public function getChildren()
	{

		return $this->children;
	}

	
	public function getIsPublished()
	{

		return $this->is_published;
	}

	
	public function getPath()
	{

		return $this->path;
	}

	
	public function getStats()
	{

		return $this->stats;
	}

	
	public function getKind()
	{

		return $this->kind;
	}

	
	public function getIsDeleted()
	{

		return $this->is_deleted;
	}

	
	public function getPosition()
	{

		return $this->position;
	}

	
	public function getIsFeatured()
	{

		return $this->is_featured;
	}

	
	public function getIsExpanded()
	{

		return $this->is_expanded;
	}

	
	public function getExternalUrl()
	{

		return $this->external_url;
	}

	
	public function getPasswd()
	{

		return $this->passwd;
	}

	
	public function getOptions()
	{

		return $this->options;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getCreatedBy()
	{

		return $this->created_by;
	}

	
	public function getModifiedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->modified_at === null || $this->modified_at === '') {
			return null;
		} elseif (!is_int($this->modified_at)) {
						$ts = strtotime($this->modified_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [modified_at] as date/time value: " . var_export($this->modified_at, true));
			}
		} else {
			$ts = $this->modified_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getModifiedBy()
	{

		return $this->modified_by;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = PagePeer::ID;
		}

	} 
	
	public function setLanguage($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->language !== $v || $v === '') {
			$this->language = $v;
			$this->modifiedColumns[] = PagePeer::LANGUAGE;
		}

	} 
	
	public function setName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = PagePeer::NAME;
		}

	} 
	
	public function setNameMenu($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name_menu !== $v || $v === '') {
			$this->name_menu = $v;
			$this->modifiedColumns[] = PagePeer::NAME_MENU;
		}

	} 
	
	public function setSlug($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->slug !== $v || $v === '') {
			$this->slug = $v;
			$this->modifiedColumns[] = PagePeer::SLUG;
		}

	} 
	
	public function setMegaSlug($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->mega_slug !== $v || $v === '') {
			$this->mega_slug = $v;
			$this->modifiedColumns[] = PagePeer::MEGA_SLUG;
		}

	} 
	
	public function setContent($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->content !== $v) {
			$this->content = $v;
			$this->modifiedColumns[] = PagePeer::CONTENT;
		}

	} 
	
	public function setContent2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->content_2 !== $v) {
			$this->content_2 = $v;
			$this->modifiedColumns[] = PagePeer::CONTENT_2;
		}

	} 
	
	public function setParent($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->parent !== $v) {
			$this->parent = $v;
			$this->modifiedColumns[] = PagePeer::PARENT;
		}

	} 
	
	public function setDescendants($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->descendants !== $v) {
			$this->descendants = $v;
			$this->modifiedColumns[] = PagePeer::DESCENDANTS;
		}

	} 
	
	public function setChildren($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->children !== $v) {
			$this->children = $v;
			$this->modifiedColumns[] = PagePeer::CHILDREN;
		}

	} 
	
	public function setIsPublished($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_published !== $v) {
			$this->is_published = $v;
			$this->modifiedColumns[] = PagePeer::IS_PUBLISHED;
		}

	} 
	
	public function setPath($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->path !== $v) {
			$this->path = $v;
			$this->modifiedColumns[] = PagePeer::PATH;
		}

	} 
	
	public function setStats($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->stats !== $v) {
			$this->stats = $v;
			$this->modifiedColumns[] = PagePeer::STATS;
		}

	} 
	
	public function setKind($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->kind !== $v) {
			$this->kind = $v;
			$this->modifiedColumns[] = PagePeer::KIND;
		}

	} 
	
	public function setIsDeleted($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_deleted !== $v) {
			$this->is_deleted = $v;
			$this->modifiedColumns[] = PagePeer::IS_DELETED;
		}

	} 
	
	public function setPosition($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->position !== $v) {
			$this->position = $v;
			$this->modifiedColumns[] = PagePeer::POSITION;
		}

	} 
	
	public function setIsFeatured($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_featured !== $v || $v === 0) {
			$this->is_featured = $v;
			$this->modifiedColumns[] = PagePeer::IS_FEATURED;
		}

	} 
	
	public function setIsExpanded($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_expanded !== $v || $v === 1) {
			$this->is_expanded = $v;
			$this->modifiedColumns[] = PagePeer::IS_EXPANDED;
		}

	} 
	
	public function setExternalUrl($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->external_url !== $v || $v === '') {
			$this->external_url = $v;
			$this->modifiedColumns[] = PagePeer::EXTERNAL_URL;
		}

	} 
	
	public function setPasswd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->passwd !== $v || $v === '') {
			$this->passwd = $v;
			$this->modifiedColumns[] = PagePeer::PASSWD;
		}

	} 
	
	public function setOptions($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->options !== $v) {
			$this->options = $v;
			$this->modifiedColumns[] = PagePeer::OPTIONS;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = PagePeer::CREATED_AT;
		}

	} 
	
	public function setCreatedBy($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->created_by !== $v) {
			$this->created_by = $v;
			$this->modifiedColumns[] = PagePeer::CREATED_BY;
		}

	} 
	
	public function setModifiedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [modified_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->modified_at !== $ts) {
			$this->modified_at = $ts;
			$this->modifiedColumns[] = PagePeer::MODIFIED_AT;
		}

	} 
	
	public function setModifiedBy($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->modified_by !== $v) {
			$this->modified_by = $v;
			$this->modifiedColumns[] = PagePeer::MODIFIED_BY;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->language = $rs->getString($startcol + 1);

			$this->name = $rs->getString($startcol + 2);

			$this->name_menu = $rs->getString($startcol + 3);

			$this->slug = $rs->getString($startcol + 4);

			$this->mega_slug = $rs->getString($startcol + 5);

			$this->content = $rs->getString($startcol + 6);

			$this->content_2 = $rs->getString($startcol + 7);

			$this->parent = $rs->getInt($startcol + 8);

			$this->descendants = $rs->getString($startcol + 9);

			$this->children = $rs->getString($startcol + 10);

			$this->is_published = $rs->getInt($startcol + 11);

			$this->path = $rs->getString($startcol + 12);

			$this->stats = $rs->getString($startcol + 13);

			$this->kind = $rs->getInt($startcol + 14);

			$this->is_deleted = $rs->getInt($startcol + 15);

			$this->position = $rs->getInt($startcol + 16);

			$this->is_featured = $rs->getInt($startcol + 17);

			$this->is_expanded = $rs->getInt($startcol + 18);

			$this->external_url = $rs->getString($startcol + 19);

			$this->passwd = $rs->getString($startcol + 20);

			$this->options = $rs->getString($startcol + 21);

			$this->created_at = $rs->getTimestamp($startcol + 22, null);

			$this->created_by = $rs->getInt($startcol + 23);

			$this->modified_at = $rs->getTimestamp($startcol + 24, null);

			$this->modified_by = $rs->getInt($startcol + 25);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 26; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Page object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PagePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PagePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(PagePeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PagePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PagePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PagePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collAttachements !== null) {
				foreach($this->collAttachements as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collImages !== null) {
				foreach($this->collImages as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collPageIndexs !== null) {
				foreach($this->collPageIndexs as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PagePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collAttachements !== null) {
					foreach($this->collAttachements as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collImages !== null) {
					foreach($this->collImages as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collPageIndexs !== null) {
					foreach($this->collPageIndexs as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getLanguage();
				break;
			case 2:
				return $this->getName();
				break;
			case 3:
				return $this->getNameMenu();
				break;
			case 4:
				return $this->getSlug();
				break;
			case 5:
				return $this->getMegaSlug();
				break;
			case 6:
				return $this->getContent();
				break;
			case 7:
				return $this->getContent2();
				break;
			case 8:
				return $this->getParent();
				break;
			case 9:
				return $this->getDescendants();
				break;
			case 10:
				return $this->getChildren();
				break;
			case 11:
				return $this->getIsPublished();
				break;
			case 12:
				return $this->getPath();
				break;
			case 13:
				return $this->getStats();
				break;
			case 14:
				return $this->getKind();
				break;
			case 15:
				return $this->getIsDeleted();
				break;
			case 16:
				return $this->getPosition();
				break;
			case 17:
				return $this->getIsFeatured();
				break;
			case 18:
				return $this->getIsExpanded();
				break;
			case 19:
				return $this->getExternalUrl();
				break;
			case 20:
				return $this->getPasswd();
				break;
			case 21:
				return $this->getOptions();
				break;
			case 22:
				return $this->getCreatedAt();
				break;
			case 23:
				return $this->getCreatedBy();
				break;
			case 24:
				return $this->getModifiedAt();
				break;
			case 25:
				return $this->getModifiedBy();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PagePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getLanguage(),
			$keys[2] => $this->getName(),
			$keys[3] => $this->getNameMenu(),
			$keys[4] => $this->getSlug(),
			$keys[5] => $this->getMegaSlug(),
			$keys[6] => $this->getContent(),
			$keys[7] => $this->getContent2(),
			$keys[8] => $this->getParent(),
			$keys[9] => $this->getDescendants(),
			$keys[10] => $this->getChildren(),
			$keys[11] => $this->getIsPublished(),
			$keys[12] => $this->getPath(),
			$keys[13] => $this->getStats(),
			$keys[14] => $this->getKind(),
			$keys[15] => $this->getIsDeleted(),
			$keys[16] => $this->getPosition(),
			$keys[17] => $this->getIsFeatured(),
			$keys[18] => $this->getIsExpanded(),
			$keys[19] => $this->getExternalUrl(),
			$keys[20] => $this->getPasswd(),
			$keys[21] => $this->getOptions(),
			$keys[22] => $this->getCreatedAt(),
			$keys[23] => $this->getCreatedBy(),
			$keys[24] => $this->getModifiedAt(),
			$keys[25] => $this->getModifiedBy(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setLanguage($value);
				break;
			case 2:
				$this->setName($value);
				break;
			case 3:
				$this->setNameMenu($value);
				break;
			case 4:
				$this->setSlug($value);
				break;
			case 5:
				$this->setMegaSlug($value);
				break;
			case 6:
				$this->setContent($value);
				break;
			case 7:
				$this->setContent2($value);
				break;
			case 8:
				$this->setParent($value);
				break;
			case 9:
				$this->setDescendants($value);
				break;
			case 10:
				$this->setChildren($value);
				break;
			case 11:
				$this->setIsPublished($value);
				break;
			case 12:
				$this->setPath($value);
				break;
			case 13:
				$this->setStats($value);
				break;
			case 14:
				$this->setKind($value);
				break;
			case 15:
				$this->setIsDeleted($value);
				break;
			case 16:
				$this->setPosition($value);
				break;
			case 17:
				$this->setIsFeatured($value);
				break;
			case 18:
				$this->setIsExpanded($value);
				break;
			case 19:
				$this->setExternalUrl($value);
				break;
			case 20:
				$this->setPasswd($value);
				break;
			case 21:
				$this->setOptions($value);
				break;
			case 22:
				$this->setCreatedAt($value);
				break;
			case 23:
				$this->setCreatedBy($value);
				break;
			case 24:
				$this->setModifiedAt($value);
				break;
			case 25:
				$this->setModifiedBy($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PagePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setLanguage($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNameMenu($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSlug($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setMegaSlug($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setContent($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setContent2($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setParent($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDescendants($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setChildren($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setIsPublished($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setPath($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setStats($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setKind($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setIsDeleted($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setPosition($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setIsFeatured($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setIsExpanded($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setExternalUrl($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setPasswd($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setOptions($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setCreatedAt($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setCreatedBy($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setModifiedAt($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setModifiedBy($arr[$keys[25]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PagePeer::DATABASE_NAME);

		if ($this->isColumnModified(PagePeer::ID)) $criteria->add(PagePeer::ID, $this->id);
		if ($this->isColumnModified(PagePeer::LANGUAGE)) $criteria->add(PagePeer::LANGUAGE, $this->language);
		if ($this->isColumnModified(PagePeer::NAME)) $criteria->add(PagePeer::NAME, $this->name);
		if ($this->isColumnModified(PagePeer::NAME_MENU)) $criteria->add(PagePeer::NAME_MENU, $this->name_menu);
		if ($this->isColumnModified(PagePeer::SLUG)) $criteria->add(PagePeer::SLUG, $this->slug);
		if ($this->isColumnModified(PagePeer::MEGA_SLUG)) $criteria->add(PagePeer::MEGA_SLUG, $this->mega_slug);
		if ($this->isColumnModified(PagePeer::CONTENT)) $criteria->add(PagePeer::CONTENT, $this->content);
		if ($this->isColumnModified(PagePeer::CONTENT_2)) $criteria->add(PagePeer::CONTENT_2, $this->content_2);
		if ($this->isColumnModified(PagePeer::PARENT)) $criteria->add(PagePeer::PARENT, $this->parent);
		if ($this->isColumnModified(PagePeer::DESCENDANTS)) $criteria->add(PagePeer::DESCENDANTS, $this->descendants);
		if ($this->isColumnModified(PagePeer::CHILDREN)) $criteria->add(PagePeer::CHILDREN, $this->children);
		if ($this->isColumnModified(PagePeer::IS_PUBLISHED)) $criteria->add(PagePeer::IS_PUBLISHED, $this->is_published);
		if ($this->isColumnModified(PagePeer::PATH)) $criteria->add(PagePeer::PATH, $this->path);
		if ($this->isColumnModified(PagePeer::STATS)) $criteria->add(PagePeer::STATS, $this->stats);
		if ($this->isColumnModified(PagePeer::KIND)) $criteria->add(PagePeer::KIND, $this->kind);
		if ($this->isColumnModified(PagePeer::IS_DELETED)) $criteria->add(PagePeer::IS_DELETED, $this->is_deleted);
		if ($this->isColumnModified(PagePeer::POSITION)) $criteria->add(PagePeer::POSITION, $this->position);
		if ($this->isColumnModified(PagePeer::IS_FEATURED)) $criteria->add(PagePeer::IS_FEATURED, $this->is_featured);
		if ($this->isColumnModified(PagePeer::IS_EXPANDED)) $criteria->add(PagePeer::IS_EXPANDED, $this->is_expanded);
		if ($this->isColumnModified(PagePeer::EXTERNAL_URL)) $criteria->add(PagePeer::EXTERNAL_URL, $this->external_url);
		if ($this->isColumnModified(PagePeer::PASSWD)) $criteria->add(PagePeer::PASSWD, $this->passwd);
		if ($this->isColumnModified(PagePeer::OPTIONS)) $criteria->add(PagePeer::OPTIONS, $this->options);
		if ($this->isColumnModified(PagePeer::CREATED_AT)) $criteria->add(PagePeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(PagePeer::CREATED_BY)) $criteria->add(PagePeer::CREATED_BY, $this->created_by);
		if ($this->isColumnModified(PagePeer::MODIFIED_AT)) $criteria->add(PagePeer::MODIFIED_AT, $this->modified_at);
		if ($this->isColumnModified(PagePeer::MODIFIED_BY)) $criteria->add(PagePeer::MODIFIED_BY, $this->modified_by);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PagePeer::DATABASE_NAME);

		$criteria->add(PagePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setLanguage($this->language);

		$copyObj->setName($this->name);

		$copyObj->setNameMenu($this->name_menu);

		$copyObj->setSlug($this->slug);

		$copyObj->setMegaSlug($this->mega_slug);

		$copyObj->setContent($this->content);

		$copyObj->setContent2($this->content_2);

		$copyObj->setParent($this->parent);

		$copyObj->setDescendants($this->descendants);

		$copyObj->setChildren($this->children);

		$copyObj->setIsPublished($this->is_published);

		$copyObj->setPath($this->path);

		$copyObj->setStats($this->stats);

		$copyObj->setKind($this->kind);

		$copyObj->setIsDeleted($this->is_deleted);

		$copyObj->setPosition($this->position);

		$copyObj->setIsFeatured($this->is_featured);

		$copyObj->setIsExpanded($this->is_expanded);

		$copyObj->setExternalUrl($this->external_url);

		$copyObj->setPasswd($this->passwd);

		$copyObj->setOptions($this->options);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setCreatedBy($this->created_by);

		$copyObj->setModifiedAt($this->modified_at);

		$copyObj->setModifiedBy($this->modified_by);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getAttachements() as $relObj) {
				$copyObj->addAttachement($relObj->copy($deepCopy));
			}

			foreach($this->getImages() as $relObj) {
				$copyObj->addImage($relObj->copy($deepCopy));
			}

			foreach($this->getPageIndexs() as $relObj) {
				$copyObj->addPageIndex($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PagePeer();
		}
		return self::$peer;
	}

	
	public function initAttachements()
	{
		if ($this->collAttachements === null) {
			$this->collAttachements = array();
		}
	}

	
	public function getAttachements($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collAttachements === null) {
			if ($this->isNew()) {
			   $this->collAttachements = array();
			} else {

				$criteria->add(AttachementPeer::PAGE_ID, $this->getId());

				AttachementPeer::addSelectColumns($criteria);
				$this->collAttachements = AttachementPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(AttachementPeer::PAGE_ID, $this->getId());

				AttachementPeer::addSelectColumns($criteria);
				if (!isset($this->lastAttachementCriteria) || !$this->lastAttachementCriteria->equals($criteria)) {
					$this->collAttachements = AttachementPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastAttachementCriteria = $criteria;
		return $this->collAttachements;
	}

	
	public function countAttachements($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(AttachementPeer::PAGE_ID, $this->getId());

		return AttachementPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addAttachement(Attachement $l)
	{
		$this->collAttachements[] = $l;
		$l->setPage($this);
	}

	
	public function initImages()
	{
		if ($this->collImages === null) {
			$this->collImages = array();
		}
	}

	
	public function getImages($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collImages === null) {
			if ($this->isNew()) {
			   $this->collImages = array();
			} else {

				$criteria->add(ImagePeer::PAGE_ID, $this->getId());

				ImagePeer::addSelectColumns($criteria);
				$this->collImages = ImagePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ImagePeer::PAGE_ID, $this->getId());

				ImagePeer::addSelectColumns($criteria);
				if (!isset($this->lastImageCriteria) || !$this->lastImageCriteria->equals($criteria)) {
					$this->collImages = ImagePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastImageCriteria = $criteria;
		return $this->collImages;
	}

	
	public function countImages($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ImagePeer::PAGE_ID, $this->getId());

		return ImagePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addImage(Image $l)
	{
		$this->collImages[] = $l;
		$l->setPage($this);
	}

	
	public function initPageIndexs()
	{
		if ($this->collPageIndexs === null) {
			$this->collPageIndexs = array();
		}
	}

	
	public function getPageIndexs($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPageIndexs === null) {
			if ($this->isNew()) {
			   $this->collPageIndexs = array();
			} else {

				$criteria->add(PageIndexPeer::PAGE_ID, $this->getId());

				PageIndexPeer::addSelectColumns($criteria);
				$this->collPageIndexs = PageIndexPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(PageIndexPeer::PAGE_ID, $this->getId());

				PageIndexPeer::addSelectColumns($criteria);
				if (!isset($this->lastPageIndexCriteria) || !$this->lastPageIndexCriteria->equals($criteria)) {
					$this->collPageIndexs = PageIndexPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPageIndexCriteria = $criteria;
		return $this->collPageIndexs;
	}

	
	public function countPageIndexs($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PageIndexPeer::PAGE_ID, $this->getId());

		return PageIndexPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addPageIndex(PageIndex $l)
	{
		$this->collPageIndexs[] = $l;
		$l->setPage($this);
	}

} 