<?php


abstract class BasePagePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'page';

	
	const CLASS_DEFAULT = 'lib.model.Page';

	
	const NUM_COLUMNS = 26;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'page.ID';

	
	const LANGUAGE = 'page.LANGUAGE';

	
	const NAME = 'page.NAME';

	
	const NAME_MENU = 'page.NAME_MENU';

	
	const SLUG = 'page.SLUG';

	
	const MEGA_SLUG = 'page.MEGA_SLUG';

	
	const CONTENT = 'page.CONTENT';

	
	const CONTENT_2 = 'page.CONTENT_2';

	
	const PARENT = 'page.PARENT';

	
	const DESCENDANTS = 'page.DESCENDANTS';

	
	const CHILDREN = 'page.CHILDREN';

	
	const IS_PUBLISHED = 'page.IS_PUBLISHED';

	
	const PATH = 'page.PATH';

	
	const STATS = 'page.STATS';

	
	const KIND = 'page.KIND';

	
	const IS_DELETED = 'page.IS_DELETED';

	
	const POSITION = 'page.POSITION';

	
	const IS_FEATURED = 'page.IS_FEATURED';

	
	const IS_EXPANDED = 'page.IS_EXPANDED';

	
	const EXTERNAL_URL = 'page.EXTERNAL_URL';

	
	const PASSWD = 'page.PASSWD';

	
	const OPTIONS = 'page.OPTIONS';

	
	const CREATED_AT = 'page.CREATED_AT';

	
	const CREATED_BY = 'page.CREATED_BY';

	
	const MODIFIED_AT = 'page.MODIFIED_AT';

	
	const MODIFIED_BY = 'page.MODIFIED_BY';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Language', 'Name', 'NameMenu', 'Slug', 'MegaSlug', 'Content', 'Content2', 'Parent', 'Descendants', 'Children', 'IsPublished', 'Path', 'Stats', 'Kind', 'IsDeleted', 'Position', 'IsFeatured', 'IsExpanded', 'ExternalUrl', 'Passwd', 'Options', 'CreatedAt', 'CreatedBy', 'ModifiedAt', 'ModifiedBy', ),
		BasePeer::TYPE_COLNAME => array (PagePeer::ID, PagePeer::LANGUAGE, PagePeer::NAME, PagePeer::NAME_MENU, PagePeer::SLUG, PagePeer::MEGA_SLUG, PagePeer::CONTENT, PagePeer::CONTENT_2, PagePeer::PARENT, PagePeer::DESCENDANTS, PagePeer::CHILDREN, PagePeer::IS_PUBLISHED, PagePeer::PATH, PagePeer::STATS, PagePeer::KIND, PagePeer::IS_DELETED, PagePeer::POSITION, PagePeer::IS_FEATURED, PagePeer::IS_EXPANDED, PagePeer::EXTERNAL_URL, PagePeer::PASSWD, PagePeer::OPTIONS, PagePeer::CREATED_AT, PagePeer::CREATED_BY, PagePeer::MODIFIED_AT, PagePeer::MODIFIED_BY, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'language', 'name', 'name_menu', 'slug', 'mega_slug', 'content', 'content_2', 'parent', 'descendants', 'children', 'is_published', 'path', 'stats', 'kind', 'is_deleted', 'position', 'is_featured', 'is_expanded', 'external_url', 'passwd', 'options', 'created_at', 'created_by', 'modified_at', 'modified_by', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Language' => 1, 'Name' => 2, 'NameMenu' => 3, 'Slug' => 4, 'MegaSlug' => 5, 'Content' => 6, 'Content2' => 7, 'Parent' => 8, 'Descendants' => 9, 'Children' => 10, 'IsPublished' => 11, 'Path' => 12, 'Stats' => 13, 'Kind' => 14, 'IsDeleted' => 15, 'Position' => 16, 'IsFeatured' => 17, 'IsExpanded' => 18, 'ExternalUrl' => 19, 'Passwd' => 20, 'Options' => 21, 'CreatedAt' => 22, 'CreatedBy' => 23, 'ModifiedAt' => 24, 'ModifiedBy' => 25, ),
		BasePeer::TYPE_COLNAME => array (PagePeer::ID => 0, PagePeer::LANGUAGE => 1, PagePeer::NAME => 2, PagePeer::NAME_MENU => 3, PagePeer::SLUG => 4, PagePeer::MEGA_SLUG => 5, PagePeer::CONTENT => 6, PagePeer::CONTENT_2 => 7, PagePeer::PARENT => 8, PagePeer::DESCENDANTS => 9, PagePeer::CHILDREN => 10, PagePeer::IS_PUBLISHED => 11, PagePeer::PATH => 12, PagePeer::STATS => 13, PagePeer::KIND => 14, PagePeer::IS_DELETED => 15, PagePeer::POSITION => 16, PagePeer::IS_FEATURED => 17, PagePeer::IS_EXPANDED => 18, PagePeer::EXTERNAL_URL => 19, PagePeer::PASSWD => 20, PagePeer::OPTIONS => 21, PagePeer::CREATED_AT => 22, PagePeer::CREATED_BY => 23, PagePeer::MODIFIED_AT => 24, PagePeer::MODIFIED_BY => 25, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'language' => 1, 'name' => 2, 'name_menu' => 3, 'slug' => 4, 'mega_slug' => 5, 'content' => 6, 'content_2' => 7, 'parent' => 8, 'descendants' => 9, 'children' => 10, 'is_published' => 11, 'path' => 12, 'stats' => 13, 'kind' => 14, 'is_deleted' => 15, 'position' => 16, 'is_featured' => 17, 'is_expanded' => 18, 'external_url' => 19, 'passwd' => 20, 'options' => 21, 'created_at' => 22, 'created_by' => 23, 'modified_at' => 24, 'modified_by' => 25, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, )
	);

	
	public static function getMapBuilder()
	{
		return BasePeer::getMapBuilder('lib.model.map.PageMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PagePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PagePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PagePeer::ID);

		$criteria->addSelectColumn(PagePeer::LANGUAGE);

		$criteria->addSelectColumn(PagePeer::NAME);

		$criteria->addSelectColumn(PagePeer::NAME_MENU);

		$criteria->addSelectColumn(PagePeer::SLUG);

		$criteria->addSelectColumn(PagePeer::MEGA_SLUG);

		$criteria->addSelectColumn(PagePeer::CONTENT);

		$criteria->addSelectColumn(PagePeer::CONTENT_2);

		$criteria->addSelectColumn(PagePeer::PARENT);

		$criteria->addSelectColumn(PagePeer::DESCENDANTS);

		$criteria->addSelectColumn(PagePeer::CHILDREN);

		$criteria->addSelectColumn(PagePeer::IS_PUBLISHED);

		$criteria->addSelectColumn(PagePeer::PATH);

		$criteria->addSelectColumn(PagePeer::STATS);

		$criteria->addSelectColumn(PagePeer::KIND);

		$criteria->addSelectColumn(PagePeer::IS_DELETED);

		$criteria->addSelectColumn(PagePeer::POSITION);

		$criteria->addSelectColumn(PagePeer::IS_FEATURED);

		$criteria->addSelectColumn(PagePeer::IS_EXPANDED);

		$criteria->addSelectColumn(PagePeer::EXTERNAL_URL);

		$criteria->addSelectColumn(PagePeer::PASSWD);

		$criteria->addSelectColumn(PagePeer::OPTIONS);

		$criteria->addSelectColumn(PagePeer::CREATED_AT);

		$criteria->addSelectColumn(PagePeer::CREATED_BY);

		$criteria->addSelectColumn(PagePeer::MODIFIED_AT);

		$criteria->addSelectColumn(PagePeer::MODIFIED_BY);

	}

	const COUNT = 'COUNT(page.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT page.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PagePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PagePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PagePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PagePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PagePeer::populateObjects(PagePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PagePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PagePeer::getOMClass();
		$cls = sfPropel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

  static public function getUniqueColumnNames()
  {
    return array();
  }
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PagePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PagePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PagePeer::ID);
			$selectCriteria->add(PagePeer::ID, $criteria->remove(PagePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PagePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PagePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Page) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PagePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Page $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PagePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PagePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PagePeer::DATABASE_NAME, PagePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PagePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PagePeer::DATABASE_NAME);

		$criteria->add(PagePeer::ID, $pk);


		$v = PagePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PagePeer::ID, $pks, Criteria::IN);
			$objs = PagePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePagePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			Propel::registerMapBuilder('lib.model.map.PageMapBuilder');
}
