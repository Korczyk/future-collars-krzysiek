<?php

/**
 * Subclass for representing a row from the 'page' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Page extends BasePage
{
	public function getOption($name)
	{
		$options = unserialize(base64_decode($this->getOptions()));
		if(isset($options[$name])){
			return $options[$name];
		}
		else{
			return null;
		}
	}
	
	
	
	public function getBreadcrumps(){
		if(!isset($this->breadcrumps)){
			$conn = Propel::getConnection();
			$query = "select id,name_menu,slug,mega_slug from page where id in (".implode(",", array_reverse(explode(" ",$this->getPath()))).") order by find_in_set(id,'".implode(",", array_reverse(explode(" ",$this->getPath())))."')";
			$rs=$conn->executeQuery($query);
			$breadcrumps=array();
			while($rs->next()){
				$breadcrumps[$rs->getInt('id')]['name_menu']=$rs->getString('name_menu');
				$breadcrumps[$rs->getInt('id')]['slug']=$rs->getString('slug');
				$breadcrumps[$rs->getInt('id')]['mega_slug']=$rs->getString('mega_slug');
			}
			$this->breadcrumps = $breadcrumps;
		}
		
		return $this->breadcrumps;
	}
	
	public function fromArray($array, $keyType = BasePeer::TYPE_FIELDNAME)
	{	
		
		$array['parent'] = isset($array['parent'])?$array['parent']:0;
		$array['slug'] =  Utils::normalizeName(trim($array['name']));
		$array['name_menu'] = ($array['name_menu'] ? $array['name_menu'] : $array['name']);
		$array['is_featured'] = isset($array['is_featured']) ? $array['is_featured'] : 0;
		$array['is_published'] = isset($array['is_published']) ? $array['is_published'] : 0;
		$array['external_url'] = $array['external_url'] ? $array['external_url'] : "";
		
		
		parent::fromArray($array, $keyType = BasePeer::TYPE_FIELDNAME);
		
		$array['position'] = $this->isNew()?$this->buildPosition():$this->getPosition();
		$array['mega_slug'] = $this->buildMegaPath();
		
		$options = array();
		if(isset($array['option'])){
			foreach($array['option'] as $key=>$value){
				$options[$key]=$value;
			}
		}
		
		$array["options"]=base64_encode(serialize($options));
		
		parent::fromArray($array, $keyType = BasePeer::TYPE_FIELDNAME);
		
		
	}
	
	private function buildPosition(){
		$conn = Propel::getConnection();
		$query = "select max(position) as position from page where kind=".$this->getKind();
		$rs = $conn->executeQuery($query);
		while($rs->next()){
			$position = $rs->getInt('position');
		}
		return $position+1;
		
	}
	
	public function buildIndex(){
		
		$conn = Propel::getConnection();
		$query='delete from page_index where page_id='.$this->getId();
		$conn->executeQuery($query);
		
		$query='insert into page_index (page_id, word, weight) values ';
		$array=array();
		foreach($this->getWords() as $word => $weight){
			$array[]="(".$this->getId().",'".$word."',".$weight.")";
		}
		if($array) $conn->executeQuery($query.implode(',',$array));
		
	}
	
	public function getWords(){
		// body
		$raw_text =  str_repeat(' '.strip_tags($this->getContent()), 1);
	
		// title
		$raw_text .= str_repeat(' '.$this->getName(), 5);
	
		// title and body stemming
	
		$stemmed_words = PagePeer::stemPhrase($raw_text);
	
		// unique words with weight
		$words = array_count_values($stemmed_words);
	
		return $words;
	}
	
	
	
	
	private function buildMegaPath(){
		if($this->getParent()){
			$conn = Propel::getConnection();
			$page = PagePeer::retrieveByPK($this->getParent());
			$path = explode(" ",$page->getPath());
			if($path){
				$query = "select slug from page where id in(".implode(",",array_values($path)).") order by field (id,".implode(",",array_values($path)).")";
				$rs = $conn->executeQuery($query);
				$path=array();
				while($rs->next()){
					$path[]=$rs->getString("slug");
				}
			}
			$path = array_reverse($path);
				
			if($path){
				$path = implode("/",$path)."/".$this->getSlug();
			}
			else{
				$path=$this->getSlug();
			}
			return $path;
		}
		else{
			return $this->getSlug();
		}
	
	}
	
	public function getPathArray(){
		echo $this->getPath();
		return explode(" ",$this->getPath());
	}
	
	public function buildBreadcrump($parents){
		$array=array();
		if($this->getParent()){
			$path = explode(" ",$parents[$this->getParent()]['path']);
			$path = array_reverse($path);
			
			foreach($path as $p){
				$array[$p]=$parents[$p]['name'];
			}
		}
		return implode(" / ",$array);
	}
	
	public function isActive(){
	
		$request = sfContext::getInstance()->getRequest();
		$descendants = explode(" ",$this->getDescendants());
		$c = new Criteria;
		$c->add(PagePeer::MEGA_SLUG,$request->getParameter('slug'));
		$c->add(PagePeer::LANGUAGE,sfConfig::get('sf_dimension'));
		$page = PagePeer::doSelectOne($c);
		if($page instanceof Page){
			if(in_array($page->getId(), $descendants)){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			if(!$this->getMegaSlug()){
				return true;
			}
			else{
				return false;
			}
		}
	
	
	
	}
	
	public function getMegaSlug(){
		sfLoader::loadHelpers("I18N","Url");
		if(parent::getMegaSlug()==Utils::normalizeName(__("Homepage"))){
			return "";
		}
		else{
			return parent::getMegaSlug();
		}
	
	}
	
	
	
	
	
	
}
