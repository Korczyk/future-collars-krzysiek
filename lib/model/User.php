<?php

/**
 * Subclass for representing a row from the 'user' table.
 *
 * 
 *
 * @package lib.model
 */ 
class User extends BaseUser
{
	public function fromArray($arr, $keyType = BasePeer::TYPE_FIELDNAME)
	{
		if(strlen($arr['passwd'])){
			$arr['salt']=(md5(time().rand(0,1024)));
			$arr['passwd']=(md5($arr['salt'].$arr['passwd']));
		}
		else{
			$arr['salt']=$this->getSalt();
			$arr['passwd'] = $this->getPasswd();
				
		}
		return parent::fromArray($arr, $keyType);		
	}
}
