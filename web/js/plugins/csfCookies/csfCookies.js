function setCookie(CookieName, CookieVal, CookieExp, CookiePath, CookieDomain, CookieSecure){
    var CookieText = escape(CookieName) + '=' + escape(CookieVal); //escape() : Encodes the String
    CookieText += (CookieExp ? '; EXPIRES=' + CookieExp.toGMTString() : '');
    CookieText += (CookiePath ? '; PATH=' + CookiePath : '');
    CookieText += (CookieDomain ? '; DOMAIN=' + CookieDomain : '');
    CookieText += (CookieSecure ? '; SECURE' : '');
    document.cookie = CookieText;
}
   

function getCookie(CookieName){
    var CookieVal = null;
    if(document.cookie){
   	    var arr = document.cookie.split((escape(CookieName) + '=')); 
   	    if(arr.length >= 2){
       	    var arr2 = arr[1].split(';');
   		    CookieVal  = unescape(arr2[0]); //unescape() : Decodes the String
   	    }
    }
    return CookieVal;
}

function deleteCookie(CookieName){
    var tmp = getCookie(CookieName);
    if(tmp){ 
        setCookie(CookieName,tmp,(new Date(1))); //Used for Expire 
    }
}


$(window).load(function(){
	$("a#cookie-close").click(function(){
		setCookie('hide_cookie',1); $('.cookie-wrapper').hide();
	})
})


