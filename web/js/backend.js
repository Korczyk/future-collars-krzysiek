$(function()
{
	//prevent form submit on enter
	$(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	
	
	// hidden notify message 
	hideNotyMessage();
	
	// slideUp/Down widget
	slidingWidget();
	
	// rewrite name menu
	rewritePageTitle();
	
	if($(".page-list").length > 0)
	{
		$(this).nestedSortable(
		{
			handle: 'div',
			items: 'li',
			toleranceElement: '> div',
			listType: 'ul'
		});
	}
});

function hideNotyMessage()
{
	$("body").find("div.alert").each(function()
	{
		if($(this).data('hide') == 1)
		{
			$(this).delay($(this).data('timeout')).fadeOut($(this).data('duration'));
		}
	});
}

function slidingWidget()
{
	$("i.hide-widget").click(function()
	{
		c = $(this).parent().next();

		if(c.is(":visible")) { c.slideUp(); } else { c.slideDown(); }
		return false;
	});
}

function rewritePageTitle()
{
	$("#page_name").keyup(function()
	{
		$("#page_name_menu").val($(this).val());
	});
}

/* 
 * * image uploader * * 
 */
function imageUploader(size)
{
	removeFromGallery();
	boxCheckedActions();
	
	pageId = $("input#page_id").val(); 
	// create uploader object 
		var uploader=new plupload.Uploader({
			runtimes : 'gears,html5,flash,silverlight,browserplus',
			browse_button : 'browse',
			container : 'upload-container',
			max_file_size : size,
			url : "/backend_dev.php/zdjecia/dodaj",
			flash_swf_url : '/plupload/js/plupload.flash.swf',
			silverlight_xap_url : '/plupload/js/plupload.silverlight.xap',
			filters : [ {title : "Image files", extensions : "jpg,gif,png,tiff"} ],
			multipart_params: { "pid" : pageId }
		});
	
	// initiate uploader object
		uploader.init();
	
	// bind FILES ADDED function
	// this function is running when all files are added to queue
		uploader.bind('FilesAdded', function(up, files)
		{
			var schema;
			$("#upload-content").show()
			
			$.each(files, function(i, file) 
			{
				schema="";
			    schema += '<div class="file-item" id="upload-item-'+file.id+'" data-fid="'+file.id+'">';
			    schema += '<div class="file-item-name">'+file.name+'</div>';
			    schema += '<div class="file-item-desc"><input id = "file-desc-'+file.id+'" class = "form-control" type = "text" name = "file_desc" value = "" /></div>';
			    schema += '<div class="file-item-size">'+plupload.formatSize(file.size)+'</div>';
				schema += '<div class="file-item-delete"><a data-fid="'+file.id+'" class="removeFile" href=""><span class = "glyphicon glyphicon-remove"></span></a></div>';
				schema += '</div>';
			    
				$('#files-list').append(schema);
				// show upload button
			    if(uploader.files.length > 0) { $("#uploadfiles").css("display","inline-block"); } else { $("#uploadfiles").css("display","none"); }
			});
			
			$(".removeFile").click(function()
			{
				el=$(this);
			    $.each(uploader.files, function (i, file) 
			    {
			    	if (file && file.id == el.data('fid')) 
			    	{
			    		uploader.removeFile(file);
			    		el.parent().parent().fadeOut();
			    	}
			    });
			   
			    return false;
			});
		});
	
	// start upload function when file is added to queue
		$('#uploadfiles').click(function(e) 
		{
			uploader.start();
			e.preventDefault();
		});

	// display error msg 
		uploader.bind('Error', function(up, err) 
		{
			if(err.code == '-600') $(".errors-container").append('<div style = "margin: 1% 0;" class = "alert alert-danger fade in"><b>'+err.file.name + '</b> - Plik jest zbyt duży!<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>');
			if(err.code == '-601') $(".errors-container").append('<div style = "margin: 1% 0;" class = "alert alert-danger fade in"><b>'+err.file.name+'</b> - Niepoprawny format!<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>');
		});
	
	// before file upload action
		uploader.bind('BeforeUpload', function(up, file) 
		{
			$.extend(up.settings.multipart_params, { desc: $('input#file-desc-' + file.id).val() } );
			$("#upload-item-"+file.id+"").find(".file-item-delete").html('<div class="preloader"></div>');
		});
		
	// file is uploaded action
		uploader.bind('FileUploaded', function(up, file) 
		{
			$("#upload-item-"+file.id+"").find(".file-item-delete").html('<i class="glyphicon glyphicon-ok-circle" style="color: green; font-size: 16px;"></i>');
			$("#upload-item-"+file.id+"").delay(700).fadeOut(700);
		});
		
	// refresh files list when file is uploaded
		uploader.bind('UploadComplete', function(up, files)
		{
			$(".upload-success-msg").fadeIn(700,function()
			{
				$("#upload-content, #uploadfiles").fadeOut(200);
				$(this).delay(4000).fadeOut(5000);
			});
			
			// excluded images id array
			exclude = [];
			$(".images-list").children().each(function()
			{
				if($(this).find(".removeFromGallery").length) { exclude.push(parseInt($(this).find(".removeFromGallery").data("id"))); }
			});

			$.ajax(
			{
				url: "/backend_dev.php/zdjecia/lista",
			    type: "POST",
			    data: { pid: pageId, excluded: exclude },
			    success: function(response)
			    {
			    	json=$.parseJSON(response);
			        $.each(json, function(key,value) { addElementToFilesList(value); })
			        
			        $(".images-list").children().each(function() { if(!$(this).hasClass('image-item-removed')) $(this).fadeIn(); });
			        resetCheckUncheckAll();
			     // refresh remove from gallery function
					removeFromGallery();
			    }
			});
			
			// refresh remove from gallery function
			removeFromGallery();
		});
}

function addElementToFilesList(arr)
{
	cl = $(".images-list .image-item:last-child:not(.image-item-removed)");
	if(cl.hasClass('image-item-odd')) { cl = 'even'; } else { cl = 'odd'; }
			
	sch='<div class="image-item image-item-'+cl+' clear" style="display: none;" id="gall-img-'+arr.id+'">';
	sch += '<div class = "image-check"><input type = "checkbox" name = "image[]" class = "image-checker" data-id = "'+arr.id+'" value = "'+arr.id+'" /></div>';
	sch += '<div class="image-img">';
	sch += '<img alt="" src="'+arr.url+'"></div>';
	sch += '<div class="image-name">'+arr.name+'</div>';
	sch += '<div class="image-actions"><a data-id="'+arr.id+'" class="removeFromGallery" href="backend_dev.php/zdjecia/usun/'+arr.id+'">usuń</a></div>';
	sch += '</div>';
	$(".images-list").append(sch);
	fillImages();
}

function addElementToAttachementsList(arr)
{
	cl = $("#attached-files-list .attach-item:last-child:not(.attach-item-removed)");
	if(cl.hasClass('attach-item-odd')) { cl = 'even'; } else { cl = 'odd'; }
			
	sch='<div class="attach-item attach-item-'+cl+' clear" style="display: none;" id="attach-file-'+arr.id+'">';
	sch += '<div class = "attach-check"><input type = "checkbox" name = "image[]" class = "attach-checker" data-id = "'+arr.id+'" value = "'+arr.id+'" /></div>';
	sch += '<div class="attach-name">'+arr.name+'</div>';
	sch += '<div class="attach-actions"><a data-id="'+arr.id+'" class="removeFromAttach" href="backend_dev.php/zalaczniki/usun/'+arr.id+'">usuń</a></div>';
	sch += '</div>';
	
	$("#attached-files-list").append(sch);
}

function removeFromGallery()
{
	$(".removeFromGallery").click(function()
	{
		iid = $(this).data('id');
		$.ajax(
		{
			url: "/backend_dev.php/zdjecia/usun/"+iid,
			type: "POST",
			success: function(response)
			{
				$(".images-list #gall-img-"+iid).addClass('image-item-removed').fadeOut();
			}
		});
		
		count = 0;
		$(".images-list").find(".image-item").each(function()
		{
			if(!$(this).hasClass('image-item-removed')) { count++; }
		});
		if(count) { resetCheckUncheckAll(); $("#checked-all-actions a").hide(); }
		
		return false;
	});	
}

function removeFromAttach()
{
	$(".removeFromAttach").click(function()
	{
		if(confirm("Czy na pewno usunąć załącznik?"))
		{
			iid = $(this).data('id');
			$.ajax(
			{
				url: "/backend_dev.php/zalaczniki/usun/"+iid,
				type: "POST",
				success: function(response)
				{
					$("#attached-files-list #attach-file-"+iid).addClass('attach-item-removed').fadeOut();
				}
			});
					
			count = 0;
			$(".attach-list").find(".attach-item").each(function()
			{
				if(!$(this).hasClass('attach-item-removed')) { count++; }
			});
		}
		else
		{
			return false;
		}
		
		return false;
	});	
}

// fill images to thumbnail icon container
function fillImages()
{
	var w = 0; 
	var h = 0;
	var ratio = 0;
	
	$(".image-img").each(function()
	{
		w = $(this).find('img').width();
		h = $(this).find('img').height();

		if(w >= h) 
		{ 
			ratio = parseFloat(w/h);
			h = parseInt($(this).css('height'));
			$(this).find('img').css({ 'width' : parseInt(h*ratio)+'px', 'height' : h+'px' });
		}
		else 
		{ 
			ratio = parseFloat(h/w);
			w = parseInt($(this).css('width'));
			$(this).find('img').css({ 'width' : w+'px', 'height' : (h*ratio)+'px' }); 
		}
	});
}

// check/uncheck images and run actions
function boxCheckedActions()
{
	// add active/not active class
	$(".image-checker").click(function()
	{
		if($(this).is(":checked")) { $("#gall-img-"+$(this).data('id')).addClass('box-active');  }
		else { $("#gall-img-"+$(this).data('id')).removeClass('box-active'); }
		
		if($(".images-list").find("input.image-checker:checked").length <= 0) { resetCheckUncheckAll(); $("#checked-all-actions a").hide();} 
	});

	// check/uncheck all
	$(".checker-state-button").click(function()
	{
		el = $(this);
		$(".images-list").find("input.image-checker").each(function()
		{
			if(el.attr('id') == 'check-all') 
			{
				$(this).prop('checked',true);
				$("#gall-img-"+$(this).data('id')).addClass('box-active');
			}
			else
			{
				$(this).attr('checked',false);
				$("#gall-img-"+$(this).data('id')).removeClass('box-active');
			}
		});

		if(el.attr('id') == 'check-all') 
		{ 
			el.attr('id','uncheck-all').html('Odznacz wszystkie');
			$("#delete-checked").fadeIn(); 
		}
		else 
		{ 
			el.attr('id','check-all').html('Zaznacz wszystkie');
			$("#delete-checked").fadeOut(); 
		}
		
		return false;
	});

	$("#delete-checked").click(function()
	{
		toRemove = [];
		$(".images-list").find("input.image-checker:checked").each(function() { toRemove.push($(this).val()); });
		$.ajax(
		{
			url: "/backend_dev.php/zdjecia/usun-wiele",
			type: "POST",
			data: { pids: toRemove },
			success: function(response)
			{
				$.each(toRemove, function(index, value)
				{
					$("#gall-img-"+value).addClass('image-item-removed').fadeOut();
				});
				resetCheckUncheckAll();
				$("#checked-all-actions a").hide();
				$(".remove-success-msg").fadeIn(700).delay(3000).fadeOut();
			}
		});
		return false;
	});
}

function resetCheckUncheckAll()
{
	$("#checked-all-actions").show();
	$(".checker-state-button").attr('id','check-all').html('Zaznacz wszystkie').show();
	$("#delete-checked").fadeOut(); 
	$(".images-list").find("input.image-checker").each(function() 
	{
		$(this).attr('checked',false);
		$("#gall-img-"+$(this).data('id')).removeClass('box-active');
	});
}

function filesUploader(size)
{
	removeFromAttach();
	
	pageId = $("input#page_id").val(); 
	// create uploader object 
		var uploader = new plupload.Uploader({
			runtimes : 'gears,html5,flash,silverlight,browserplus',
			browse_button : 'browse-attachement',
			container : 'attachements-content',
			max_file_size : size,
			url : "/backend_dev.php/zalaczniki/dodaj",
			flash_swf_url : '/plupload/js/plupload.flash.swf',
			silverlight_xap_url : '/plupload/js/plupload.silverlight.xap',
			multipart_params: { "pid" : pageId }
		});
		
		uploader.init();
		
		// bind FILES ADDED function
		// this function is running when all files are added to queue
			uploader.bind('FilesAdded', function(up, files)
			{
				var schema;
				$("#attachements-content").show();
				$.each(files, function(i, file) 
				{
					schema="";
				    schema += '<div class="attached-item" id="attach-item-'+file.id+'" data-fid="'+file.id+'">';
				    schema += '<div class="attach-item-name">'+file.name+'</div>';
				    schema += '<div class="attach-item-desc"><input id = "attach-file-desc-'+file.id+'" class = "form-control" type = "text" name = "file_desc" value = "" /></div>';
				    schema += '<div class="attach-item-size">'+plupload.formatSize(file.size)+'</div>';
					schema += '<div class="attach-item-delete"><a data-fid="'+file.id+'" class="removeAttach" href=""><span class = "glyphicon glyphicon-remove"></span></a></div>';
					schema += '</div>';
				    
					$('#attachements-list').append(schema);
					// show upload button
				    if(uploader.files.length > 0) { $("#upload-attachement").css("display","inline-block"); } else { $("#upload-attachement").css("display","none"); }
				});
				
				$(".removeAttach").click(function()
				{
					el=$(this);
				    $.each(uploader.files, function (i, file) 
				    {
				    	if (file && file.id == el.data('fid')) 
				    	{
				    		uploader.removeFile(file);
				    		el.parent().parent().fadeOut();
				    	}
				    });
				    if(uploader.files.length <= 0) { $("#upload-attachement").css("display","none"); }
				    return false;
				});
			});
			
		// start upload function when file is added to queue
			$('#upload-attachement').click(function(e) 
			{
				uploader.start();
				e.preventDefault();
			});

		// display error msg 
			uploader.bind('Error', function(up, err) 
			{
				if(err.code == '-600') $(".attachements-errors-container").append('<div style = "margin: 1% 0;" class = "alert alert-danger fade in"><b>'+err.file.name + '</b> - Plik jest zbyt duży!<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>');
				if(err.code == '-601') $(".attachements-errors-container").append('<div style = "margin: 1% 0;" class = "alert alert-danger fade in"><b>'+err.file.name+'</b> - Niepoprawny format!<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>');
			});
			
			// before file upload action
			uploader.bind('BeforeUpload', function(up, file) 
			{
				$.extend(up.settings.multipart_params, { desc: $('input#attach-file-desc-' + file.id).val() } );
				$("#attach-item-"+file.id+"").find(".attach-item-delete").html('<div class="preloader"></div>');
			});
			
		// file is uploaded action
			uploader.bind('FileUploaded', function(up, file) 
			{
				$("#attach-item-"+file.id+"").find(".attach-item-delete").html('<i class="glyphicon glyphicon-ok-circle" style="color: green; font-size: 16px;"></i>');
				$("#attach-item-"+file.id+"").delay(700).fadeOut(700);
			});
			
		// refresh files list when file is uploaded
			uploader.bind('UploadComplete', function(up, files)
			{
				$(".attachement-add-success-msg").fadeIn(700,function()
				{
					$("#attachements-content, #upload-attachement").fadeOut(200);
					$(this).delay(4000).fadeOut(5000);
				});
				
				//excluded images id array
				exclude = [];
				$("#attached-files-list").children().each(function()
				{
					if($(this).find(".removeFromAttach").length) { exclude.push(parseInt($(this).find(".removeFromAttach").data("id"))); }
				});

				$.ajax(
				{
					url: "/backend_dev.php/zalaczniki/lista",
				    type: "POST",
				    data: { pid: pageId, excluded: exclude },
				    success: function(response)
				    {
				    	json=$.parseJSON(response);
				        $.each(json, function(key,value) { addElementToAttachementsList(value); })
				        
				        $("#attached-files-list").children().each(function() { if(!$(this).hasClass('attach-item-removed')) $(this).fadeIn(); });
				     ///   resetCheckUncheckAll();
				     // refresh remove from gallery function
				        removeFromAttach();
				    }
				});
				
				// refresh remove from gallery function
				removeFromAttach();
			});
		
}

function imageDescriptionChanger()
{
	$(".change-description-trigger").click(function()
	{
		el = $(this).parent().find('.change-description-wrapper');
		originalText = el.html();
		
		actionUrl = ($(this).data('type') == 'image' ? 'zdjecia' : 'zalaczniki');
		
		
		if(!el.hasClass('active-change'))
		{
			el.addClass('active-change');
			
			el.html('<input class = "form-control" type = "text" value = "'+originalText+'" data-default = "'+originalText+'" /><a href = "" class = "save-change-description"></a><a href = "" class = "cancel-change-description"></a>');
			$(".cancel-change-description").click(function() { el.removeClass('active-change').html(el.find('input').data('default')); return false; });
			
			$(".save-change-description").click(function()
			{
				saveBtn = $(this);
				
				$.ajax(
				{
					url: "/backend_dev.php/"+actionUrl+"/zmien-opis",
					type: "POST",
					data: { iid: el.parent().find('.change-description-trigger').data('imageid'), description: el.find('input').val() },
					success: function(response)
					{
						if(el.find('input').val() != "") { el.removeClass('active-change').html(el.find('input').val()); }
						else { el.removeClass('active-change').html(el.parent().find('.change-description-trigger').data('filename')); }
					}
				});
	
				return false;
			});
		}
	
		return false;
	});		
}
