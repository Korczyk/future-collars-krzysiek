$(document).ready(function() {


    function initCustomSwiperPagination(_swiper,_section) {
        var js_swiper_home = $(_swiper);
        var home_section = $(_section);


        $.each(js_swiper_home, function() {
            var pagination = home_section.find('.group-swiper .swiper-pagination');
            var arr_prev = home_section.find('.group-swiper .swiper-button-prev');
            var arr_next = home_section.find('.group-swiper .swiper-button-next');

            new Swiper(_swiper, {
                autoplay: {
                    delay: 5000,
                    disableOnInteraction: false
                },
                loop: false,
                navigation: {
                    nextEl: arr_next,
                    prevEl: arr_prev
                },
                slidesPerView: 1,
                pagination: {
                    el: pagination
                },
            });
        })
    }
    initCustomSwiperPagination('#js-swiper-home','#home-section')
    initCustomSwiperPagination('#js-swiper-story-transformation','#story-transformation-section')




    const breakpoint = window.matchMedia( '(max-width: 768px)' );
    let mySwiper;

    const breakpointChecker = function() {
        if ( breakpoint.matches === true ) {
            return enableSwiper();
        } else if ( breakpoint.matches === false ) {
            if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
            return;
        }
    };

    const enableSwiper = function() {
        mySwiper = new Swiper ('#js-swiper-partners', {
            lautoplay: {
                delay: 3000,
                disableOnInteraction: false
            },
            loop: false,
            slidesPerView: 1,
        });

    };

    breakpoint.addListener(breakpointChecker);
    breakpointChecker();




    $('select').selectric({
        disableOnMobile: false,
        nativeOnMobile: false,
        forceRenderAbove: false,
        forceRenderBelow: true,
        keySearchTimeout: 500,
    });


    function initTooltip(_handler,_tooltip,_class) {
        if($(_handler).length) {
            $(_handler).mouseover(function() {
                if(!$(_tooltip).hasClass(_class)) {
                    $(_tooltip).addClass(_class);
                }
            })
            .mouseout(function() {
                if($(_tooltip).hasClass(_class)) {
                    $(_tooltip).removeClass(_class);
                }
            });
        }
    }
    initTooltip('#consultation-form .regulations', '#consultation-form .tooltip-wrapper','active')



    function toggleQuestion(_handler,_question,_elem,_class) {
        if($(_handler).length) {
            $.each($(_question), function() {
                if(!$(this).hasClass(_class)) {
                    $(this).find(_elem).attr('style','display: none')
                }
            })
            $(document).on('click touch', _handler, function() {
                var that = $(this).closest(_question);
                if(that.hasClass(_class)) {
                    that.removeClass(_class);
                }
                else {
                    that.addClass(_class);
                }
                that.find(_elem).slideToggle();
            })
        }
    }
    toggleQuestion('.icon-toggle','.question','.row','active')




    function validateForm2(_form) {

        var $form = $(_form)
        var $button = $form.find('[type=submit]');

        // validate form
        $form.validate({
            debug: true,
            onkeyup: function(element) {
                $(element).valid();
            },
            ignore: ':not([name])',
            onfocusin: false,
            onfocusout: false,
            rules: {
                guide_adress_email: {
                    required: true
                },
                text: {
                    required: true
                },
                checkbox: {
                    required: true
                },
                radio: {
                    required: true
                },
            },
            messages: {
                guide_adress_email: {
                    required: 'List is required',
                },
                text: {
                    required: 'Text is required',
                },
                checkbox: {
                    required: 'Checkbox is required',
                },
                radio: {
                    required: 'Radio is required',
                },
            },
            errorClass: 'text',
            validClass: 'text',
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('dd').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                var $element = $(element)
                    , $group = $element.closest('.form__group');

                $group.removeClass('form__group--success').addClass('form__group--error');

                switch (element.type) {
                    case 'select-one':
                        if ($group.find('.selectric-wrapper').length !== 0) {
                            // close the select
                            $(element).selectric('close');
                        }
                        break;

                    // add more behavior...
                }
            },
            unhighlight: function(element, errorClass, validClass) {
                var $element = $(element)
                    , $group = $element.closest('.form__group');

                $group.removeClass('form__group--error').addClass('form__group--success');

                switch (element.type) {
                    case 'select-one':
                        if ($group.find('.selectric-wrapper').length !== 0) {
                            // close the select
                            $('select').selectric('close');
                        }
                        break;

                    // add more behavior...
                }
            }
        });

        $('#form-list').on('selectric-change', function(event, element, selectric) {
            console.log(event);
            console.log(element);
            console.log(selectric);

            $(this).valid();
        });

        $form.on('submit', function(event) {
            event.preventDefault();

            // required form valid
            if ($form.valid()) {
                $form.unbind('submit').submit();
            }

            return false;
        });
    }
    // validateForm2("#transformation-form")
    // validateForm2("#consultation-form")

    // form

    function initModal(_handler,_modal) {
        $(document).on('click touch', _handler, function () {
            console.log('TAK!');
            $(_modal).modal({
                fadeDuration: 100
            })
        })
    }
    initModal('a[href="#modal-update-your-career"]','#modal-update-your-caree')


    function showMore(_handler,_parent, _class) {
        $(document).on('click touch', _handler, function () {
            if($(_handler).hasClass(_class)) {

            }
        })
    }





    // const breakpoint_1100 = window.matchMedia('(max-width: 1100px)');
    // function fbreakpoint1100() {
    //     if (breakpoint_1100.matches === true) {
    //     } else {}
    // }
    // breakpoint_1100.addListener(fbreakpoint1100);
    // fbreakpoint1100();
    //



    var globalH = 0;

    function initContentHeight(_parent,_target,_height) {
        var h = $(_parent).find(_height).innerHeight();
        var contentH = $(_parent).find(_target).innerHeight();
        if(globalH == 0) {
            globalH = contentH;
            console.log('global h = '+globalH);
        }
        $(_parent).find(_target).animate({maxHeight:h+"px"},100);
        if($(_parent).hasClass('is-active')) {
            $(_parent).removeClass('is-active');
        }
    }
    initContentHeight('#knowledge-base-section','.list-of-person','.person')


    var w = $(window).width();
    $(window).on('resize', function(){
        if($(window).width() != w) {
            w = $(window).width();
            console.log('log');
            initContentHeight('#knowledge-base-section','.list-of-person','.person')
        }
    });


    function expandContent(_handler,_parent, _content, _class) {
        $(document).on('click touch', _handler, function() {
            var that = $(this).closest(_parent).find(_content);
            if($(_parent).hasClass(_class)) {
                initContentHeight('#knowledge-base-section','.list-of-person','.person')
                $(_parent).removeClass(_class);
            }
            else {
                $(this).closest(_parent).find(_content).animate({maxHeight:globalH+"px"},100)
                $(_parent).addClass(_class);
            }
        })
    }
    expandContent('#js-knowledge-base-show-more','#knowledge-base-section', '.list-of-person', 'is-active')







})

