var shareImageButton = document.querySelector('#share-image-button');
var createPostArea = document.querySelector('#create-post');
var closeCreatePostModalButton = document.querySelector('#close-create-post-modal-btn');
var sharedMomentsArea = document.querySelector('#shared-moments');

function unregisterServiceWorker() {
    if('serviceWorker' in navigator) {
        navigator.serviceWorker.getRegistrations()
            .then(function(registrations) {
                for(var i=0;i<registrations.length;i++) {
                    registrations[i].unregister();
                }
            })
    }
}

function openCreatePostModal() {
  createPostArea.style.display = 'block';
  console.log('create post modal');
  if(deferPrompt) {
    deferPrompt.prompt();
    deferPrompt.userChoice.then(function(choiceResult) {
      console.log(choiceResult.outcome)
        if(choiceResult.outcome === 'dismissed') {
          console.log('user cancel prompt');
        }
        else {
          console.log('user add to home screen')
        }
    })
      deferPrompt = null
  }
    // unregisterServiceWorker();
}

function closeCreatePostModal() {
  createPostArea.style.display = 'none';
}

shareImageButton.addEventListener('click', openCreatePostModal);

closeCreatePostModalButton.addEventListener('click', closeCreatePostModal);

const USER_REQUESTED = [
    'https://httpbin.org/get',
    '/src/images/sf-boat.jpg',
]

function checkCacheSupport() {
    if(!'caches' in window) {
        console.log('usuwam, bo nie wspieram cache');
        var button = document.querySelector('.button-add');
        button.remove();
    }
}


function onSaveButtonClicked(e) {
    //ta f pobiera jeszcze raz dynamiczne dane do NOWEJ pamieci podrecznej
    if('caches' in window) {
        console.log('obsługuje');
        caches.open('user-requested')
            .then(function(cache) {
                cache.addAll(USER_REQUESTED);
            })
    }
}

function clearCards() {
    while(sharedMomentsArea.hasChildNodes()) {
        console.log('clear cards');
        sharedMomentsArea.removeChild(sharedMomentsArea.lastChild);
    }
}

function createCard() {
    var cardWrapper = document.createElement('div');
    cardWrapper.className = 'shared-moment-card mdl-card mdl-shadow--2dp';
    var cardTitle = document.createElement('div');
    cardTitle.className = 'mdl-card__title';
    cardTitle.style.backgroundImage = 'url("/src/images/sf-boat.jpg")';
    cardTitle.style.backgroundSize = 'cover';
    cardTitle.style.height = '180px';
    cardWrapper.appendChild(cardTitle);
    var cardTitleTextElement = document.createElement('h2');
    cardTitleTextElement.style.color = 'blue';
    cardTitleTextElement.className = 'mdl-card__title-text';
    cardTitleTextElement.textContent = 'San Francisco Trip';
    cardTitle.appendChild(cardTitleTextElement);
    var cardSupportingText = document.createElement('div');
    cardSupportingText.className = 'mdl-card__supporting-text';
    cardSupportingText.textContent = 'In San Francisco';
    cardSupportingText.style.textAlign = 'center';
    var cardSaveButton = document.createElement('button');
    cardSaveButton.classList.add('button-add');
    cardSaveButton.textContent = 'Save';
    cardSupportingText.appendChild(cardSaveButton);
    cardSaveButton.addEventListener('click', onSaveButtonClicked);

    cardWrapper.appendChild(cardSupportingText);
    // componentHandler.upgradeElement(cardWrapper);
    sharedMomentsArea.appendChild(cardWrapper);
}


var url = 'https://httpbin.org/post';
var networkDataReciver = false;

// fetch(url).then(function(res) {
//         return res.json();
//     })
fetch(url,{
    method: 'POST',
    headers: {
        'Content-Type':'application/json',
        'Accept': 'application/json'
    },
    body: JSON.stringify({
        message: 'Hello world'
    })
})
    .then(function(data) {
        console.log('From web: ',data);
        networkDataReciver = true;
        clearCards();
        createCard();
    })
    .then(function() {
        checkCacheSupport();
    })


if('caches' in window) {
    caches.match(url)
        .then(function(resp) {
            if(resp) {
                return resp.json()
            }
        })
        .then(function(data) {
            if(!networkDataReciver) {
                console.log('From cache: ',data);
                clearCards();
                createCard();
            }
        })
}



