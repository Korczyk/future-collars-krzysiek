if(!window.Promise) {
    console.log('nie wspieram Promise')
    window.Promise = Promise
}

if(('serviceWorker' in navigator) && ('caches' in window)) {
    window.addEventListener('load', function () {
        navigator.serviceWorker.register('./../sw.js')
            .then(function(registration) {
                console.log('ServiceWorker registration successful')
            }).catch(function(err) {
            console.log('ServiceWorker registration failed: '+err);
        })
    })
}