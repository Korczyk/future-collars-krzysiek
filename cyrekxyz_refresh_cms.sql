-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Czas generowania: 27 Maj 2020, 12:04
-- Wersja serwera: 10.3.23-MariaDB
-- Wersja PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `cyrekxyz_refresh_cms`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `attachement`
--

CREATE TABLE `attachement` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `config`
--

INSERT INTO `config` (`id`, `name`, `value`) VALUES
(1, 'csfRedirect_pl', 'a:5:{i:0;a:2:{s:6:\"source\";s:35:\"http://refresh-cms.pl/strona-glowna\";s:6:\"target\";s:36:\"http://refresh-cms.pl//strona-glowna\";}i:1;a:2:{s:6:\"source\";s:36:\"http://refresh-cms.pl//strona-glowna\";s:6:\"target\";s:48:\"http://refresh-cms.pl/super-strona/strona-glowna\";}i:2;a:2:{s:6:\"source\";s:34:\"http://refresh-cms.pl/super-strona\";s:6:\"target\";s:42:\"http://refresh-cms.pl/kontakt/super-strona\";}i:3;a:2:{s:6:\"source\";s:56:\"http://refresh-cms.pl/kontakt/super-strona/strona-glowna\";s:6:\"target\";s:35:\"http://refresh-cms.pl/strona-glowna\";}i:4;a:2:{s:6:\"source\";s:42:\"http://refresh-cms.pl/kontakt/super-strona\";s:6:\"target\";s:34:\"http://refresh-cms.pl/super-strona\";}}'),
(2, 'email_pl', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mail`
--

CREATE TABLE `mail` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `language` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `name_menu` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `mega_slug` varchar(1024) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `content_2` text DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `descendants` text DEFAULT NULL,
  `children` text DEFAULT NULL,
  `is_published` tinyint(1) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `stats` text DEFAULT NULL,
  `kind` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `is_expanded` tinyint(1) NOT NULL DEFAULT 1,
  `external_url` varchar(1024) NOT NULL DEFAULT '',
  `passwd` varchar(32) NOT NULL DEFAULT '',
  `options` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `language`, `name`, `name_menu`, `slug`, `mega_slug`, `content`, `content_2`, `parent`, `descendants`, `children`, `is_published`, `path`, `stats`, `kind`, `is_deleted`, `position`, `is_featured`, `is_expanded`, `external_url`, `passwd`, `options`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
(1, 'pl', 'Strona Główna', 'Strona Główna', 'strona-glowna', 'strona-glowna', '<p>Hello World</p>\r\n', NULL, 0, '1', '', 1, '1', NULL, 1, NULL, 1, 1, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjMiO30=', '2018-05-08 14:50:48', 0, NULL, 0),
(2, 'pl', 'Kontakt', 'Kontakt', 'kontakt', 'kontakt', '', NULL, 0, '2', '', 1, '2', NULL, 1, NULL, 2, 1, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjMiO30=', '2018-05-08 16:33:48', 0, NULL, 0),
(4, 'pl', 'Testowy', 'Testowy', 'testowy', 'testowy', '', NULL, 0, '4', '', 0, '4', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:02:48', 0, NULL, 0),
(5, 'pl', 'Testowy2', 'Testowy2', 'testowy2', 'testowy2', '', NULL, 0, '5', '', 0, '5', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:02:58', 0, NULL, 0),
(6, 'pl', 'Testowy3', 'Testowy3', 'testowy3', 'testowy3', '', NULL, 0, '6', '', 0, '6', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:03:08', 0, NULL, 0),
(7, 'pl', 'Testowy 4', 'Testowy 4', 'testowy-4', 'testowy-4', '', NULL, 0, '7', '', 0, '7', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:03:19', 0, NULL, 0),
(8, 'pl', 'Testowy 5', 'Testowy 5', 'testowy-5', 'testowy-5', '', NULL, 0, '8', '', 0, '8', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:03:29', 0, NULL, 0),
(9, 'pl', 'Testowy 43', 'Testowy 43', 'testowy-43', 'testowy-43', '', NULL, 0, '9', '', 0, '9', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:03:38', 0, NULL, 0),
(10, 'pl', 'Testowy 123', 'Testowy 123', 'testowy-123', 'testowy-123', '', NULL, 0, '10', '', 0, '10', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:03:43', 0, NULL, 0),
(11, 'pl', 'Testowt 76', 'Testowt 76', 'testowt-76', 'testowt-76', '', NULL, 0, '11', '', 0, '11', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:03:52', 0, NULL, 0),
(12, 'pl', 'Testowy 21', 'Testowy 21', 'testowy-21', 'testowy-21', '', NULL, 0, '12', '', 0, '12', NULL, 4, NULL, 1, 0, 1, '', '', 'YTo0OntzOjEwOiJtZXRhX3RpdGxlIjtzOjA6IiI7czoxNjoibWV0YV9kZXNjcmlwdGlvbiI7czowOiIiO3M6MTM6Im1ldGFfa2V5d29yZHMiO3M6MDoiIjtzOjg6InRlbXBsYXRlIjtzOjE6IjAiO30=', '2018-05-09 11:04:01', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page_index`
--

CREATE TABLE `page_index` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `word` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `page_index`
--

INSERT INTO `page_index` (`id`, `page_id`, `word`, `weight`) VALUES
(5, 2, 'kontakt', 5),
(6, 4, 'testowy', 5),
(7, 5, 'testowy2', 5),
(8, 6, 'testowy3', 5),
(9, 7, 'testowy', 5),
(10, 8, 'testowy', 5),
(11, 9, 'testowy', 5),
(12, 10, 'testowy', 5),
(13, 10, '123', 5),
(14, 11, 'testowt', 5),
(22, 12, 'testowy', 5),
(33, 1, 'hello', 1),
(34, 1, 'world', 1),
(35, 1, 'strona', 5),
(36, 1, 'glowna', 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `role` tinyint(4) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT 0,
  `passwd` varchar(32) DEFAULT NULL,
  `salt` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `role`, `description`, `is_deleted`, `passwd`, `salt`) VALUES
(1, 'Max Cyrek', 'maxcyrek@gmail.com', 1, '', 0, '60ae8ee6b8c8756137a77b927e1057ea', '8d3cc3ace9f81da2c34a85bc5a9eca15');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `attachement`
--
ALTER TABLE `attachement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attachement_FKIndex1` (`page_id`);

--
-- Indeksy dla tabeli `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_FKIndex1` (`page_id`);

--
-- Indeksy dla tabeli `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `page_index`
--
ALTER TABLE `page_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_index_FKIndex1` (`page_id`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla tabel zrzutów
--

--
-- AUTO_INCREMENT dla tabeli `attachement`
--
ALTER TABLE `attachement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT dla tabeli `page_index`
--
ALTER TABLE `page_index`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `attachement`
--
ALTER TABLE `attachement`
  ADD CONSTRAINT `attachement_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_FK_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `page_index`
--
ALTER TABLE `page_index`
  ADD CONSTRAINT `page_index_FK_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
