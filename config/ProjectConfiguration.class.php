<?php

require_once dirname(__FILE__).'/../lib/symfony/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

require_once(dirname(__FILE__).'/../plugins/ysfDimensionsPlugin/lib/config/ysfProjectConfiguration.class.php');

class ProjectConfiguration extends ysfProjectConfiguration
{
	public function setup()
	{
		
	}
}
